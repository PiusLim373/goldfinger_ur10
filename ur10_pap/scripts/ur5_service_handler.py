#!/usr/bin/env python
from flask import Flask, request, json, jsonify
import requests
import subprocess

app = Flask(__name__)

PROGRAM_MANAGER_RUNNING = False
DEBUG_COUNTER = 0

@app.route('/start_process', methods = ['GET'])
def start_process():
    global PROGRAM_MANAGER_RUNNING
    # launch pt2 camera driver and program manger
    print("launching pt2 camera driver")
    print("launching program manager")
    # subprocess.Popen('rosrun program_manager ur5_program_manager.py ', shell=True)
    PROGRAM_MANAGER_RUNNING = True

    return jsonify({'success':PROGRAM_MANAGER_RUNNING})

@app.route('/kill_process', methods = ['GET'])
def kill_process():
    global PROGRAM_MANAGER_RUNNING
    print("launching program manager")
    subprocess.Popen(['rosnode kill /ur5_program_manager'], shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    PROGRAM_MANAGER_RUNNING = False
    return True


@app.route('/query_process', methods = ['GET']) 
def query_process():
    global DEBUG_COUNTER, PROGRAM_MANAGER_RUNNING
    print("querying program status")
    #return True if ur5 process finished, else False
    # kill pt2 camera driver as well

    #debug process
    if DEBUG_COUNTER >= 5:
        PROGRAM_MANAGER_RUNNING = False
    else:
        DEBUG_COUNTER += 1
    return jsonify({'program_status':PROGRAM_MANAGER_RUNNING})


if __name__ == "__main__":
    app.run(debug=True, port=5002)