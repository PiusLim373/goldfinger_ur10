#include <ros/ros.h>
#include <iostream>
#include <string>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <actionlib/server/simple_action_server.h>
#include <ur10_gripper/GripperServerAction.h>
#include <ur_msgs/SetIO.h>
#include <ur_msgs/IOStates.h>
#include <sensor_msgs/JointState.h>
#include <plc_server/PLCService.h>

using namespace std;

int PARALLEL_GRIPPER = 50;
int SUCTION_GRIPPER = 51;
int DRAG_GRIPPER = 52;
int CLOSING_PIN = 4;
int OPENING_PIN = 5;
int SUCTION_GRIPPER_PIN = 6; // 1 for suction_gripper,0 for parallel_gripper gripper
int DRAG_GRIPPER_PIN = 1;    // 1 for drag_gripper,0 for parallel_gripper gripper

int CURRENT_GRIPPER = PARALLEL_GRIPPER;

ros::Publisher JS_PUB;

ros::ServiceClient IO_CLIENT;
ros::ServiceClient PLC_CLIENT;

typedef actionlib::SimpleActionServer<ur10_gripper::GripperServerAction> Server;

void ur_io_service_call(int pin, int state)
{
    ur_msgs::SetIO srv;
    srv.request.fun = 1;
    srv.request.pin = pin;
    srv.request.state = state;
    IO_CLIENT.call(srv);
}

void suction(bool state)
{
    cout << "calling plc to turn on suction" << endl;
    plc_server::PLCService plc_srv;
    plc_srv.request.task = plc_srv.request.WRITE;
    plc_srv.request.defined_pin = "SUCTION";
    plc_srv.request.value = state;
    PLC_CLIENT.call(plc_srv);
}

void executeCB(const ur10_gripper::GripperServerGoalConstPtr &goal, Server *as)
{
    // create result
    ur10_gripper::GripperServerResult result;

    // gripper changing
    if (goal->task == goal->CHANGE_GRIPPER)
    {
        if (goal->option == goal->PARALLEL_GRIPPER)
        {
            ur_io_service_call(SUCTION_GRIPPER_PIN, 0);
            ur_io_service_call(DRAG_GRIPPER_PIN, 0);
            CURRENT_GRIPPER = PARALLEL_GRIPPER;
            cout << "changed to parallel_gripper" << endl;
            result.success = true;
        }
        else if (goal->option == goal->SUCTION_GRIPPER)
        {
            ur_io_service_call(SUCTION_GRIPPER_PIN, 1);
            ur_io_service_call(DRAG_GRIPPER_PIN, 0);
            CURRENT_GRIPPER = SUCTION_GRIPPER;
            cout << "changed to suction_gripper" << endl;
            result.success = true;
        }
        else if (goal->option == goal->DRAG_GRIPPER)
        {
            ur_io_service_call(DRAG_GRIPPER_PIN, 1);
            ur_io_service_call(SUCTION_GRIPPER_PIN, 0);
            CURRENT_GRIPPER = DRAG_GRIPPER;
            cout << "changed to drag_gripper" << endl;
            result.success = true;
        }
    }

    else if (goal->task == goal->GRIPPER_ACTION)
    {
        if (goal->option == goal->OPEN_GRIPPER)
        {
            if (CURRENT_GRIPPER == PARALLEL_GRIPPER)
            {
                cout << "opening parallel gripper" << endl;
                ur_io_service_call(CLOSING_PIN, 0);
                ur_io_service_call(OPENING_PIN, 1);
                usleep(100000);
                ur_io_service_call(OPENING_PIN, 0);
                cout << "opening gripper completed" << endl;
                result.success = true;
            }
            else
            {
                cout << "releasing suction" << endl;
                suction(false);
                result.success = true;
            }
        }

        else if (goal->option == goal->CLOSE_GRIPPER)
        {
            if (CURRENT_GRIPPER == PARALLEL_GRIPPER)
            {
                cout << "closing gripper" << endl;
                ur_io_service_call(OPENING_PIN, 0);
                ur_io_service_call(CLOSING_PIN, 1);
                usleep(100000);
                ur_io_service_call(CLOSING_PIN, 0);
                usleep(300000); // sleep in microsec, 300ms here
                try
                {
                    // float gripper_voltage_sum = 0.0;
                    // int i = 10;
                    // while (i--) gripper_voltage_sum += ros::topic::waitForMessage<ur_msgs::IOStates>("/ur_hardware_interface/io_states", ros::Duration (5.0))->analog_in_states[0].state;
                    // result.opening = 32.0 - gripper_voltage_sum/10.0/10.0*32;
                    float gripper_voltage = ros::topic::waitForMessage<ur_msgs::IOStates>("/ur_hardware_interface/io_states", ros::Duration(5.0))->analog_in_states[0].state;
                    // result.opening = 32.0 - gripper_voltage/10.0*32;
                    result.opening = 32.0 - (int)gripper_voltage / 10.0 * 32;
                }
                catch (...)
                {
                    cout << "something wrong, node cant keep up, ignoreing" << endl;
                }
                result.success = true;
            }
            else if (CURRENT_GRIPPER == SUCTION_GRIPPER)
            {
                cout << "activating suction" << endl;
                suction(true);
                result.opening = -1;
                result.success = true;
            }
        }
    }
    else if (goal->task == goal->GET_OPENING)
    {
        cout << "getting gripper opening" << endl;
        // float gripper_voltage_sum = 0.0;
        // int i = 10;
        // while (i--) gripper_voltage_sum += ros::topic::waitForMessage<ur_msgs::IOStates>("/ur_hardware_interface/io_states", ros::Duration (5.0))->analog_in_states[0].state;
        // // cout << 32.0 - gripper_voltage_sum/5.0/10.0*32<< "mm" <<endl;
        // result.opening = 32.0 - gripper_voltage_sum/10.0/10.0*32;
        try
        {
            float gripper_voltage = ros::topic::waitForMessage<ur_msgs::IOStates>("/ur_hardware_interface/io_states", ros::Duration(5.0))->analog_in_states[0].state;
            result.opening = 32.0 - (int)gripper_voltage / 10.0 * 32;
            result.success = true;
        }
        catch (...)
        {
            cout << "something wrong, node cant keep up, ignoreing" << endl;
            result.success = false;
        }
    }

    else
        cout << "invalid options" << endl;

    // setting result and return
    cout << result << endl;
    as->setSucceeded(result);
    return;
}

void publish_joint_state()
{
    ros::Rate rate(5); // ROS Rate at 5Hz
    while (ros::ok())
    {
        sensor_msgs::JointState publish_data;
        publish_data.header.stamp = ros::Time::now();
        publish_data.position.resize(1);
        publish_data.name.resize(1);
        publish_data.name[0] = "zimmer_base_link__tray_finger";
        try
        {
            float voltage = ros::topic::waitForMessage<ur_msgs::IOStates>("/ur_hardware_interface/io_states", ros::Duration(5.0))->analog_in_states[0].state;
            float opening = (32.0 - voltage / 10.0 * 32.0) / 2000;
            publish_data.position[0] = opening;
        }
        catch (...)
        {
            cout << "something wrong, node cant keep up, ignoreing" << endl;
        }
        JS_PUB.publish(publish_data);
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "gripper_server");
    ros::NodeHandle nh;

    ros::AsyncSpinner spinner(0);
    spinner.start();

    // initialise a service call client to ur io
    IO_CLIENT = nh.serviceClient<ur_msgs::SetIO>("/ur_hardware_interface/set_io");
    PLC_CLIENT = nh.serviceClient<plc_server::PLCService>("/plc_server");

    // gripper action lib, takes in action lib call from program manager and trigger io to open/close gripper
    Server server(nh, "gripper_server", boost::bind(&executeCB, _1, &server), false);
    server.start();

    // publish gripper opening to /joint_states to visualize finger opening on rviz
    JS_PUB = nh.advertise<sensor_msgs::JointState>("joint_states", 1);
    publish_joint_state();
    ros::waitForShutdown();
}
