#!/usr/bin/env python
import numpy
import requests
import globals
import smach
import rospy
from hardcoded_variables import *
import os
from time import sleep
import time
import ast

from ur10_pap.msg import *
from pcl_server.msg import *
from ai_server.msg import *
from ib1_server.msg import *

from geometry_msgs.msg import PoseStamped

IGNORE_IB = False

################################################################################################# NORMAL FUNCTIONS #########################
def get_depth(theta, u, v, corrected_angle):
    angle_list = [0, 45, 90, 135, 180]
    closest_angle_list = []
    for x  in angle_list:
        closest_angle_list.append(abs(theta - x))
    closest_angle = angle_list[closest_angle_list.index(min(closest_angle_list))]
    angle_pixel_crosstable = {0: (0, -1), 45:(1, -1), 90:(1, 0), 135:(1, 1), 180: (0, 1), 225:(-1, 1), 270:(-1, 0), 315:(-1, -1)}
    has_depth = False
    return_pose = None
    while not has_depth: 
        for i in range(1, 3):
            angle_list = [0, 45, 90, 135, 180, 225, 270, 315]
            if closest_angle < 180:
                priority_angle_list = [closest_angle, closest_angle+180]
            else:
                priority_angle_list = [closest_angle, closest_angle-180]
            for x in priority_angle_list:
                angle_list.remove(x)
            total = [-1] + priority_angle_list + angle_list
            for x in total:
                if x == -1:
                    check_u = u
                    check_v = v
                else:
                    check_u = u + angle_pixel_crosstable[x][0]*i
                    check_v = v + angle_pixel_crosstable[x][1]*i
                rospy.loginfo("checking point cloud at u, v: [" + str(check_u) + ", " +str(check_v) + "]")
                pcl_goal = PCLServerGoal()
                pcl_goal.task = pcl_goal.UPDATE_POINTCLOUD
                pcl_goal.module = "pt1"
                globals.PCL_CLIENT.send_goal(pcl_goal)
                globals.PCL_CLIENT.wait_for_result()
                pcl_result = PCLServerResult()
                pcl_result = globals.PCL_CLIENT.get_result()
                if pcl_result.success:
                    pcl_input = [check_u, check_v, corrected_angle]
                    pcl_goal = PCLServerGoal()
                    pcl_goal.task = pcl_goal.GET_COOR
                    pcl_goal.pcl_input = pcl_input
                    pcl_goal.module = "pt1"
                    globals.PCL_CLIENT.send_goal(pcl_goal)
                    globals.PCL_CLIENT.wait_for_result()
                    pcl_result = PCLServerResult()
                    pcl_result = globals.PCL_CLIENT.get_result()
                    has_depth = pcl_result.success
                    if has_depth:
                        rospy.loginfo("depth get successfully")
                        return_pose = pcl_result.coor_wrt_world
                        break
                else:
                    rospy.logerr("something wrong when capturing depth")
            if has_depth:
                break
    return return_pose

def check_drag_pick_suceess(mode = 'default', name = None, u = None, v = None):
    ai_goal = AIServerGoal()
    ai_goal.task = ai_goal.PICK_SUCCESS_CHECK
    ai_goal.which_module = ai_goal.PT1
    if mode == 'default':        
        ai_goal.previous_tried_inst_name = globals.PREVIOUS_TRIED_INSTRUMENT['name']
        ai_goal.previous_tried_inst_u = globals.PREVIOUS_TRIED_INSTRUMENT['u']
        ai_goal.previous_tried_inst_v = globals.PREVIOUS_TRIED_INSTRUMENT['v']
    elif mode == 'flipper':
        ai_goal.previous_tried_inst_name = name
        ai_goal.previous_tried_inst_u = u
        ai_goal.previous_tried_inst_v = v
    globals.AI_CLIENT.send_goal(ai_goal)
    globals.AI_CLIENT.wait_for_result()
    ai_result = AIServerResult()
    ai_result = globals.AI_CLIENT.get_result()
    if ai_result.item_remaining == 1:
        rospy.loginfo("drag / pick failed")
        globals.PREVIOUS_TRIED_INSTRUMENT['try_count'] += 1
        return False
    elif ai_result.item_remaining == 0:
        rospy.loginfo("drag / pick success!")
        globals.PREVIOUS_TRIED_INSTRUMENT = {'name': '', 'u': 0, 'v':0, 'try_count':0, 'eliminate': False}
        return True
    elif ai_result.item_remaining == 2:
        rospy.logerr("pick success check query failed")
        return False

def activate_ocm():
    if globals.OCM_STATUS['try_count'] < 2:
        rospy.loginfo("turn on orientation correcting mechanism with mode:" + str(globals.OCM_STATUS['mode']) + ', current try count: ' + str(globals.OCM_STATUS['try_count']))
        if globals.OCM_STATUS['mode'] == 1:
            os.system("rosservice call /ur_hardware_interface/set_io 1 " + str(ocm_pin_a) + " 1")
            sleep(0.5)
            os.system("rosservice call /ur_hardware_interface/set_io 1 " + str(ocm_pin_a) + " 0")
            sleep(2)
        elif globals.OCM_STATUS['mode'] == 2:
            os.system("rosservice call /ur_hardware_interface/set_io 1 " + str(ocm_pin_b) + " 1")
            sleep(0.5)
            os.system("rosservice call /ur_hardware_interface/set_io 1 " + str(ocm_pin_b) + " 0")
            sleep(2)
        globals.OCM_STATUS['try_count'] += 1
        return 'success'
    else:
        if globals.OCM_STATUS['mode'] == 1:
            globals.OCM_STATUS['mode'] = 2
        elif globals.OCM_STATUS['mode'] == 2:
            globals.OCM_STATUS['mode'] = 1
        globals.OCM_STATUS['try_count'] = 0
        activate_ocm()

def update_pt1_and_ocm_center():
    ai_goal = AIServerGoal()
    ai_goal.which_module = ai_goal.PT1
    ai_goal.task = ai_goal.GET_BOUNDARY_CENTER
    globals.AI_CLIENT.send_goal(ai_goal)
    globals.AI_CLIENT.wait_for_result()
    ai_result = AIServerResult()
    ai_result = globals.AI_CLIENT.get_result()
    if ai_result.topick:
        if ai_result.ai_output.v < 350:
            print("pt1 too far away, activating rotating table")
            return False
        pt1_center_x , pt1_center_y = ai_result.ai_output.u, ai_result.ai_output.v
        ocm_center_x , ocm_center_y = ai_result.ai_output.h, ai_result.ai_output.w
        pt1_depth, ocm_depth = False, False
        pt1_check = [[pt1_center_x, pt1_center_y]]
        ocm_check = [[ocm_center_x , ocm_center_y]]
        for i in range(1, 5):
            pt1_check.append([pt1_center_x + i, pt1_center_y + i])
            pt1_check.append([pt1_center_x - i, pt1_center_y - i])
            ocm_check.append([ocm_center_x + i, ocm_center_y + i])
            ocm_check.append([ocm_center_x - i, ocm_center_y - i])
        for j in range(9):
            pcl_goal = PCLServerGoal()
            pcl_goal.task = pcl_goal.UPDATE_POINTCLOUD
            pcl_goal.module = "pt1"
            globals.PCL_CLIENT.send_goal(pcl_goal)
            globals.PCL_CLIENT.wait_for_result()
            pcl_result = PCLServerResult()
            pcl_result = globals.PCL_CLIENT.get_result()
            if pcl_result.success:
                if not pt1_depth:
                    pcl_input = [pt1_check[j][0], pt1_check[j][1], -90]
                    pcl_goal = PCLServerGoal()
                    pcl_goal.task = pcl_goal.GET_COOR
                    pcl_goal.pcl_input = pcl_input
                    pcl_goal.module = "pt1"
                    globals.PCL_CLIENT.send_goal(pcl_goal)
                    globals.PCL_CLIENT.wait_for_result()
                    pt1_pcl_result = PCLServerResult()
                    pt1_pcl_result = globals.PCL_CLIENT.get_result()
                    pt1_depth = pt1_pcl_result.success
                    if pt1_depth:
                        rospy.loginfo("pt1 depth get successfully")
                        globals.PT1_CENTER_COOR = pt1_pcl_result.coor_wrt_world
                if not ocm_depth:
                    pcl_input = [ocm_check[j][0], ocm_check[j][1], -90]
                    pcl_goal = PCLServerGoal()
                    pcl_goal.task = pcl_goal.GET_COOR
                    pcl_goal.pcl_input = pcl_input
                    pcl_goal.module = "pt1"
                    globals.PCL_CLIENT.send_goal(pcl_goal)
                    globals.PCL_CLIENT.wait_for_result()
                    ocm_pcl_result = PCLServerResult()
                    ocm_pcl_result = globals.PCL_CLIENT.get_result()
                    ocm_depth = ocm_pcl_result.success
                    if ocm_depth:
                        rospy.loginfo("ocm depth get successfully")
                        globals.OCM_CENTER_COOR = ocm_pcl_result.coor_wrt_world
                        globals.OCM_CENTER_COOR.pose.position.z = 1.035 #was 1.045
                if pt1_depth and ocm_depth:
                    break
            else:
                rospy.logwarn("something wrong when capturing depth, retrying")
        if (pt1_depth and ocm_depth):
            rospy.loginfo("ocm and pt1 depth updated successfully")
            return True
        else:
            globals.PT1_CENTER_COOR = None
            globals.OCM_CENTER_COOR = None
            rospy.logwarn("pt1 and ocm center coor NOT updated")
            return False
    else:
        rospy.logwarn("ai error finding boundary center")
        return False

def transfer_back_pt1():
    '''On event of picking failure detection, will release the gripped instrument back to pt1 and retry'''
    motion_goal = RobotControlGoal()
    motion_goal.option = motion_goal.PLACE_SPECIAL
    if globals.PT1_CENTER_COOR:
        motion_goal.target_pose = globals.PT1_CENTER_COOR
        motion_goal.special_item = "pt1_fail_pick_release"
    else:
        motion_goal.special_item = "pt1_fail_pick_release_error"
    globals.MOTION_CLIENT.send_goal(motion_goal)
    globals.MOTION_CLIENT.wait_for_result()
    if globals.MOTION_CLIENT.get_result().success:
        return True

                           
def adjust_belt_width(instrument):
    if IGNORE_IB:
        return True
    ib1_goal = IB1ServerGoal()
    ib1_goal.task = ib1_goal.SET_CONVEYOR_WIDTH
    ib1_goal.tool_loaded = instrument
    globals.IB1_CLIENT.send_goal(ib1_goal)
    globals.IB1_CLIENT.wait_for_result()
    ib1_status = globals.IB1_CLIENT.get_result().status
    if ib1_status == "AVAILABLE":
        return True
    else:
        return False

def check_ib_fully_ready():
    if IGNORE_IB:
        return True
    ib1_goal = IB1ServerGoal()
    ib1_goal.task = ib1_goal.GET_STATUS
    globals.IB1_CLIENT.send_goal(ib1_goal)
    if globals.IB1_CLIENT.wait_for_result(rospy.Duration(1)):
        ib1_status = globals.IB1_CLIENT.get_result().status
        if ib1_status == "AVAILABLE":
            return True
        else:
            return False
    else:
        rospy.loginfo("no respond from IB1, probably still inspecting")
        return False

def indicator_release(instrument):
    '''place an indicator on the belt where tcj is loaded to be transferred into wrapping bay'''
    motion_goal = RobotControlGoal()
    motion_goal.option = motion_goal.PLACE
    motion_goal.target_pose = globals.list_to_posestamped(ib_indicator_release_placeholder)
    motion_goal.starting_location = "pt1"
    globals.MOTION_CLIENT.send_goal(motion_goal)
    globals.MOTION_CLIENT.wait_for_result()
    if globals.MOTION_CLIENT.get_result().success:
        ib1_goal = IB1ServerGoal()
        ib1_goal.task = ib1_goal.DELIVER_INDICATOR
        ib1_goal.tool_loaded = instrument
        globals.IB1_CLIENT.send_goal(ib1_goal)
        sleep(0.5)
        globals.LOGGING.loginfo("Successfully transfer indicator to Wrapping Bay")
        return True
    else:
        globals.LOGGING.logerr("Error during dropping indicator at Inspection Bay")
        return False

def release_and_wrap(instrument):
    globals.LOGGING.loginfo("Opening slot to drop " + str(instrument) + " into Wrapping Bay")
    ib1_goal = IB1ServerGoal()
    ib1_goal.task = ib1_goal.OPEN_SORTER
    ib1_goal.tool_loaded = instrument
    globals.IB1_CLIENT.send_goal(ib1_goal)
    globals.IB1_CLIENT.wait_for_result()
    if globals.IB1_CLIENT.get_result().status == "AVAILABLE":
        globals.LOGGING.loginfo("Drop successful, wrapping in progress...")
        if requests.get(globals.WRAPPING_BAY_URL).status_code == 200:
            return True
        else:
            globals.LOGGING.logerr("Error during wrapping " +str(instrument) + ", please retrieve and wrap the items manually and transfer to drybay")
            return False
    else:
        globals.LOGGING.logerr("Error during opening slot for " +str(instrument) + ", please retrieve and wrap the items manually and transfer to drybay")
        return False

################################################################################################# SMACH CLASSES ############################
class InitialiseIB(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'fail'])
    
    def execute(self, userdata):
        if IGNORE_IB:
            return 'success'
        if globals.HAS_INITIALISE_IB:
            print("IB has been initialised before, skipping")
            return 'success'
        else:
            globals.LOGGING.loginfo("Initialise IB with Box ID: " + str(globals.BOX_ID))
            while not check_ib_fully_ready():
                sleep(1)
            ib1_goal = IB1ServerGoal()
            ib1_goal.task = ib1_goal.RESET
            ib1_goal.box_id = str(globals.BOX_ID).zfill(3)
            globals.IB1_CLIENT.send_goal(ib1_goal)
            globals.IB1_CLIENT.wait_for_result(rospy.Duration(1))
            if globals.IB1_CLIENT.get_result().status == "AVAILABLE":
                globals.HAS_INITIALISE_IB = True
                return 'success'
            else:
                globals.LOGGING.logerr("Something wrong during ib initialization, is power on?")
                return 'fail'

class ActivateRotatingTable(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'retry'])
    
    def execute(self, userdata):
        globals.LOGGING.loginfo("Processing Table 1 to Non-Ring Inspection Bay transfer process started...")
        rospy.loginfo("turn on processing table rotating mechanism")
        os.system("rosservice call /ur_hardware_interface/set_io 1 " + str(rotating_table_pin) + " 1")
        sleep_timer = numpy.random.uniform(1,2)
        sleep(sleep_timer)
        os.system("rosservice call /ur_hardware_interface/set_io 1 " + str(rotating_table_pin) + " 0")
        sleep(sleep_timer)
        if update_pt1_and_ocm_center():
            return "success"
        else: 
            globals.LOGGING.logerr("Error during PT1 and OCM depth aquisition, retrying")
            return "retry"

class QueryIB(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'fail'])
    
    def execute(self, userdata):
        rospy.loginfo("checking ib to see if available")
        if IGNORE_IB:
            return 'success'
        ib1_goal = IB1ServerGoal()
        ib1_goal.task = ib1_goal.GET_STATUS
        globals.IB1_CLIENT.send_goal(ib1_goal)
        if not globals.IB1_CLIENT.wait_for_result(rospy.Duration(1)):
            globals.LOGGING.logwarn("IB1 still inspecting")
        else:
            ib1_status = globals.IB1_CLIENT.get_result().status
            if ib1_status == "AVAILABLE":
                globals.IB_STATUS['ready'] = True
            elif ib1_status == "ERROR":
                globals.LOGGING.logerr("Non-Ring Inspection Bay has raised error, please remove items and restart")
                return 'fail'
            elif time.time() - globals.IB_STATUS['time_last_load'] > 50:
                rospy.loginfo("50 sec has passed, preparing to load")
                globals.IB_STATUS['ready'] = True
        return 'success'
        
class QueryVision(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'retry', 'activate_rotating_table', 'drag', 'wait', 'get_more_inst'])
    
    def execute(self, userdata):
        globals.INSTRUMENT_DETECTED['name'] = ''
        globals.INSTRUMENT_DETECTED['coor_to_go'] = PoseStamped()
        globals.INSTRUMENT_DETECTED['drag_direction'] = ''
        globals.INSTRUMENT_DETECTED['u'] = 0
        globals.INSTRUMENT_DETECTED['v'] = 0
        globals.INSTRUMENT_DETECTED['orientation'] = ''
        ai_goal = AIServerGoal()
        ai_goal.which_module = ai_goal.PT1
        ai_goal.inspection_bay.ready = globals.IB_STATUS['ready']
        ai_goal.inspection_bay.time_last_load = globals.IB_STATUS['time_last_load']
        ai_goal.inspection_bay.inst_last_load = globals.IB_STATUS['inst_last_load']

        #eliminate prevous tried inst from search list
        if globals.PREVIOUS_TRIED_INSTRUMENT['eliminate']:
            ai_goal.previous_tried_inst_name = globals.PREVIOUS_TRIED_INSTRUMENT['name']
            ai_goal.previous_tried_inst_u = globals.PREVIOUS_TRIED_INSTRUMENT['u']
            ai_goal.previous_tried_inst_v = globals.PREVIOUS_TRIED_INSTRUMENT['v']

        if globals.FORCE_ISOLATE:
            ai_goal.task = ai_goal.FORCE_ISOLATION
            globals.FORCE_ISOLATE = False
        
        globals.AI_CLIENT.send_goal(ai_goal)
        globals.AI_CLIENT.wait_for_result()
        ai_result = AIServerResult()
        ai_result = globals.AI_CLIENT.get_result()
        if ai_result.topick:
            globals.EMPTY_COUNTER = 0
            #found previous instrument, checking if to eliminate from list, activate rotating table to refresh the table
            if (ai_result.ai_output.item == globals.PREVIOUS_TRIED_INSTRUMENT['name']) and(abs(ai_result.ai_output.u) - globals.PREVIOUS_TRIED_INSTRUMENT['u'] < 10) and (abs(ai_result.ai_output.v) - globals.PREVIOUS_TRIED_INSTRUMENT['v'] < 10):
                rospy.loginfo("found previous tried instrument")
                if globals.PREVIOUS_TRIED_INSTRUMENT['try_count'] >= 2:
                    globals.PREVIOUS_TRIED_INSTRUMENT['try_count'] = 0
                    rospy.loginfo("previous tried instrument has been tried for 2 times, eliminating from the search list")
                    globals.PREVIOUS_TRIED_INSTRUMENT['eliminate'] = True
                    return 'activate_rotating_table'
                else:
                    rospy.loginfo("previous tried instrument hasn't reach the trying threshold, trying...")

            #picking of isolated instrument
            if not ai_result.overlapped:
                rospy.loginfo("isolated instrument found, proceeding to get depth data")
                angle = ai_result.ai_output.theta - 180
                orientation = ai_result.ai_output.side
                item = ai_result.ai_output.item
                globals.INSTRUMENT_DETECTED['name'] = item
                globals.INSTRUMENT_DETECTED['u'] = ai_result.ai_output.u
                globals.INSTRUMENT_DETECTED['v'] = ai_result.ai_output.v
                globals.INSTRUMENT_DETECTED['coor_to_go'] = get_depth(ai_result.ai_output.theta, ai_result.ai_output.u, ai_result.ai_output.v, angle)
                if not globals.INSTRUMENT_DETECTED['coor_to_go']:
                    rospy.logwarn("pcl cant get depth, retrying")
                    return 'retry'
                if globals.INSTRUMENT_DETECTED['coor_to_go'].pose.position.z > 1.2:
                    rospy.logwarn("pcl get weird height, retrying")
                    return 'retry'
                globals.INSTRUMENT_DETECTED['orientation'] = orientation
                rospy.loginfo(globals.INSTRUMENT_DETECTED['coor_to_go'])
                globals.PREVIOUS_TRIED_INSTRUMENT['name'] = item
                globals.PREVIOUS_TRIED_INSTRUMENT['u'] = ai_result.ai_output.u
                globals.PREVIOUS_TRIED_INSTRUMENT['v'] = ai_result.ai_output.v
                globals.DRAG_COUNTER = 0
                print(globals.INSTRUMENT_DETECTED)
                return 'success'

            #to drag overlapping instrument to side for picking
            else:
                if globals.DRAG_COUNTER >= 3:
                    rospy.logwarn("has been dragging for 3 times, shaking the table instead to prevent looping")
                    globals.DRAG_COUNTER = 0
                    globals.PREVIOUS_TRIED_INSTRUMENT['name'] = ''
                    return 'activate_rotating_table'

                rospy.loginfo("all intruments are all overlapped with others")
                angle = -90
                globals.INSTRUMENT_DETECTED['name'] = ai_result.ai_output.item
                globals.INSTRUMENT_DETECTED['u'] = ai_result.ai_output.u
                globals.INSTRUMENT_DETECTED['v'] = ai_result.ai_output.v
                globals.INSTRUMENT_DETECTED['coor_to_go'] = get_depth(ai_result.ai_output.theta, ai_result.ai_output.u, ai_result.ai_output.v, angle)
                if not globals.INSTRUMENT_DETECTED['coor_to_go']:
                    rospy.logwarn("pcl cant get depth, retrying")
                    return 'retry'
                if globals.INSTRUMENT_DETECTED['coor_to_go'].pose.position.z > 1.2:
                    rospy.logwarn("pcl get weird height, retrying")
                    return 'retry'
                globals.INSTRUMENT_DETECTED['orientation'] = ai_result.ai_output.side
                globals.INSTRUMENT_DETECTED['drag_direction'] = ai_result.direction
                rospy.loginfo(globals.INSTRUMENT_DETECTED['coor_to_go'])
                globals.PREVIOUS_TRIED_INSTRUMENT['name'] = ai_result.ai_output.item
                globals.PREVIOUS_TRIED_INSTRUMENT['u'] = ai_result.ai_output.u
                globals.PREVIOUS_TRIED_INSTRUMENT['v'] = ai_result.ai_output.v
                return 'drag'
        else:
            #both ib not free and call for force isolation
            if ai_result.force_isolation:
                rospy.loginfo("vision cant find instruments that the inspection bays are currently processing, force isolate is activated")
                globals.FORCE_ISOLATE = True
            
            #call for force isolation returns, 
            # item_remaining == 2 means more than 5 isolated instrument detected, to just wait 
            # item_remaining == 3 means all insturment are alr isolated, to just wait as well
            elif ai_result.item_remaining == 2 or ai_result.item_remaining == 3:
                globals.FORCE_ISOLATE = False
                rospy.loginfo("force_isolation routine call returned, but vision detected that either there are already 5 of all instruemnt isolated, to just wait")
                sleep(1)
                return 'wait'

            else:
                globals.FORCE_ISOLATE = False
                rospy.loginfo("ai cant see anything")
                if globals.EMPTY_COUNTER >=5:
                    rospy.logwarn("cant see item for 5 times, prompting to get more instrument")
                    return 'get_more_inst'
                globals.EMPTY_COUNTER += 1
            return 'retry'

class DragInst(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'retry'])
    
    def execute(self, userdata):
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.GO_TO_PT1_INSTRUMENT_PAP
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.CHANGE_GRIPPER
            motion_goal.gripper = motion_goal.DRAG
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                motion_goal.option = motion_goal.DRAG_TASK
                motion_goal.target_pose = globals.INSTRUMENT_DETECTED['coor_to_go']
                motion_goal.starting_location = "pt1_instrument_pap"
                motion_goal.special_item = globals.INSTRUMENT_DETECTED['drag_direction']
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    globals.DRAG_COUNTER += 1
                    if check_drag_pick_suceess():
                        rospy.loginfo("instrument drag is successful")
                        rospy.logwarn(globals.INSTRUMENT_DETECTED)
                        return 'success'
                    else:
                        rospy.logwarn("something wrong during instrument drag, vision detected item still there")
                        return 'retry'
                else:
                    rospy.logwarn("something wrong during instrument drag")
                    return 'retry'
            else:
                rospy.loginfo("error during gripper changing")
                return 'retry'
        else:
            rospy.loginfo("error during pt1 homing")
            return 'retry'

class TransferInst(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'retry', 'flip'])
    
    def execute(self, userdata):
        transfer_to_flipper = False
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.GO_TO_PT1_INSTRUMENT_PAP
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.CHANGE_GRIPPER
            if globals.INSTRUMENT_DETECTED['name'] == "RUL":
                motion_goal.gripper = motion_goal.SUCTION
            else:
                motion_goal.gripper = motion_goal.PARALLEL
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                # motion_goal.option = motion_goal.PICK
                motion_goal.option = motion_goal.PICK_SPECIAL
                motion_goal.special_item = "pt1_picking"
                motion_goal.target_pose = globals.INSTRUMENT_DETECTED['coor_to_go']
                if globals.INSTRUMENT_DETECTED['name'] == "SPR" or globals.INSTRUMENT_DETECTED['name'] == "BAP" or globals.INSTRUMENT_DETECTED['name'] == "RUL":
                    motion_goal.target_pose.pose.position.z = 1.064 #1.073
                elif globals.INSTRUMENT_DETECTED['name'] == "RTL":
                    motion_goal.target_pose.pose.position.z = 1.067 #1.074
                elif globals.INSTRUMENT_DETECTED['coor_to_go'].pose.position.z - 0.005 < 1.07:  #1.077
                    motion_goal.target_pose.pose.position.z = 1.07
                else:
                    motion_goal.target_pose.pose.position.z -= 0.005
                # if globals.INSTRUMENT_DETECTED['coor_to_go'].pose.position.z - 0.01 < 1.046:
                #     motion_goal.target_pose.pose.position.z = 1.046
                # else:
                #     motion_goal.target_pose.pose.position.z -= 0.01 
                motion_goal.starting_location = "pt1_instrument_pap"
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                motion_result = RobotControlResult()
                motion_result = globals.MOTION_CLIENT.get_result()
                if motion_result.success:
                    motion_goal = RobotControlGoal()
                    motion_goal.option = motion_goal.READ_GRIPPER_OPENING
                    globals.MOTION_CLIENT.send_goal(motion_goal)
                    globals.MOTION_CLIENT.wait_for_result()
                    motion_result = RobotControlResult()
                    motion_result = globals.MOTION_CLIENT.get_result()
                    if motion_result.success:
                        write_string = globals.INSTRUMENT_DETECTED['name'] + ", " + (globals.INSTRUMENT_DETECTED['orientation']) + ", " + str(motion_result.gripper_opening)
                        ############ checking gripper opening 
                        if globals.INSTRUMENT_DETECTED['name'] == "FOR" and globals.INSTRUMENT_DETECTED['orientation'] == 'S':
                            if round(motion_result.gripper_opening, 1) != instrument_width[globals.INSTRUMENT_DETECTED['name']]['S1'] and round(motion_result.gripper_opening, 1) != instrument_width[globals.INSTRUMENT_DETECTED['name']]['S2']:
                                # rospy.logwarn("gripper opening doesn't match pre recorded width, multiple instruments might be picked")
                                rospy.logwarn("gripper opening doesn't match pre recorded width of FOR with orientation S, multiple instruments might be picked, opening detected: " + str(round(motion_result.gripper_opening, 1)) + "expeted size: " + str(instrument_width[globals.INSTRUMENT_DETECTED['name']]['S1'])+ "or " + str(instrument_width[globals.INSTRUMENT_DETECTED['name']]['S2']))
                                write_string += ' Wrong size detected'
                                transfer_back_pt1()
                                return 'retry'
                        elif globals.INSTRUMENT_DETECTED['name'] == "FOR" and globals.INSTRUMENT_DETECTED['orientation'] == 'F':
                            if round(motion_result.gripper_opening, 1) < instrument_width[globals.INSTRUMENT_DETECTED['name']]['F1'] or round(motion_result.gripper_opening, 1) > instrument_width[globals.INSTRUMENT_DETECTED['name']]['F2']:
                                # rospy.logwarn("gripper opening doesn't match pre recorded width, multiple instruments might be picked")
                                rospy.logwarn("gripper opening doesn't match pre recorded width of FOR with orientation F, multiple instruments might be picked, opening detected: " + str(round(motion_result.gripper_opening, 1)) + "expeted size: " + str(instrument_width[globals.INSTRUMENT_DETECTED['name']]['F1'])+ "or " + str(instrument_width[globals.INSTRUMENT_DETECTED['name']]['F2']))
                                write_string += ' Wrong size detected'
                                transfer_back_pt1()
                                return 'retry'
                        elif globals.INSTRUMENT_DETECTED['name'] == "RTL" and globals.INSTRUMENT_DETECTED['orientation'] == 'F':
                            if round(motion_result.gripper_opening, 1) != instrument_width[globals.INSTRUMENT_DETECTED['name']]['F1'] and round(motion_result.gripper_opening, 1) != instrument_width[globals.INSTRUMENT_DETECTED['name']]['F2']:
                                # rospy.logwarn("gripper opening doesn't match pre recorded width, multiple instruments might be picked")
                                rospy.logwarn("gripper opening doesn't match pre recorded width of RTL with orientation F, multiple instruments might be picked, opening detected: " + str(round(motion_result.gripper_opening, 1)) + "expeted size: " + str(instrument_width[globals.INSTRUMENT_DETECTED['name']]['F1'])+ "or " + str(instrument_width[globals.INSTRUMENT_DETECTED['name']]['F2']))
                                write_string += ' Wrong size detected'
                                transfer_back_pt1()
                                return 'retry'
                        elif globals.INSTRUMENT_DETECTED['name'] == "BAP" and globals.INSTRUMENT_DETECTED['orientation'] == 'F':
                            if round(motion_result.gripper_opening, 1) != instrument_width[globals.INSTRUMENT_DETECTED['name']]['F1'] and round(motion_result.gripper_opening, 1) != instrument_width[globals.INSTRUMENT_DETECTED['name']]['F2']:
                                # rospy.logwarn("gripper opening doesn't match pre recorded width, multiple instruments might be picked")
                                rospy.logwarn("gripper opening doesn't match pre recorded width of BAP with orientation F, multiple instruments might be picked, opening detected: " + str(round(motion_result.gripper_opening, 1)) + "expeted size: " + str(instrument_width[globals.INSTRUMENT_DETECTED['name']]['F1'])+ "or " + str(instrument_width[globals.INSTRUMENT_DETECTED['name']]['F2']))
                                write_string += ' Wrong size detected'
                                transfer_back_pt1()
                                return 'retry'
                        elif globals.INSTRUMENT_DETECTED['name'] == "TCJ" and globals.INSTRUMENT_DETECTED['orientation'] == 'F':
                            if round(motion_result.gripper_opening, 1) < instrument_width[globals.INSTRUMENT_DETECTED['name']]['F']:
                                # rospy.logwarn("gripper opening doesn't match pre recorded width, multiple instruments might be picked")
                                rospy.logwarn("gripper opening doesn't match pre recorded width of TCJ with orientation F, multiple instruments might be picked, opening detected: " + str(round(motion_result.gripper_opening, 1)) + "expeted size: greater than " + str(instrument_width[globals.INSTRUMENT_DETECTED['name']]['F']))
                                write_string += ' Wrong size detected'
                                transfer_back_pt1()
                                return 'retry'
                        elif globals.INSTRUMENT_DETECTED['name'] == "SPR" and globals.INSTRUMENT_DETECTED['orientation'] == 'F':
                            if round(motion_result.gripper_opening, 1) != instrument_width[globals.INSTRUMENT_DETECTED['name']]['F1'] and round(motion_result.gripper_opening, 1) != instrument_width[globals.INSTRUMENT_DETECTED['name']]['F2']:
                                # rospy.logwarn("gripper opening doesn't match pre recorded width, multiple instruments might be picked")
                                rospy.logwarn("gripper opening doesn't match pre recorded width of SPR with orientation F, multiple instruments might be picked, opening detected: " + str(round(motion_result.gripper_opening, 1)) + "expeted size: " + str(instrument_width[globals.INSTRUMENT_DETECTED['name']]['F1']) + " or " + str(instrument_width[globals.INSTRUMENT_DETECTED['name']]['F2']))
                                write_string += ' Wrong size detected'
                                transfer_back_pt1()
                                return 'retry'
                        elif globals.INSTRUMENT_DETECTED['name'] == "RUL":
                            pass
                        else:
                            if round(motion_result.gripper_opening, 1) != instrument_width[globals.INSTRUMENT_DETECTED['name']][globals.INSTRUMENT_DETECTED['orientation']]:
                                # rospy.logwarn("gripper opening doesn't match pre recorded width, multiple instruments might be picked" + str(round(motion_result.gripper_opening, 1)))
                                # rospy.logwarn("gripper opening doesn't match pre recorded width of " + str(globals.INSTRUMENT_DETECTED['name']) + ", multiple instruments might be picked, opening detected: " + str(round(motion_result.gripper_opening, 1)) + "expected size: " + str(instrument_width[globals.INSTRUMENT_DETECTED['name']]['F']))
                                rospy.logwarn("gripper opening doesn't match pre recorded width of " + str(globals.INSTRUMENT_DETECTED['name']) + ", multiple instruments might be picked, opening detected: " + str(round(motion_result.gripper_opening, 1)) + "expected size: " + str(instrument_width[globals.INSTRUMENT_DETECTED['name']][globals.INSTRUMENT_DETECTED['orientation']]))
                                write_string += ' Wrong size detected'
                                transfer_back_pt1()
                                return 'retry'
                        write_string += '\n'
                        try:
                            globals.F.write(write_string)
                        except:
                            print("file not initialised, ignoring")
                        ############ checking gripper opening ends
                    if check_drag_pick_suceess():
                        motion_result = RobotControlResult()
                        motion_goal.option = motion_goal.PLACE
                        if globals.INSTRUMENT_DETECTED['orientation'] != 'F':
                            motion_goal.target_pose = globals.OCM_CENTER_COOR
                            transfer_to_flipper = True
                            motion_goal.starting_location = "pt1_instrument_pap"
    
                        else:
                            while not check_ib_fully_ready():
                                sleep(1)
                            sleep(0.5)
                            if not adjust_belt_width(globals.INSTRUMENT_DETECTED['name']):
                                globals.LOGGING.logerr("Error during adjusting belt width, it this stuck or powered down?")
                                transfer_back_pt1()
                                return 'retry'
                            if globals.INSTRUMENT_DETECTED['name'] == 'TCJ':
                                motion_goal.target_pose = globals.list_to_posestamped(ib_tcj_input_placeholder)
                            else:
                                motion_goal.target_pose = globals.list_to_posestamped(ib_placeholder)
                            motion_goal.extra_zdown = 0.15
                        globals.MOTION_CLIENT.send_goal(motion_goal)
                        globals.MOTION_CLIENT.wait_for_result()
                        if globals.MOTION_CLIENT.get_result().success:
                            if transfer_to_flipper:
                                if globals.place_success_check("flipper"):
                                    rospy.loginfo("transfer is successful")
                                    return 'flip'
                                else:
                                    rospy.logwarn("transfer is NOT successful")
                                    return 'retry'
                            else:
                                if globals.place_success_check("ib"):
                                    rospy.loginfo("transfer is successful")
                                    return 'success'
                                else:
                                    rospy.logwarn("transfer is NOT successful")
                                    return 'retry'
                        else:
                            rospy.loginfo("error during placing")
                            transfer_back_pt1()
                            return 'retry'
                    else:
                        transfer_back_pt1()
                        return 'retry'
                else:
                    rospy.loginfo("error during picking")
                    transfer_back_pt1()
                    return 'retry'
            else:
                rospy.loginfo("error during gripper changing")
                return 'retry'
        else:
            rospy.loginfo("error during pt1 homing")
            return 'retry'

class TransferFromFlip(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'fail', 'ocm_faulty', 'item_missing', 'retry'])
    
    def execute(self, userdata):
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT > 6:
            globals.TRIAL_COUNT = 0
            return 'ocm_faulty'
        activate_ocm()
        sleep(0.5)
        ai_goal = AIServerGoal()
        ai_goal.task = ai_goal.PICK_FROM_FLIP
        ai_goal.which_module = ai_goal.PT1
        globals.AI_CLIENT.send_goal(ai_goal)
        ai_result = AIServerActionResult()
        globals.AI_CLIENT.wait_for_result()
        ai_result = globals.AI_CLIENT.get_result()
        if ai_result.topick:
            if ai_result.ai_output.item == 'ODD':
                rospy.logwarn("multiple item found on ocm, proceeding to clear it out")
                return 'ocm_faulty'
            elif ai_result.ai_output.item == 'BAP' or  ai_result.ai_output.item == "RUL":
                rospy.logwarn("bap or ruler found on ocm, proceeding to clear it out")
                return 'ocm_faulty'
            elif ai_result.ai_output.side != 'F':
                #instrument is not F, try one mroe time and will rotate the other way if still fail
                rospy.logwarn("orientation still incorrect, retrying")
                return 'retry' 
            picking_coor = get_depth(ai_result.ai_output.theta, ai_result.ai_output.u, ai_result.ai_output.v, ai_result.ai_output.theta-180)
            picking_coor.pose.position.z = 1.022 # was1.0265
            picking_coor.pose.position.y += 0.01
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.GO_TO_PT1_INSTRUMENT_PAP
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                motion_goal = RobotControlGoal()
                motion_goal.option = motion_goal.CHANGE_GRIPPER
                motion_goal.gripper = motion_goal.PARALLEL
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    motion_goal = RobotControlGoal()
                    motion_goal.option = motion_goal.PICK_SPECIAL
                    motion_goal.special_item = "pt1_picking"
                    motion_goal.target_pose = picking_coor
                    motion_goal.starting_location = "pt1_instrument_pap"
                    globals.MOTION_CLIENT.send_goal(motion_goal)
                    globals.MOTION_CLIENT.wait_for_result()
                    if globals.MOTION_CLIENT.get_result().success:
                        if check_drag_pick_suceess('flipper', ai_result.ai_output.item, ai_result.ai_output.u, ai_result.ai_output.v):
                            while not check_ib_fully_ready():
                                sleep(1)
                            sleep(0.5)
                            if not adjust_belt_width(globals.INSTRUMENT_DETECTED['name']):
                                globals.LOGGING.logerr("Error during adjusting belt width, it this stuck or powered down?")
                                transfer_back_pt1()
                                return 'fail'
                            motion_goal = RobotControlGoal()
                            motion_goal.option = motion_goal.PLACE
                            motion_goal.extra_zdown = 0.15
                            if globals.INSTRUMENT_DETECTED['name'] == 'TCJ':
                                motion_goal.target_pose = globals.list_to_posestamped(ib_tcj_input_placeholder)
                            else:
                                motion_goal.target_pose = globals.list_to_posestamped(ib_placeholder)
                            globals.MOTION_CLIENT.send_goal(motion_goal)
                            globals.MOTION_CLIENT.wait_for_result()
                            if globals.MOTION_CLIENT.get_result().success:
                                if globals.place_success_check("ib"):
                                    rospy.loginfo("transfer is successful")
                                    globals.TRIAL_COUNT = 0
                                    return 'success'
                                else:
                                    rospy.logwarn("transfer is NOT successful")
                                    return 'retry'
                            else:
                                rospy.loginfo("error during placing")
                                return 'retry'
                        else:
                            transfer_back_pt1()
                            if globals.FAULTY_OCM_INSTRUMENT['try_count'] >= 1:
                                globals.FAULTY_OCM_INSTRUMENT['name'] = ai_result.ai_output.item
                                rospy.logwarn("reached ocm try count of 2, signalling ocm faulty sequence")
                                return 'ocm_faulty'
                            else:
                                globals.FAULTY_OCM_INSTRUMENT['try_count'] += 1
                                rospy.logwarn("transfer from ocm unsuccessful, current trial count: "+  str(globals.FAULTY_OCM_INSTRUMENT['try_count']))
                            return 'retry'
                    else:
                        transfer_back_pt1()
                        rospy.loginfo("error during picking")
                        return 'retry'
                else:
                    rospy.loginfo("error during gripper changing")
                    return 'retry'
            else:
                rospy.loginfo("error during pt1 homing")
                return 'retry'
        else:
            rospy.loginfo("detect nothing on ocm, probably drop during trasnfer / successfully tranferred to pt1, exiting loop")
            return 'item_missing'    

class FaultyOCM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'retry'])
    
    def execute(self, userdata):
        rospy.logwarn("multiple item detected on ocm or ocm sequence returned failed several times, proceed to trasnfer them back to processing table or remove from ocm respectively")
        ai_goal = AIServerGoal()
        ai_goal.which_module = ai_goal.PT1
        ai_goal.task = ai_goal.PICK_FROM_FLIP
        ai_goal.instrument_needed = 'ODD'
        globals.AI_CLIENT.send_goal(ai_goal)
        globals.AI_CLIENT.wait_for_result()
        ai_result = AIServerResult()
        ai_result = globals.AI_CLIENT.get_result()
        if ai_result.topick:
            if globals.FAULTY_OCM_INSTRUMENT['try_count'] >= 2 or ai_result.ai_output.item == "BAP" or ai_result.ai_output.item == "RUL":
                #clearing item from ocm 
                rospy.logwarn("clearing ocm")
                drag_coor = get_depth(ai_result.ai_output.theta, ai_result.ai_output.u, ai_result.ai_output.v, ai_result.ai_output.theta-180)
                motion_goal = RobotControlGoal()
                motion_goal.option = motion_goal.CHANGE_GRIPPER
                motion_goal.gripper = motion_goal.DRAG
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    motion_goal.option = motion_goal.DRAG_TASK
                    motion_goal.target_pose = drag_coor
                    motion_goal.starting_location = "pt1_instrument_pap"
                    motion_goal.special_item = "faulty_ocm_instrument"
                    globals.MOTION_CLIENT.send_goal(motion_goal)
                    globals.MOTION_CLIENT.wait_for_result()
                    if globals.MOTION_CLIENT.get_result().success:
                        globals.FAULTY_OCM_INSTRUMENT = {'name':'', 'orientation':'', 'coor_to_go': PoseStamped(), 'drag_direction':'', 'u': 0, 'v':0, 'try_count': 0}
                        return 'retry'
            else:
                globals.FAULTY_OCM_INSTRUMENT['name'] = ai_result.ai_output.item
                globals.FAULTY_OCM_INSTRUMENT['u'] = ai_result.ai_output.u
                globals.FAULTY_OCM_INSTRUMENT['v'] = ai_result.ai_output.v
                globals.FAULTY_OCM_INSTRUMENT['orientation'] = ai_result.ai_output.theta
                globals.FAULTY_OCM_INSTRUMENT['coor'] = get_depth(ai_result.ai_output.theta, ai_result.ai_output.u, ai_result.ai_output.v, ai_result.ai_output.theta-180)
                if globals.FAULTY_OCM_INSTRUMENT['coor'] != None:
                    globals.FAULTY_OCM_INSTRUMENT['coor'].pose.position.z = 1.022
                    motion_goal = RobotControlGoal()
                    motion_goal.option = motion_goal.PICK_SPECIAL
                    motion_goal.special_item = "pt1_picking"
                    motion_goal.target_pose = globals.FAULTY_OCM_INSTRUMENT['coor']
                    motion_goal.starting_location = "pt1_instrument_pap"
                    globals.MOTION_CLIENT.send_goal(motion_goal)
                    globals.MOTION_CLIENT.wait_for_result()
                    if globals.MOTION_CLIENT.get_result().success:
                        if check_drag_pick_suceess('flipper', ai_result.ai_output.item, ai_result.ai_output.u, ai_result.ai_output.v):
                            transfer_back_pt1()
                            globals.FAULTY_OCM_INSTRUMENT = {'name':'', 'orientation':'', 'coor_to_go': PoseStamped(), 'drag_direction':'', 'u': 0, 'v':0, 'try_count': 0}
                            return 'retry'
                        else:
                            globals.FAULTY_OCM_INSTRUMENT['try_count'] += 1
                            return 'retry'
                    else:
                        transfer_back_pt1()
                        rospy.logwarn("error during picking")
                        return 'retry'
                else:
                    rospy.logwarn("pcl unable to get coordinate")
                    return 'retry'
        else:
            rospy.loginfo("no thing left on ocm, success and exiting")
            return 'success'

class StartIB(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])
    
    def execute(self, userdata):
        rospy.loginfo("ping ib to start inspection task") 
        if IGNORE_IB:
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.GO_TO_PT1_INSTRUMENT_PAP
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                rospy.loginfo("returned to pt1 pap")
            return 'success'
        ib1_goal = IB1ServerGoal()
        ib1_goal.task = ib1_goal.INSPECT
        # ib1_goal.tool_loaded = globals.INSTRUMENT_DETECTED['name']      #this line is not needed during actual ib as inspection sequence is determined by top caemra ai
        globals.IB1_CLIENT.send_goal(ib1_goal)
        globals.IB_STATUS['ready'] = False
        globals.IB_STATUS['time_last_load'] = time.time()
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.GO_TO_PT1_INSTRUMENT_PAP
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            rospy.loginfo("returned to pt1 pap")
        return 'success'

class GetMoreInstrument(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'ended', 'fail', 'retry'])
    
    def execute(self, userdata):
        if globals.KIDNEY_DISH_EMPTY:
            try:
                globals.F.close()
            except:
                print("file not initialised, ignoring")
            globals.LOGGING.loginfo("All tools on PT1 have been processed")
            globals.PROCESS_COMPLETED = 6
            return 'ended'
        globals.LOGGING.loginfo("First half if instruments has been processed, transferring the second half")
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.CHANGE_GRIPPER
        motion_goal.gripper = motion_goal.PARALLEL
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.GO_TO_DRYBAY
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                motion_goal.option = motion_goal.PICK
                motion_goal.target_pose = globals.list_to_posestamped(half_kidney_dish_picking_coor)
                motion_goal.starting_location = "drybay"
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    motion_goal = RobotControlGoal()
                    motion_goal.option = motion_goal.GO_TO_PT1
                    globals.MOTION_CLIENT.send_goal(motion_goal)
                    globals.MOTION_CLIENT.wait_for_result()
                    while not update_pt1_and_ocm_center():
                        os.system("rosservice call /ur_hardware_interface/set_io 1 " + str(rotating_table_pin) + " 1")
                        sleep(1)
                        os.system("rosservice call /ur_hardware_interface/set_io 1 " + str(rotating_table_pin) + " 0")
                        sleep(1)
                    if globals.MOTION_CLIENT.get_result().success:
                        motion_goal = RobotControlGoal()
                        motion_goal.option = motion_goal.PLACE_SPECIAL
                        motion_goal.target_pose = globals.PT1_CENTER_COOR
                        motion_goal.target_pose.pose.position.y -= 0.08
                        motion_goal.target_pose.pose.position.z = 1.23
                        motion_goal.special_item = "kidney_dish"
                        motion_goal.extra_argument = "2"
                        globals.MOTION_CLIENT.send_goal(motion_goal)
                        globals.MOTION_CLIENT.wait_for_result()
                        if globals.MOTION_CLIENT.get_result().success:
                            # dispensing instrument end, placing at waiting area
                            motion_goal = RobotControlGoal()
                            motion_goal.option = motion_goal.GO_TO_DRYBAY
                            globals.MOTION_CLIENT.send_goal(motion_goal)
                            globals.MOTION_CLIENT.wait_for_result()
                            if globals.MOTION_CLIENT.get_result().success:
                                motion_goal.option = motion_goal.PLACE
                                motion_goal.target_pose = globals.list_to_posestamped(kidney_dish_placing_coor)
                                motion_goal.starting_location = "drybay"
                                globals.MOTION_CLIENT.send_goal(motion_goal)
                                globals.MOTION_CLIENT.wait_for_result()
                                if globals.MOTION_CLIENT.get_result().success:
                                    rospy.loginfo("kidney dish transfer is successful")
                                    globals.KIDNEY_DISH_EMPTY = True
                                    globals.LOGGING.loginfo("Second half instrument transfer is successful")
                                    return "success"
                                else:
                                    transfer_back_pt1()
                                    globals.LOGGING.logerr("Something is wrong during Kidney Dish placing, process can't be rectifified, please assist to place the Kidney Dish on Flip Bay")
                                    return "fail"
                            else:
                                transfer_back_pt1()
                                globals.LOGGING.logerr("Error during DryBay homing, process can't be rectifified, please assist to place the Kidney Dish on Flip Bay")
                                return "fail"
                        else:
                            transfer_back_pt1()
                            globals.LOGGING.logerr("Error during Kidney Dish dumping, process can't be rectifified, please assist to dump remaining instrument on Processing Table and place the Kidney Dish on Flip Bay")
                            return "fail"
                    else:
                        transfer_back_pt1()
                        globals.LOGGING.logerr("Error during PT1 homing, process can't be rectifified, please assist to dump remaining instrument on Processing Table and place the Kidney Dish on Flip Bay")
                        return "fail"
                else:
                    transfer_back_pt1()
                    globals.LOGGING.logerr("Error during Kidney Dish picking, retrying")
                    return "retry"
            else:
                globals.LOGGING.logerr("Error during Drybay homing, retrying")
                return "retry"
        else:
            globals.LOGGING.logerr("Error during gripper changing, retrying")
            return "retry"
                                

class ReplenishFromSpareAndTransferIndicator(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail','retry'])
    
    def execute(self, userdata):
        globals.PROCESS_COMPLETED = 7
        return 'success'
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT > 3:
            globals.TRIAL_COUNT = 0
            return 'fail'
        globals.LOGGING.loginfo("Transferring indicator and spares instruments (if any)...")

        while not check_ib_fully_ready():
            sleep(1)
            
        if not adjust_belt_width('IND'):
            globals.LOGGING.logerr("Error during adjusting belt width, it this stuck or powered down?")
            return 'retry'

        if not globals.HAS_GET_REPLENISH_ITEMLISTS or globals.ITEM_TO_REPLENISH == None:
            globals.LOGGING.loginfo("Getting defective / missing item from inspection bay")
            ib1_goal = IB1ServerGoal()
            ib1_goal.task = ib1_goal.GET_DEFECTIVE_TOOL_LIST
            globals.IB1_CLIENT.send_goal(ib1_goal)
            globals.IB1_CLIENT.wait_for_result()
            globals.ITEM_TO_REPLENISH = ast.literal_eval(globals.IB1_CLIENT.get_result().replenish_tool)
            globals.LOGGING.loginfo("Item to be refill: " + str(globals.ITEM_TO_REPLENISH) + ", pinging spare bay to release items")

        # globals.PROCESS_COMPLETED = 6
        # return 'success'


        #processing retractor replenishing and wrapping
        if not globals.RTL_WRAPPED: 
            if globals.ITEM_TO_REPLENISH['RTL1'] > 0:   #replenish adult retractors
                globals.plc_task("nrsb_ext")
                globals.LOGGING.loginfo("Pinging Non Ring Spare Bay to dispense " + str(globals.ITEM_TO_REPLENISH['RTL1']) + " Retractor(s) - Adult" )
                if requests.post(globals.NON_RING_SPARE_BAY_URL, json={'RTL1': globals.ITEM_TO_REPLENISH['RTL1']}).status_code == 200:
                    globals.LOGGING.loginfo("Successfully replenish Retractors")
                    globals.ITEM_TO_REPLENISH['RTL1'] = 0
                else:
                    globals.LOGGING.logerr("Error during Retractors replenishment, retrying")
                    return 'retry'
            if globals.ITEM_TO_REPLENISH['RTL2'] > 0:   #replenish baby retractors
                globals.plc_task("nrsb_ext")
                globals.LOGGING.loginfo("Pinging Non Ring Spare Bay to dispense " + str(globals.ITEM_TO_REPLENISH['RTL2']) + " Retractor(s) - Baby" )
                if requests.post(globals.NON_RING_SPARE_BAY_URL, json={'RTL2': globals.ITEM_TO_REPLENISH['RTL2']}).status_code == 200:
                    globals.LOGGING.loginfo("Successfully replenish Retractors")
                    globals.ITEM_TO_REPLENISH['RTL2'] = 0
                else:
                    globals.LOGGING.logerr("Error during Retractors replenishment, retrying")
                    return 'retry'
            if release_and_wrap("RTL"):
                globals.LOGGING.loginfo("Wrapping Bay have finished Retractors process successfully")
                globals.TRIAL_COUNT = 0
                globals.RTL_WRAPPED = True
            else:
                return 'fail'

        #processing towel clip jones replenishing and wrapping
        if not globals.TCJ_WRAPPED:
            if globals.ITEM_TO_REPLENISH['TCJ'] > 0:
                globals.plc_task("nrsb_ext")
                globals.LOGGING.loginfo("Pinging Non Ring Spare Bay to dispense " + str(globals.ITEM_TO_REPLENISH['TCJ']) + " Towel Clip Jones(s)" )
                if requests.post(globals.NON_RING_SPARE_BAY_URL, json={'TCJ': globals.ITEM_TO_REPLENISH['TCJ']}).status_code == 200:
                    globals.LOGGING.loginfo("Successfully replenish Towel Clip Jones")
                    globals.ITEM_TO_REPLENISH['TCJ'] = 0
                else:
                    globals.LOGGING.logerr("Error during Towel Clip Jones replenishment, retrying")
                    return 'retry'

            if not globals.get_indicator():
                return 'retry'
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.GO_TO_PT1
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                if indicator_release("TCJ"):
                    while not check_ib_fully_ready():
                        sleep(1)
                    if release_and_wrap("TCJ"):
                        globals.LOGGING.loginfo("Wrapping Bay have finished Towel Clip Jones process successfully")
                        globals.TRIAL_COUNT = 0
                        globals.TCJ_WRAPPED = True
                    else:
                        return 'fail'
                else:
                    return 'retry'
            else:
                globals.LOGGING.logerr("Error during PT1 homing, retrying")
                return 'retry'
        
        #processing loose instrument replenishing and wrapping
        if not globals.LOS_WRAPPED:
            if globals.ITEM_TO_REPLENISH['RUL'] > 0 or globals.ITEM_TO_REPLENISH['BAP'] > 0 or globals.ITEM_TO_REPLENISH['SPR'] > 0 or globals.ITEM_TO_REPLENISH['FOR1'] > 0 or globals.ITEM_TO_REPLENISH['FOR2'] > 0:
                globals.plc_task("nrsb_ext")
                for x in globals.ITEM_TO_REPLENISH:
                    if x != 'RUL' and  x != 'TCJ' and  x != 'RTL1' and  x != 'RTL2':
                        if globals.ITEM_TO_REPLENISH[x] > 0:
                            globals.LOGGING.loginfo("Pinging Non Ring Spare Bay to dispense " + str(globals.ITEM_TO_REPLENISH[x]) + " " + str(x) )
                            if requests.post(globals.NON_RING_SPARE_BAY_URL, json={x: globals.ITEM_TO_REPLENISH[x]}).status_code == 200:
                                globals.LOGGING.loginfo("Successfully replenish " + str(x))
                                globals.ITEM_TO_REPLENISH[x] = 0
                            else:
                                globals.LOGGING.logerr("Error during Loose instruments replenishment, retrying")
                                return 'retry'

                if globals.ITEM_TO_REPLENISH['RUL'] > 0:
                    globals.LOGGING.loginfo("Replenishing " + str(globals.ITEM_TO_REPLENISH['RUL']) + " Ruler from Non Ring Spare Bay")
                    motion_goal = RobotControlGoal()
                    motion_goal.option = motion_goal.CHANGE_GRIPPER
                    motion_goal.gripper = motion_goal.SUCTION
                    globals.MOTION_CLIENT.send_goal(motion_goal)
                    globals.MOTION_CLIENT.wait_for_result()
                    if globals.MOTION_CLIENT.get_result().success:
                        motion_goal = RobotControlGoal()
                        motion_goal.option = motion_goal.GO_TO_PT1
                        globals.MOTION_CLIENT.send_goal(motion_goal)
                        globals.MOTION_CLIENT.wait_for_result()
                        if globals.MOTION_CLIENT.get_result().success:
                            motion_goal = RobotControlGoal()
                            motion_goal.option = motion_goal.PICK
                            motion_goal.target_pose = globals.list_to_posestamped(sparebay_ruler_placeholder)
                            globals.MOTION_CLIENT.send_goal(motion_goal)
                            globals.MOTION_CLIENT.wait_for_result()
                            if globals.MOTION_CLIENT.get_result().success:
                                motion_goal = RobotControlGoal()
                                motion_goal.option = motion_goal.PLACE
                                motion_goal.target_pose = globals.list_to_posestamped(sparebay_tool_release_placeholder)
                                motion_goal.starting_location = "pt1"
                                globals.MOTION_CLIENT.send_goal(motion_goal)
                                globals.MOTION_CLIENT.wait_for_result()
                                if globals.MOTION_CLIENT.get_result().success:
                                    globals.LOGGING.loginfo("Ruler replenishment is successful!")
                                    globals.ITEM_TO_REPLENISH['RUL'] = 0
                                else:
                                    globals.LOGGING.logerr("Ruler transfer is not successful, retrying")
                                    return 'retry'
                            else:
                                globals.LOGGING.logerr("Something is wrong during Ruler picking")
                                return 'retry'
                        else:
                            globals.LOGGING.logerr("Something is wrong during PT1 homing")
                            return 'retry'
                    else:
                        globals.LOGGING.logerr("Something is wrong during gripper changing")
                        return 'retry'
            
            if not globals.get_indicator():
                return 'retry'
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.GO_TO_PT1
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                if indicator_release("LOS"):
                    while not check_ib_fully_ready():
                        sleep(1)
                    if release_and_wrap("LOS"):
                        globals.LOGGING.loginfo("Wrapping Bay have finished Loose Instrument process successfully")
                        globals.TRIAL_COUNT = 0
                        globals.LOS_WRAPPED = True
                    else:
                        return 'fail'
                else:
                    return 'retry'
            else:
                globals.LOGGING.logerr("Error during PT1 homing, retrying")
                return 'retry'

        globals.plc_task("nrsb_ret")
        globals.PROCESS_COMPLETED = 7
        return 'success'