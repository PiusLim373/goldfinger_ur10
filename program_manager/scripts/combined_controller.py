#!/usr/bin/env python

import rospy
import requests
import datetime

from roswebui.msg import *
from ur10_pap.msg import *
from pcl_server.msg import *
from ai_server.msg import *
from plc_server.srv import *
from ib1_server.msg import *

import actionlib
import smach
import smach_ros
import datetime
import globals
from wetbay_function import *
from pt1_function import *
from drybay_function import *


######################################################################### common function ###########################################333
def initialization():
    server_connected = 0

    globals.WEBUI_CLIENT = actionlib.SimpleActionClient('webui_handler', WebUIAction)
    if globals.WEBUI_CLIENT.wait_for_server(rospy.Duration(1)):
        rospy.loginfo("roswebui server connected")
        server_connected += 1

    globals.MOTION_CLIENT = actionlib.SimpleActionClient('motion_server', RobotControlAction)
    if globals.MOTION_CLIENT.wait_for_server(rospy.Duration(1)):
        rospy.loginfo("motion server connected")
        server_connected += 1

    globals.AI_CLIENT = actionlib.SimpleActionClient('ai_server', AIServerAction)
    if globals.AI_CLIENT.wait_for_server(rospy.Duration(1)):
        rospy.loginfo("ai server connected")
        server_connected += 1
    
    globals.PCL_CLIENT = actionlib.SimpleActionClient('pcl_server', PCLServerAction)
    if globals.PCL_CLIENT.wait_for_server(rospy.Duration(1)):
        rospy.loginfo("pcl server connected")
        server_connected += 1
    
    globals.PLC_CLIENT = rospy.ServiceProxy('plc_server', PLCService)
    try:
        rospy.wait_for_service('plc_server', rospy.Duration(1))
        rospy.loginfo("plc server connected")
        server_connected += 1
    except:
        pass
    
    globals.IB1_CLIENT = actionlib.SimpleActionClient('ib1_server', IB1ServerAction)
    if globals.IB1_CLIENT.wait_for_server(rospy.Duration(1)):
        rospy.loginfo("ib1 server connected")
        server_connected += 1

    if server_connected == 6:
        globals.LOGGING.loginfo("All servers connected, system ready")
    
    else:
        globals.LOGGING.logerr(str(server_connected)+ " out of  5 servers are connected, system not fully ready")

class RequestHumanIntervention(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['database', 'bracket', 'gallipot', 'kidney_dish', 'tray', \
                                            'pt1', 'sparebay', \
                                            'tray_detection', 'indicator', \
                                            'forcep_brac', 'mix_brac', 'drybay_gallipot', 'drybay_kidney_dish', 'drybay_wbt', \
                                            # 'drybay_printedlist', 'drybay_stickertag',\
                                            'reset_temp_brac'])
    
    def execute(self, userdata):
        '''signalling frontend and wait for return, after return, will pick up from what's left'''
        print("current process completed: ", globals.PROCESS_COMPLETED)
        ################################################################################################################ WETBAY Recovery
        if globals.PROCESS_COMPLETED < 5:
            #retract extended drawer and change LED to red
            globals.plc_task("drawer_ret_wetbay", globals.PIGEONHOLE_PROCESSING)
            globals.plc_task("drawer_err_wetbay", globals.PIGEONHOLE_PROCESSING)
            if globals.PROCESS_COMPLETED == 0:
                #error during database cretion
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Bracket processing"})

            elif globals.PROCESS_COMPLETED == 1:
                #error during bracket transfer
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Tray detection"})
            
            elif globals.PROCESS_COMPLETED == 2:
                #error during gallipot transfer, possible error: gallipot not loaded/seen, container not seen, protective stop triggerred during transfer
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Gallipot processing"})
            
            elif globals.PROCESS_COMPLETED == 3:
                #error during kidney dish transfer, possible error: kidneydish not loaded/seen, protective stop triggerred during transfer
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Kidney Dish processing"})
            
            elif globals.PROCESS_COMPLETED == 4:
                globals.plc_task("drawer_ret_drybay", globals.PIGEONHOLE_PROCESSING)
                sleep(0.1)
                globals.plc_task("drawer_err_drybay", globals.PIGEONHOLE_PROCESSING)
                #error during tray transfer, possible error: tray not loaded/seen, container not seen, protective stop triggerred during transfer
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Tray processing"})

            error_solved = False
            while not error_solved:
                #keep pooling until error is solved
                error_solved = requests.get(globals.ERROR_RECTIFICATION_URL).json()['error_solved']
                sleep(1)

            globals.plc_task("drawer_rec_wetbay", globals.PIGEONHOLE_PROCESSING)
            sleep(5) #wait for drawer to fully extend
            if globals.PROCESS_COMPLETED == 0:
                return 'bracket'

            elif globals.PROCESS_COMPLETED == 1:
                return 'database'
            
            elif globals.PROCESS_COMPLETED == 2:
                return 'gallipot'
            
            elif globals.PROCESS_COMPLETED == 3:
                return 'kidney_dish'
            
            elif globals.PROCESS_COMPLETED == 4:
                globals.plc_task("drawer_rec_drybay", globals.PIGEONHOLE_PROCESSING)
                return 'tray'
        ################################################################################################################ WETBAY Recovery Ends
        ################################################################################################################ PT1 Recovery
        elif globals.PROCESS_COMPLETED < 7 or globals.PROCESS_COMPLETED == 16:

            #lights up all drawer red
            globals.plc_task("all_drawer_err")

            # send error to frontend
            if globals.PROCESS_COMPLETED == 5:
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Processing Table 1 Picking"})
            elif globals.PROCESS_COMPLETED == 6:
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Non-Ring Spare Bay Replenishment"})
            elif globals.PROCESS_COMPLETED == 16:
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Resetting Temp Bracket Position"})

            error_solved = False
            while not error_solved:
                #keep pooling until error is solved
                error_solved = requests.get(globals.ERROR_RECTIFICATION_URL).json()['error_solved']
                sleep(1)
            globals.plc_task("all_drawer_rec")

            if globals.PROCESS_COMPLETED == 5:
                return 'pt1'
            
            elif globals.PROCESS_COMPLETED == 6:
                return 'sparebay'
            
            elif globals.PROCESS_COMPLETED == 16:
                return 'reset_temp_brac'
        ################################################################################################################ PT1 Recovery Ends
        ################################################################################################################ Drybay Recovery
        else:
            globals.plc_task("drawer_err_drybay", globals.PIGEONHOLE_PROCESSING)
            sleep(0.1)
            globals.plc_task("drawer_ret_drybay", globals.PIGEONHOLE_PROCESSING)

            if globals.PROCESS_COMPLETED == 7:
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Tray Paper"})
            elif globals.PROCESS_COMPLETED == 8:
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Indicator Placement"})
            elif globals.PROCESS_COMPLETED == 9:
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Forcep Bracket Placement"})
            elif globals.PROCESS_COMPLETED == 10:
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Mixed Bracket Placement"})
            elif globals.PROCESS_COMPLETED == 11:
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Gallipot Placement"})
            elif globals.PROCESS_COMPLETED == 12:
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Kidney Dish Placement"})
            elif globals.PROCESS_COMPLETED == 13:
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Wrapping Bay Tray Placement"})
            elif globals.PROCESS_COMPLETED == 14:
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Wrapped Printed List Placement"})
            elif globals.PROCESS_COMPLETED == 15:
                requests.post(globals.ERROR_RECTIFICATION_URL, json={"error":True, "error_message": "Sticker Tag Placement"})
           
            
            error_solved = False
            while not error_solved:
                #keep pooling until error is solved
                error_solved = requests.get(globals.ERROR_RECTIFICATION_URL).json()['error_solved']
                sleep(1)
            globals.plc_task("drawer_rec_drybay", globals.PIGEONHOLE_PROCESSING)
            sleep(5) #wait for drawer to fully extend

            if globals.PROCESS_COMPLETED == 7:
                return 'tray_detection'
            elif globals.PROCESS_COMPLETED == 8:
                return 'indicator'
            
            #9 onwards, after human intervention, it wont retry the function again, as this has no continuity
            elif globals.PROCESS_COMPLETED == 9:
                globals.PROCESS_COMPLETED = 10
                return 'mix_brac'
            elif globals.PROCESS_COMPLETED == 10:
                globals.PROCESS_COMPLETED = 11
                return 'drybay_gallipot'
            elif globals.PROCESS_COMPLETED == 11:
                globals.PROCESS_COMPLETED = 12
                return 'drybay_kidney_dish'
            elif globals.PROCESS_COMPLETED == 12:
                globals.PROCESS_COMPLETED = 13
                return 'drybay_wbt'
            elif globals.PROCESS_COMPLETED == 13:
                globals.PROCESS_COMPLETED = 14
                return 'drybay_wbt'
            elif globals.PROCESS_COMPLETED == 14:
                globals.PROCESS_COMPLETED = 15
                return 'drybay_printedlist'
            elif globals.PROCESS_COMPLETED == 15:
                globals.PROCESS_COMPLETED = 16
                return 'drybay_stickertag'
        ################################################################################################################ Drybay Recovery Ends


######################################################################### DEBUG FUNCTION ###########################################333
class DebugRouterSM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['wetbay','pt1', 'drybay', 'debug'])
    
    def execute(self, userdata):
        # return 'debug'
        return 'wetbay'
        #return 'pt1'
        globals.PIGEONHOLE_PROCESSING = globals.PIGEONHOLE_TO_PROCESS[globals.PIGEONHOLE_PROCESSING_INDEX]
        globals.LOGGING.loginfo("Processing pigeonhole: " + str(globals.PIGEONHOLE_PROCESSING))
        return 'drybay'

class ExtendWetBay(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail', 'debug'])
    
    def execute(self, userdata):
        globals.PIGEONHOLE_PROCESSING = globals.PIGEONHOLE_TO_PROCESS[globals.PIGEONHOLE_PROCESSING_INDEX]
        globals.LOGGING.loginfo("Processing pigeonhole: " + str(globals.PIGEONHOLE_PROCESSING))
        globals.plc_task("drawer_ext_wetbay", globals.PIGEONHOLE_PROCESSING)
        sleep(10)
        return 'success'

class CheckAnymoreToProcess(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail'])
    
    def execute(self, userdata):
        globals.PROCESS_COMPLETED = 0
        globals.TRIAL_COUNT = 0
        globals.wetbay_var_init()
        globals.pt1_var_init()
        globals.drybay_var_init()
        globals.plc_task("drawer_ret_wetbay", globals.PIGEONHOLE_PROCESSING)        #to remove this, putting here for debugging
        globals.plc_task("drawer_ret_drybay", globals.PIGEONHOLE_PROCESSING)        #to remove this, putting here for debugging
        if globals.PIGEONHOLE_PROCESSING_INDEX < len(globals.PIGEONHOLE_TO_PROCESS) - 1:
            globals.LOGGING.loginfo("still pigeonhole to process")
            globals.PIGEONHOLE_PROCESSING_INDEX += 1
            return 'fail'
        else:
            globals.LOGGING.loginfo("all pigeonhole has fnished processing")
            globals.LOGGING.loginfo("Main program has completed, terminating...")
            globals.plc_task("debug_system_reset")
            requests.get(globals.PROGRAM_MANAGER_TERMINATION_URL)
            return 'success'

class CreateGripperOpeningLog(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'activate_rotating_table', 'fail', 'debug'])
    
    def execute(self, userdata):
        now = datetime.datetime.now()
        dt_string = now.strftime("%Y%m%d_%H%M")
        globals.F = open("/home/ur10controller/ai_outputs/pt1/gripper_opening_"+ dt_string, "a")
        return 'activate_rotating_table'
        for i in range(3):
            rospy.loginfo("updating pt1 centre")
            if update_pt1_center():
                print(globals.PT1_CENTER_COOR)
                if globals.OCM_CENTER_COOR == None:
                    update_ocm_center()
                break
        return 'success'
        # return 'debug'
        
class Debug(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail'])
    
    def execute(self, userdata):
        globals.PIGEONHOLE_PROCESSING = globals.PIGEONHOLE_TO_PROCESS[globals.PIGEONHOLE_PROCESSING_INDEX]
        globals.LOGGING.loginfo("Processing pigeonhole: " + str(globals.PIGEONHOLE_PROCESSING))
        globals.plc_task("drawer_err_drybay", globals.PIGEONHOLE_PROCESSING)
        
        globals.plc_task("drawer_ret_drybay", globals.PIGEONHOLE_PROCESSING)
        raw_input("enter to fix error")
        
        globals.plc_task("drawer_rec_drybay", globals.PIGEONHOLE_PROCESSING)
        return 'success'

######################################################################### DEBUG FUNCTION END #######################################333

def main():
    try:
        sm = smach.StateMachine(outcomes=['ended'])
        with sm:
            smach.StateMachine.add('DebugRouterSM', DebugRouterSM(), transitions={'wetbay':'ExtendWetBay', 'pt1':'CreateGripperOpeningLog', 'drybay':'ReplenishFromSpareAndTransferIndicator', 'debug': 'Debug'})
            
            smach.StateMachine.add('ExtendWetBay', ExtendWetBay(), transitions={'success':'TransferBracketSM', 'fail':'ended', 'debug':'Debug'})
            smach.StateMachine.add('TransferBracketSM', TransferBracketSM(), transitions={'success':'InitialiseDB', 'retry':'TransferBracketSM', 'partially_success':'TransferBracketSM', 'half_reso':'TransferBracketSM', 'fail':'RequestHumanIntervention'})
            smach.StateMachine.add('InitialiseDB', InitialiseDB(), transitions={'success':'StartUR5ProcessSM', 'retry':'RequestHumanIntervention'})
            smach.StateMachine.add('StartUR5ProcessSM', StartUR5ProcessSM(), transitions={'success':'TransferGallipotSM'})
            smach.StateMachine.add('TransferGallipotSM', TransferGallipotSM(), transitions={'success':'TransferKidneyDishSM', 'retry':'TransferGallipotSM', 'fail':'RequestHumanIntervention'})
            smach.StateMachine.add('TransferKidneyDishSM', TransferKidneyDishSM(), transitions={'success':'TransferTraySM', 'retry':'TransferKidneyDishSM', 'fail':'RequestHumanIntervention'})
            smach.StateMachine.add('TransferTraySM', TransferTraySM(), transitions={'success':'CreateGripperOpeningLog', 'retry':'TransferTraySM', 'fail':'RequestHumanIntervention'})
            
            smach.StateMachine.add('CreateGripperOpeningLog', CreateGripperOpeningLog(), transitions={'success':'InitialiseIB', 'activate_rotating_table': 'InitialiseIB','fail':'ended', 'debug': 'Debug'})
            smach.StateMachine.add('InitialiseIB', InitialiseIB(), transitions={'success':'ActivateRotatingTable', 'fail':'RequestHumanIntervention'})
            smach.StateMachine.add('ActivateRotatingTable', ActivateRotatingTable(), transitions={'success':'QueryIB', 'retry': 'ActivateRotatingTable'})
            smach.StateMachine.add('QueryIB', QueryIB(), transitions={'success':'QueryVision', 'fail':'RequestHumanIntervention'})
            smach.StateMachine.add('QueryVision', QueryVision(), transitions={'success':'TransferInst', 'retry':'QueryVision', 'activate_rotating_table':'ActivateRotatingTable', 'drag':'DragInst', 'wait':'QueryIB', 'get_more_inst':'GetMoreInstrument'})
            smach.StateMachine.add('DragInst', DragInst(), transitions={'success':'QueryIB', 'retry':'QueryIB'})
            smach.StateMachine.add('TransferInst', TransferInst(), transitions={'success':'StartIB', 'retry':'QueryVision', 'flip':'TransferFromFlip'})
            smach.StateMachine.add('TransferFromFlip', TransferFromFlip(), transitions={'success':'StartIB', 'retry':'TransferFromFlip', 'item_missing': 'QueryVision', 'ocm_faulty': 'FaultyOCM', 'fail':'RequestHumanIntervention'})
            smach.StateMachine.add('StartIB', StartIB(), transitions={'success':'QueryIB'})
            smach.StateMachine.add('GetMoreInstrument', GetMoreInstrument(), transitions={'success':'ActivateRotatingTable', 'ended': 'ReplenishFromSpareAndTransferIndicator', 'fail':'RequestHumanIntervention', 'retry':'GetMoreInstrument'})
            smach.StateMachine.add('FaultyOCM', FaultyOCM(), transitions={'success':'QueryVision', 'retry': 'FaultyOCM'})
            smach.StateMachine.add('ReplenishFromSpareAndTransferIndicator', ReplenishFromSpareAndTransferIndicator(), transitions={'success':'PlaceTrayPaperSM', 'fail': 'RequestHumanIntervention', 'retry': 'ReplenishFromSpareAndTransferIndicator' })
            
            smach.StateMachine.add('PlaceTrayPaperSM', PlaceTrayPaperSM(), transitions={'success':'RequestHumanIntervention'})
            smach.StateMachine.add('GetTrayDetailsSM', GetTrayDetailsSM(), transitions={'success':'TransferIndicatorSM', 'fail':'CheckAnymoreToProcess', 'retry': 'GetTrayDetailsSM'})
            smach.StateMachine.add('TransferIndicatorSM', TransferIndicatorSM(), transitions={'success':'TransferForcepBrac', 'fail':'ended', 'retry':'TransferIndicatorSM'})
            smach.StateMachine.add('QueryUR5ProcessSM', QueryUR5ProcessSM(), transitions={'success':'TransferForcepBrac'})
            smach.StateMachine.add('TransferForcepBrac', TransferForcepBrac(), transitions={'success':'TransferMixBrac', 'retry':'TransferForcepBrac', 'fail':'TransferMixBrac'})
            smach.StateMachine.add('TransferMixBrac', TransferMixBrac(), transitions={'success':'TransferGallipotDBSM', 'retry':'TransferMixBrac', 'fail':'RequestHumanIntervention'})
            smach.StateMachine.add('TransferGallipotDBSM', TransferGallipotDBSM(), transitions={'success':'TransferKidneyDishDBSM', 'retry':'TransferGallipotDBSM', 'fail':'RequestHumanIntervention'})
            smach.StateMachine.add('TransferKidneyDishDBSM', TransferKidneyDishDBSM(), transitions={'success':'ReleaseCoverAndRetract', 'retry':'TransferKidneyDishDBSM', 'fail':'RequestHumanIntervention'})
            smach.StateMachine.add('TransferWrappingBayTraySM', TransferWrappingBayTraySM(), transitions={'success':'ReleaseCoverAndRetract', 'retry':'TransferWrappingBayTraySM', 'fail': 'RequestHumanIntervention'})
            # smach.StateMachine.add('TransferPrintedListSM', TransferPrintedListSM(), transitions={'success':'TransferStickerSM', 'retry':'TransferPrintedListSM', 'fail': 'RequestHumanIntervention'})
            # smach.StateMachine.add('TransferStickerSM', TransferStickerSM(), transitions={'success':'ReleaseCoverAndRetract', 'retry':'TransferStickerSM', 'fail': 'RequestHumanIntervention'})
            
            smach.StateMachine.add('ReleaseCoverAndRetract', ReleaseCoverAndRetract(), transitions={'success':'CheckAnymoreToProcess'})
            smach.StateMachine.add('ResetTempBracPositionSM', ResetTempBracPositionSM(), transitions={'success':'CheckAnymoreToProcess', 'retry': 'ResetTempBracPositionSM', 'fail': 'RequestHumanIntervention'})

            smach.StateMachine.add('CheckAnymoreToProcess', CheckAnymoreToProcess(), transitions={'success':'ended', 'fail':'ExtendWetBay'})
            smach.StateMachine.add('RequestHumanIntervention', RequestHumanIntervention(), transitions={\
                                                                                                        'database': 'InitialiseDB',\
                                                                                                        'bracket': 'TransferBracketSM', \
                                                                                                        'gallipot': 'TransferGallipotSM', \
                                                                                                        'kidney_dish': 'TransferKidneyDishSM', \
                                                                                                        'tray': 'TransferTraySM', \
                                                                                                        'pt1':'InitialiseIB', \
                                                                                                        'sparebay':'ReplenishFromSpareAndTransferIndicator',\
                                                                                                        'tray_detection':'GetTrayDetailsSM' , \
                                                                                                        'indicator': 'TransferIndicatorSM', \
                                                                                                        'forcep_brac': 'TransferForcepBrac', \
                                                                                                        'mix_brac': 'TransferMixBrac', \
                                                                                                        'drybay_gallipot':'TransferGallipotDBSM', \
                                                                                                        'drybay_kidney_dish': 'TransferKidneyDishDBSM',\
                                                                                                        'drybay_wbt': 'TransferWrappingBayTraySM', \
                                                                                                        # 'drybay_printedlist':'TransferPrintedListSM', \
                                                                                                        # 'drybay_stickertag': 'TransferStickerSM'\
                                                                                                        'reset_temp_brac': 'ResetTempBracPositionSM'\
                                                                                                        })
            
            # smach.StateMachine.add('Debug', Debug(), transitions={'success':'ended', 'fail':'ended'})
            smach.StateMachine.add('Debug', WetbayDebugFunc(), transitions={'success':'ended'})

        smach_viewer = smach_ros.IntrospectionServer('ur10_smach_controller', sm, '/Start')
        smach_viewer.start()
        outcome = sm.execute()
        rospy.spin()
        smach_viewer.stop()
    except rospy.ROSInterruptException:
        return 0
    except KeyboardInterrupt:
        return 0

if __name__ == "__main__":
    rospy.init_node('program_manager')
    globals.global_var_init()
    globals.wetbay_var_init()
    globals.pt1_var_init()
    globals.drybay_var_init()
    initialization()
    temp = sys.argv[1:]
    for x in temp:
        globals.PIGEONHOLE_TO_PROCESS.append(int(x)+1)

    # globals.plc_task("debug_system_reset")
    # globals.plc_task("system_start")
    main()