#!/usr/bin/env python
import logging
import rospy
import sys
import smach
import smach_ros
from time import sleep
from roswebui.msg import *
import actionlib
import requests
pigeonhole_processing_index = 0

NODEJS_URL = "http://localhost:5000/api/system_control"
FRONTEND_MESSAGE_ULR = NODEJS_URL + "/main_program_message"
PROGRAM_MANAGER_TERMINATION_URL = NODEJS_URL + "/kill/program_manager"



class Process(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])
    
    def execute(self, userdata):
        logging.loginfo("processing pigeonhole: " + str(pigeonhole_to_process[pigeonhole_processing_index]) + "...")
        sleep(3)
        return 'success'

class CheckAnymoreToProcess(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail'])
    
    def execute(self, userdata):
        global pigeonhole_processing_index
        if pigeonhole_processing_index < len(pigeonhole_to_process) - 1:
            # logging.loginfo("current index:", pigeonhole_processing_index)
            logging.loginfo("still pigeonhole to process")
            pigeonhole_processing_index += 1
            return 'fail'
        else:
            logging.loginfo("all pigeonhole has fnished processing")
            return 'success'

class Suicide(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])
    
    def execute(self, userdata):
        logging.loginfo("sending completion to roswebui and killing myself")
        requests.get(PROGRAM_MANAGER_TERMINATION_URL)
        return 'success'


class Debug(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])
    
    def execute(self, userdata):
        logging.loginfo("sending completion to roswebui and killing myself")
        requests.get(PROGRAM_MANAGER_TERMINATION_URL)
        # roswebui_client = actionlib.SimpleActionClient('webui_handler', WebUIAction)
        # roswebui_client.wait_for_server()
        # goal = WebUIGoal()
        # goal.task = 4
        # goal.module = "program_manager"
        # roswebui_client.send_goal(goal)   
        # roswebui_client.wait_for_result()
        return 'success'


class frontend_logging():
    def loginfo(self, message):
        rospy.loginfo(message)
        message_data = {'main_program_message': message, 'main_program_error_message': False}
        try:
            requests.post(FRONTEND_MESSAGE_ULR, json=message_data)
        except:
            #frontend not activated, ignoring
            pass

    def logwarn(self, message):
        rospy.logwarn(message)
        message_data = {'main_program_message': message, 'main_program_error_message': False}
        try:
            requests.post(FRONTEND_MESSAGE_ULR, json=message_data)
        except:
            #frontend not activated, ignoring
            pass

    def logerr(self, message):
        rospy.logerr(message)
        message_data = {'main_program_message': message, 'main_program_error_message': True}
        try:
            requests.post(FRONTEND_MESSAGE_ULR, json=message_data)
        except:
            #frontend not activated, ignoring
            pass

def main():
    try:
        sm = smach.StateMachine(outcomes=['ended'])
        with sm:
            smach.StateMachine.add('Process', Process(), transitions={'success':'CheckAnymoreToProcess'})
            smach.StateMachine.add('CheckAnymoreToProcess', CheckAnymoreToProcess(), transitions={'success':'Suicide', 'fail':'Process'})
            smach.StateMachine.add('Suicide', Suicide(), transitions={'success':'ended'})
            smach.StateMachine.add('Debug', Debug(), transitions={'success':'ended'})

        smach_viewer = smach_ros.IntrospectionServer('ur10_smach_controller', sm, '/Start')
        smach_viewer.start()
        outcome = sm.execute()
        rospy.spin()
        smach_viewer.stop()
    except rospy.ROSInterruptException:
        return 0
    except KeyboardInterrupt:
        return 0

def sohai():
    print("i am dead")

if __name__ == '__main__':
    rospy.init_node('program_manager')
    # pigeonhole_to_process = []
    # temp = sys.argv[1:]
    # for x in temp:
    #     pigeonhole_to_process.append(int(x))
    # logging = frontend_logging()
    # logging.loginfo(str(pigeonhole_to_process))
    # main()
    rospy.spin()
    rospy.on_shutdown(sohai)


