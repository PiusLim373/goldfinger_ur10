#!/usr/bin/env python
from weakref import WeakValueDictionary
from xml.etree.ElementTree import PI
import requests
import globals
import smach
import rospy
import os 
from time import sleep
from multiprocessing.pool import ThreadPool
thread = ThreadPool(processes=1)
from hardcoded_variables import *
import time

from roswebui.msg import *
from ur10_pap.msg import *
from pcl_server.msg import *
from ai_server.msg import * 

################################################################################################# NORMAL FUNCTIONS #########################
def pick_success_check(item):
    #check ai result
    ai_goal = AIServerGoal()
    ai_goal.task = ai_goal.PICK_SUCCESS_CHECK
    ai_goal.instrument_needed = item
    ai_goal.which_module = ai_goal.WETBAY
    globals.AI_CLIENT.send_goal(ai_goal)
    globals.AI_CLIENT.wait_for_result()
    ai_result = AIServerResult()
    ai_result = globals.AI_CLIENT.get_result()

    return ai_result.item_remaining

def get_ft():
    motion_goal = RobotControlGoal()
    motion_goal.option = motion_goal.READ_FORCE_TORQUE
    globals.MOTION_CLIENT.send_goal(motion_goal)
    globals.MOTION_CLIENT.wait_for_result()
    motion_result = RobotControlResult()
    motion_result = globals.MOTION_CLIENT.get_result()
    if motion_result.success:
        return motion_result.force_torque[2]
    else:
        return -1

def get_opening():
    motion_goal = RobotControlGoal()
    motion_goal.option = motion_goal.READ_GRIPPER_OPENING
    globals.MOTION_CLIENT.send_goal(motion_goal)
    globals.MOTION_CLIENT.wait_for_result()
    motion_result = RobotControlResult()
    motion_result = globals.MOTION_CLIENT.get_result()
    if motion_result.success:
        return motion_result.gripper_opening
    else:
        return -1

def vision_thread(item, half_reso = False):
    print("thread activated")
    coor_to_go = None
    ai_goal = AIServerGoal()
    ai_goal.instrument_needed = item
    ai_goal.which_module = ai_goal.WETBAY
    globals.AI_CLIENT.send_goal(ai_goal)
    globals.AI_CLIENT.wait_for_result()
    result = AIServerResult()
    result = globals.AI_CLIENT.get_result()
    print("ai returned")
    if result.topick:
        if half_reso:
            globals.LOGGING.logwarn("changing resolution of pointcloud to 960*480")
            os.system("rosrun dynamic_reconfigure dynparam set /wetbay/rc_visard_driver depth_quality H")
            sleep(1)
        pcl_goal = PCLServerGoal()
        pcl_goal.task = pcl_goal.UPDATE_POINTCLOUD
        pcl_goal.module = "wetbay"
        globals.PCL_CLIENT.send_goal(pcl_goal)
        globals.PCL_CLIENT.wait_for_result()
        pcl_result = PCLServerResult()
        pcl_result = globals.PCL_CLIENT.get_result()
        if pcl_result.success:
            if item == "bracket":
                print("i limit", int(round(result.ai_output.h/2)))
                for i in range(int(round(result.ai_output.h/2))):
                    pcl_input = [result.ai_output.u, result.ai_output.v+i, result.ai_output.theta-90]
                    print(pcl_input)
                    pcl_goal = PCLServerGoal()
                    pcl_goal.task = pcl_goal.GET_COOR
                    pcl_goal.pcl_input = pcl_input
                    pcl_goal.module = "wetbay"
                    globals.PCL_CLIENT.send_goal(pcl_goal)
                    pcl_result = PCLServerResult()
                    globals.PCL_CLIENT.wait_for_result()
                    pcl_result = globals.PCL_CLIENT.get_result()
                    if pcl_result.success:
                        if globals.PIGEONHOLE_PROCESSING <= 3 and pcl_result.coor_wrt_world.pose.position.z > 1.03:
                            coor_to_go = pcl_result.coor_wrt_world
                            
                            break
                        elif globals.PIGEONHOLE_PROCESSING > 3 and pcl_result.coor_wrt_world.pose.position.z > 0.73:
                            coor_to_go = pcl_result.coor_wrt_world
                            break
                    pcl_input = [result.ai_output.u, result.ai_output.v-i, result.ai_output.theta-90]
                    print(pcl_input)
                    pcl_goal = PCLServerGoal()
                    pcl_goal.task = pcl_goal.GET_COOR
                    pcl_goal.pcl_input = pcl_input
                    pcl_goal.module = "wetbay"
                    globals.PCL_CLIENT.send_goal(pcl_goal)
                    pcl_result = PCLServerResult()
                    globals.PCL_CLIENT.wait_for_result()
                    pcl_result = globals.PCL_CLIENT.get_result()
                    if pcl_result.success:
                        if globals.PIGEONHOLE_PROCESSING <= 3 and pcl_result.coor_wrt_world.pose.position.z > 1.03:
                            coor_to_go = pcl_result.coor_wrt_world
                            break
                        elif globals.PIGEONHOLE_PROCESSING > 3 and pcl_result.coor_wrt_world.pose.position.z > 0.73:
                            coor_to_go = pcl_result.coor_wrt_world
                            break
            elif item == "kidney_dish":
                # ai got the central point of the kidney_dish, checking throughout height of bounding box to get the max value
                for i in range(int(round(result.ai_output.h/2))):
                    pcl_input = [result.ai_output.u, result.ai_output.v+i, result.ai_output.theta-90]
                    pcl_goal = PCLServerGoal()
                    pcl_goal.task = pcl_goal.GET_COOR
                    pcl_goal.pcl_input = pcl_input
                    pcl_goal.module = "wetbay"
                    globals.PCL_CLIENT.send_goal(pcl_goal)
                    pcl_result = PCLServerResult()
                    globals.PCL_CLIENT.wait_for_result()
                    pcl_result = globals.PCL_CLIENT.get_result()
                    if pcl_result.success:
                        coor_to_go = pcl_result.coor_wrt_world
                        break
                    pcl_input = [result.ai_output.u, result.ai_output.v-i, result.ai_output.theta-90]
                    pcl_goal = PCLServerGoal()
                    pcl_goal.task = pcl_goal.GET_COOR
                    pcl_goal.pcl_input = pcl_input
                    pcl_goal.module = "wetbay"
                    globals.PCL_CLIENT.send_goal(pcl_goal)
                    pcl_result = PCLServerResult()
                    globals.PCL_CLIENT.wait_for_result()
                    pcl_result = globals.PCL_CLIENT.get_result()
                    if pcl_result.success:
                        coor_to_go = pcl_result.coor_wrt_world
                        break
            elif item == "gallipot":
                # ai got the central point of the gallipot, checking throughout height of bounding box to get the max value
                default_v = int(round(result.ai_output.v - result.ai_output.h/2))
                for i in range(int(round(result.ai_output.h/2))):
                    pcl_input = [result.ai_output.u, default_v+i, 0]
                    print("debug, checking", result.ai_output.v+i)
                    pcl_goal = PCLServerGoal()
                    pcl_goal.task = pcl_goal.GET_COOR
                    pcl_goal.pcl_input = pcl_input
                    pcl_goal.module = "wetbay"
                    globals.PCL_CLIENT.send_goal(pcl_goal)
                    pcl_result = PCLServerResult()
                    globals.PCL_CLIENT.wait_for_result()
                    pcl_result = globals.PCL_CLIENT.get_result()
                    if pcl_result.success:
                        coor_to_go = pcl_result.coor_wrt_world
                        break
                    pcl_input = [result.ai_output.u, default_v-i, 0]
                    print("debug, checking", result.ai_output.v+i)
                    pcl_goal = PCLServerGoal()
                    pcl_goal.task = pcl_goal.GET_COOR
                    pcl_goal.pcl_input = pcl_input
                    pcl_goal.module = "wetbay"
                    globals.PCL_CLIENT.send_goal(pcl_goal)
                    pcl_result = PCLServerResult()
                    globals.PCL_CLIENT.wait_for_result()
                    pcl_result = globals.PCL_CLIENT.get_result()
                    if pcl_result.success:
                        coor_to_go = pcl_result.coor_wrt_world
                        break
            elif item == "tray":
                for i in range(int(round(result.ai_output.h/2))):
                    pcl_input = [result.ai_output.u, result.ai_output.v+i, 0]
                    pcl_goal = PCLServerGoal()
                    pcl_goal.task = pcl_goal.GET_COOR
                    pcl_goal.pcl_input = pcl_input
                    pcl_goal.module = "wetbay"
                    globals.PCL_CLIENT.send_goal(pcl_goal)
                    pcl_result = PCLServerResult()
                    globals.PCL_CLIENT.wait_for_result()
                    pcl_result = globals.PCL_CLIENT.get_result()
                    if pcl_result.success:
                        coor_to_go = pcl_result.coor_wrt_world
                        break
                    pcl_input = [result.ai_output.u, result.ai_output.v-i, 0]
                    pcl_goal = PCLServerGoal()
                    pcl_goal.task = pcl_goal.GET_COOR
                    pcl_goal.pcl_input = pcl_input
                    pcl_goal.module = "wetbay"
                    globals.PCL_CLIENT.send_goal(pcl_goal)
                    pcl_result = PCLServerResult()
                    globals.PCL_CLIENT.wait_for_result()
                    pcl_result = globals.PCL_CLIENT.get_result()
                    if pcl_result.success:
                        coor_to_go = pcl_result.coor_wrt_world
                        break
        else:
            globals.LOGGING.logerr("error when updating depth data")
    else:
        globals.LOGGING.loginfo("ai cant locate " + str(item))
    if half_reso:
        globals.LOGGING.logwarn("changing resolution of pointcloud to 1290*960")
        os.system("rosrun dynamic_reconfigure dynparam set /wetbay/rc_visard_driver depth_quality F")
        sleep(1)
    print(coor_to_go)
    return coor_to_go

def go_wetbay_with_parallel_gripper():
    motion_goal = RobotControlGoal()
    motion_goal.option = motion_goal.GO_TO_WETBAY
    globals.MOTION_CLIENT.send_goal(motion_goal)
    globals.MOTION_CLIENT.wait_for_result()
    if globals.MOTION_CLIENT.get_result().success:
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.CHANGE_GRIPPER
        motion_goal.gripper = motion_goal.PARALLEL
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            return True
        else:
            globals.LOGGING.loginfo("error during gripper changing")
            return False
    else:
        globals.LOGGING.loginfo("error during wetbay_homing")
        return False

def get_destination_coor(item):
    coor_to_go = None
    has_depth = False
    ai_goal = AIServerGoal()
    if item == "pt1_center":
        ai_goal.task = ai_goal.GET_BOUNDARY_CENTER
        ai_goal.which_module = ai_goal.PT1
    elif item == "BRAC_1" or item == "BRAC_2":
        ai_goal.which_module = ai_goal.PT2
        ai_goal.instrument_needed = item
    elif item == "container":
        ai_goal.instrument_needed = "CON"
        ai_goal.which_module = ai_goal.DRYBAY
    globals.AI_CLIENT.send_goal(ai_goal)
    globals.AI_CLIENT.wait_for_result()
    ai_result = AIServerResult()
    ai_result = globals.AI_CLIENT.get_result()
    if ai_result.topick:
        if item == "pt1_center" and ai_result.ai_output.v < 350:
            print("pt1 center too far away")
            return None
        try_negative = False
        i = 0
        while not has_depth and i <= 3 :
            pcl_goal = PCLServerGoal()
            pcl_goal.task = pcl_goal.UPDATE_POINTCLOUD
            if item == "pt1_center":
                pcl_goal.module = "pt1"
            elif item == "BRAC_1" or item == "BRAC_2":
                pcl_goal.module = "pt2"
            elif item == "container":
                pcl_goal.module = "drybay"
            globals.PCL_CLIENT.send_goal(pcl_goal)
            globals.PCL_CLIENT.wait_for_result()
            pcl_result = PCLServerResult()
            pcl_result = globals.PCL_CLIENT.get_result()
            if pcl_result.success:
                if try_negative:
                    pcl_input = [ai_result.ai_output.u, ai_result.ai_output.v - i, 0]
                else:
                    pcl_input = [ai_result.ai_output.u, ai_result.ai_output.v + i, 0]
                try_negative = not try_negative
                pcl_goal = PCLServerGoal()
                pcl_goal.task = pcl_goal.GET_COOR
                pcl_goal.pcl_input = pcl_input
                if item == "pt1_center":
                    pcl_goal.module = "pt1"
                elif item == "BRAC_1" or item == "BRAC_2":
                    pcl_goal.module = "pt2"
                elif item == "container":
                    pcl_goal.module = "drybay"
                globals.PCL_CLIENT.send_goal(pcl_goal)
                globals.PCL_CLIENT.wait_for_result()
                pcl_result = PCLServerResult()
                pcl_result = globals.PCL_CLIENT.get_result()
                has_depth = pcl_result.success
                if has_depth:
                    rospy.loginfo("depth get successfully")
                    coor_to_go = pcl_result.coor_wrt_world
                    if item == "pt1_center":
                        coor_to_go.pose.position.y -= 0.08
                        coor_to_go.pose.position.z = 1.23
                        coor_to_go.pose.orientation.x = -1
                        coor_to_go.pose.orientation.y = 0
                        coor_to_go.pose.orientation.z = 0
                        coor_to_go.pose.orientation.w = 0
                        break
                    elif item == "BRAC_1" or item == "BRAC_2":
                        coor_to_go.pose.position.z = 0.865
                        coor_to_go.pose.orientation.x = 1
                        coor_to_go.pose.orientation.y = 0
                        coor_to_go.pose.orientation.z = 0
                        coor_to_go.pose.orientation.w = 0
                    elif item == "container":
                        coor_to_go.pose.position.x -= 0.01
                        coor_to_go.pose.position.y -= 0.02
                        if globals.PIGEONHOLE_PROCESSING <= 3:
                            coor_to_go.pose.position.z = 1.08
                        elif globals.PIGEONHOLE_PROCESSING > 3:
                            coor_to_go.pose.position.z = 0.77
                        coor_to_go.pose.orientation.x = 0
                        coor_to_go.pose.orientation.y = -0.998
                        coor_to_go.pose.orientation.z = -0.07
                        coor_to_go.pose.orientation.w = 0
                        break
                    else:
                        print("invalid module")
                        return None
                    
            else:
                rospy.logerr("something wrong when capturing depth")
            i += 1
        return coor_to_go
    else:
        globals.LOGGING.logerr("AI cant detect " + str(item))
        return None
################################################################################################# SMACH CLASSES ############################
class TransferBracketSM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail', 'partially_success', 'half_reso', 'retry'])
    
    def execute(self, userdata):
        if not globals.launch_pt2_camera():
            return 'retry'
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT >= 3:
            globals.TRIAL_COUNT = 0
            globals.LOGGING.logerr("Program terminated during bracket transfer, please check if bracket are loaded properly")
            return 'fail'
        if globals.BRACKET_TRANSFERRED == 2:
            globals.BRACKET_TRANSFERRED = 0
            globals.PROCESS_COMPLETED = 1
            globals.TRIAL_COUNT = 0
            return 'success'
        globals.LOGGING.loginfo("Processing brackets transfer")
        vision_result = thread.apply_async(vision_thread, args=("bracket", globals.HALF_RESO,))
        if go_wetbay_with_parallel_gripper():
            coor_to_go = vision_result.get()
            if coor_to_go == None:
                globals.LOGGING.logerr("all poselist returned are nan or vision cant locate bracket, please check if bracket are loaded properly")
                return 'retry'
            else:
                pass
                #correction based on pigeonhole
                # if globals.PIGEONHOLE_PROCESSING == 3 or globals.PIGEONHOLE_PROCESSING == 6:
                #     coor_to_go.pose.position.x += 0.01
            print(coor_to_go)
            count_before_picking = pick_success_check("bracket")
            ft_before_picking = get_ft()
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.PICK_SPECIAL
            motion_goal.special_item = "wetbay_picking"
            motion_goal.extra_argument = "wetbay_bracket"
            motion_goal.target_pose = coor_to_go
            motion_goal.pigeonhole_processing = globals.PIGEONHOLE_PROCESSING
            motion_goal.extra_zdown = 0.02
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                #picking completed, checking vision and froce torque
                count_after_picking = pick_success_check("bracket")
                ft_after_picking = get_ft()
                rospy.loginfo("bracket count before picking: " + str(count_before_picking) + "; bracket count after picking: " + str(count_after_picking))
                rospy.loginfo("ft before picking: " + str(ft_before_picking) + "; ft after picking: " + str(ft_after_picking))
                if (count_after_picking == count_before_picking and abs(ft_after_picking-ft_before_picking) < 6):
                    globals.LOGGING.logerr("picking failed")
                    opening = get_opening()
                    if opening != -1 and opening > 0:
                        globals.LOGGING.logerr("picking failed, procced to push back item from where it is picked")
                        globals.recovery_placing(module= "wetbay", coor= coor_to_go, item= "wetbay_bracket")
                    return 'retry'
                else:
                    globals.LOGGING.loginfo("vision cant locate bracket at wetbay, picking successful")
                motion_goal.option = motion_goal.GO_TO_PT2
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    pt2_sb_placing_coor = get_destination_coor(str("BRAC_" + str(globals.BRACKET_TRANSFERRED + 1)))
                    if pt2_sb_placing_coor == None:
                        globals.LOGGING.logerr("Unable to locate pt2 bracket placing coor, retrying")
                        globals.recovery_placing(module= "wetbay", coor= coor_to_go, item= "wetbay_bracket")
                        return 'retry'
                    motion_goal = RobotControlGoal()
                    motion_goal.option = motion_goal.PLACE
                    motion_goal.target_pose = pt2_sb_placing_coor
                    motion_goal.starting_location = "pt2"
                    globals.MOTION_CLIENT.send_goal(motion_goal)
                    globals.MOTION_CLIENT.wait_for_result()
                    if globals.MOTION_CLIENT.get_result().success:
                        rospy.loginfo("sb transfer is successful")
                        if globals.place_success_check("bracket"):
                            globals.BRACKET_TRANSFERRED += 1
                            globals.TRIAL_COUNT = 0
                            return 'partially_success'
                        else:
                            globals.LOGGING.logwarn("cant locate bracket at destination, bracket might be dropped during transfer, please load the bracket at input and try again")
                            return 'fail'
                    else:
                        rospy.loginfo("error during placing")
                        return 'retry'
                else:
                    rospy.loginfo("error during pt2_homing")
                    return 'retry'
            else:
                rospy.loginfo("error during picking")
                return 'retry'
        else:
            rospy.loginfo("error during wetbay_homing and changing parallel_gripper")
            return 'retry'

class InitialiseDB(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'retry'])
    
    def execute(self, userdata):
        '''detect type of tray and initialise database object'''
        ai_goal = AIServerGoal()
        ai_goal.instrument_needed = "tray"
        ai_goal.which_module = ai_goal.WETBAY
        globals.AI_CLIENT.send_goal(ai_goal)
        globals.AI_CLIENT.wait_for_result()
        result = AIServerResult()
        result = globals.AI_CLIENT.get_result()
        if result.topick:
            if result.ai_output.item == "tray1":
                globals.BOX_ID = requests.post(globals.INSTRUMENT_SET_URL, json={"model":"Sterrad"}).json()['box_id']
            elif result.ai_output.item == "tray2":
                globals.BOX_ID = requests.post(globals.INSTRUMENT_SET_URL, json={"model":"Aesculap"}).json()['box_id']
            globals.BOX_ID_URL = globals.INSTRUMENT_SET_URL + "/" + str(globals.BOX_ID)
            print(globals.BOX_ID, globals.BOX_ID_URL)
            globals.PROCESS_COMPLETED = 2
            return 'success'
        else:
            globals.LOGGING.logerr("Can't locate tray, please check if wetbay is loaded properly")
            return 'retry'

class StartUR5ProcessSM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])
    
    def execute(self, userdata):
        '''signal ur5 to start processing ring instrument'''
        webui_goal = WebUIGoal()
        webui_goal.task = webui_goal.KILL
        webui_goal.module = "pt2_camera"
        globals.WEBUI_CLIENT.send_goal(webui_goal)
        globals.WEBUI_CLIENT.wait_for_result()
        if globals.WEBUI_CLIENT.get_result().success:
            #successfully killed pt2 camera driver on ur10 side, signal ur5 to launch camera driver ans start program manager
            globals.LOGGING.logwarn("Unlocker locker manually and press enter to continue")
            raw_input("Unlocker locker manually and press enter to continue")
            return 'success'
            if requests.get(globals.UR5_START_PROCESS_URL).json()['success']:
            #   if requests.post(globals.UR5_START_PROCESS_URL, json={"box_id":globals.BOX_ID}).json()['success']:
                return 'success'
            else:
                print("ur5 loading fail")
                return 'success'


class TransferGallipotSM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail','retry'])
    
    def execute(self, userdata):
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT >= 3:
            globals.TRIAL_COUNT = 0
            globals.LOGGING.logerr("Program terminated during gallipot transfer, please check if gallipot is loaded properly")
            return 'fail'
        globals.LOGGING.loginfo("Processing gallipot transfer")
        vision_result = thread.apply_async(vision_thread, args=("gallipot",))
        if go_wetbay_with_parallel_gripper():
            coor_to_go = vision_result.get()
            if coor_to_go == None:
                globals.LOGGING.logerr("all poselist returned are nan or vision cant locate bracket")
                return 'retry'
            if globals.PIGEONHOLE_PROCESSING == None:
                globals.PIGEONHOLE_PROCESSING = int(input("Which pigoenhole is being processed?"))

            if globals.PIGEONHOLE_PROCESSING == 1:
                coor_to_go.pose.position.z = 0.925
                coor_to_go.pose.position.y += 0.01
                coor_to_go.pose.position.x += 0.01
            elif globals.PIGEONHOLE_PROCESSING == 2:
                coor_to_go.pose.position.z = 0.925
                coor_to_go.pose.position.x += 0.01
            elif (globals.PIGEONHOLE_PROCESSING == 3):
                coor_to_go.pose.position.z = 0.915
                coor_to_go.pose.position.y -= 0.02
                coor_to_go.pose.position.x += 0.01
            elif (globals.PIGEONHOLE_PROCESSING == 4): 
                coor_to_go.pose.position.z = 0.605
                coor_to_go.pose.position.y += 0.01
            elif (globals.PIGEONHOLE_PROCESSING == 5):
                coor_to_go.pose.position.z = 0.605
            elif (globals.PIGEONHOLE_PROCESSING == 6):
                coor_to_go.pose.position.z = 0.595
                coor_to_go.pose.position.y -= 0.01

            print(coor_to_go)
            count_before_picking = pick_success_check("gallipot")    
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.PICK_SPECIAL
            motion_goal.target_pose = coor_to_go
            motion_goal.special_item = "wetbay_picking"
            motion_goal.extra_argument = "wetbay_gallipot"
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                #picking completed, checking vision and froce torque
                count_after_picking = pick_success_check("gallipot")
                rospy.loginfo("gallipot count before picking: " + str(count_before_picking) + "; gallipot count after picking: " + str(count_after_picking))
                if (count_after_picking == count_before_picking):
                    globals.LOGGING.logerr("picking failed")
                    opening = get_opening()
                    if opening != -1 and opening > 0:
                        globals.LOGGING.logerr("picking failed, procced to push back item from where it is picked")
                        globals.recovery_placing(module= "wetbay", coor= coor_to_go, item= "wetbay_gallipot")
                    return 'retry'
                else:
                    globals.LOGGING.loginfo("vision cant locate gallipot at wetbay, picking successful")
                motion_goal.option = motion_goal.PLACE
                motion_goal.target_pose = globals.list_to_posestamped(gallipot_placing_coor)
                motion_goal.starting_location = "drybay"
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    rospy.loginfo("gallipot transfer is successful")
                    if globals.place_success_check("gallipot"):
                        globals.PROCESS_COMPLETED = 3
                        globals.TRIAL_COUNT = 0
                        return 'success'
                    else:
                        globals.LOGGING.loginfo("cant locate bracket at destination, item might be dropped during transfer, please load the gallipot at input and try again")
                        return 'fail'
                else:
                    rospy.loginfo("error during placing")
                    return 'retry'
            else:
                rospy.loginfo("error during picking")
                return 'retry'
        else:
            rospy.loginfo("error during wetbay_homing and changing parallel_gripper")
            return 'retry'


class TransferKidneyDishSM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'fail', 'retry'])
    
    def execute(self, userdata):
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT >= 3:
            globals.TRIAL_COUNT = 0
            globals.LOGGING.logerr("Program terminated during kidney dish transfer, please check if kidney dish is loaded properly")
            return 'fail'
        globals.LOGGING.loginfo("Processing kidney dish transfer")
        vision_result = thread.apply_async(vision_thread, args=("kidney_dish",))
        if go_wetbay_with_parallel_gripper():
            coor_to_go = vision_result.get()
            if coor_to_go == None:
                globals.LOGGING.logerr("all poselist returned are nan or vision cant locate bracket")
                return 'retry'
            if globals.PIGEONHOLE_PROCESSING == None:
                    globals.PIGEONHOLE_PROCESSING = int(input("Which pigoenhole is being processed?"))
                    
            if (globals.PIGEONHOLE_PROCESSING == 4):
                coor_to_go.pose.position.z = 0.605
            elif (globals.PIGEONHOLE_PROCESSING == 5):
                coor_to_go.pose.position.z = 0.6
            elif (globals.PIGEONHOLE_PROCESSING == 6):
                coor_to_go.pose.position.z = 0.595

            elif (globals.PIGEONHOLE_PROCESSING == 1):
                coor_to_go.pose.position.z = 0.925
            elif (globals.PIGEONHOLE_PROCESSING == 2):
                coor_to_go.pose.position.z = 0.92    
            elif (globals.PIGEONHOLE_PROCESSING == 3):
                coor_to_go.pose.position.z = 0.915
            print(coor_to_go)
            count_before_picking = pick_success_check("kidney_dish")
            ft_before_picking = get_ft()
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.PICK_SPECIAL
            motion_goal.target_pose = coor_to_go
            motion_goal.special_item = "wetbay_picking"
            motion_goal.extra_argument = "wetbay_kidney_dish"
            motion_goal.pigeonhole_processing = globals.PIGEONHOLE_PROCESSING
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                #picking completed, checking vision and froce torque
                count_after_picking = pick_success_check("kidney_dish")
                ft_after_picking = get_ft()
                rospy.loginfo("kidney_dish count before picking: " + str(count_before_picking) + "; kidney_dish count after picking: " + str(count_after_picking))
                rospy.loginfo("ft before picking: " + str(ft_before_picking) + "; ft after picking: " + str(ft_after_picking))
                if (count_after_picking == count_before_picking and abs(ft_after_picking-ft_before_picking) < 3):
                    globals.LOGGING.logerr("picking failed")
                    opening = get_opening()
                    if opening != -1 and opening > 0:
                        globals.LOGGING.logerr("picking failed, procced to push back item from where it is picked")
                        globals.recovery_placing(module= "wetbay", coor= coor_to_go, item= "wetbay_kidney_dish")
                    return 'retry'
                else:
                    globals.LOGGING.loginfo("vision cant locate kidney_dish at wetbay, picking successful")
                
                pt1_center_coor = get_destination_coor("pt1_center")
                while not pt1_center_coor:
                    os.system("rosservice call /ur_hardware_interface/set_io 1 " + str(rotating_table_pin) + " 1")
                    sleep(0.5)
                    os.system("rosservice call /ur_hardware_interface/set_io 1 " + str(rotating_table_pin) + " 0")
                    sleep(1)
                    pt1_center_coor = get_destination_coor("pt1_center")
                # removing the possibility of unable to find pt1_center coor due to sparse pointcloud, if return None means pt1_center too far away
                #if pt1_center_coor == None:
                    # globals.LOGGING.logerr("Unable to locate pt1 center, retrying")
                    # globals.recovery_placing(module= "wetbay", coor= coor_to_go, item= "wetbay_kidney_dish")
                    # return 'retry'
                motion_goal = RobotControlGoal()
                motion_goal.option = motion_goal.PLACE_SPECIAL
                motion_goal.target_pose = pt1_center_coor
                motion_goal.special_item = "kidney_dish"
                motion_goal.extra_argument = "1"
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    # dispensing instrument end, placing at waiting area
                    motion_goal = RobotControlGoal()
                    motion_goal.option = motion_goal.GO_TO_DRYBAY
                    globals.MOTION_CLIENT.send_goal(motion_goal)
                    globals.MOTION_CLIENT.wait_for_result()
                    if globals.MOTION_CLIENT.get_result().success:
                        motion_goal.option = motion_goal.PLACE
                        motion_goal.target_pose = globals.list_to_posestamped(kidney_dish_placing_coor)
                        motion_goal.starting_location = "drybay"
                        globals.MOTION_CLIENT.send_goal(motion_goal)
                        globals.MOTION_CLIENT.wait_for_result()
                        if globals.MOTION_CLIENT.get_result().success:
                            globals.LOGGING.loginfo("kidney dish transfer is successful")
                            if globals.place_success_check("kidney_dish"):
                                globals.TRIAL_COUNT = 0
                                globals.PROCESS_COMPLETED = 4
                                return 'success'
                            else:
                                globals.LOGGING.loginfo("cant locate kidney dish at destination, item might be dropped during transfer, please load the kidney dish at input and try again")
                                return 'fail'
                        else:
                            rospy.loginfo("error during placing")
                            globals.recovery_placing(module= "wetbay", coor= coor_to_go, item= "wetbay_kidney_dish")
                            return 'retry'
                    else:
                        rospy.loginfo("error during drybay homing")
                        globals.recovery_placing(module= "wetbay", coor= coor_to_go, item= "wetbay_kidney_dish")
                        return 'fail'  
                else:
                    rospy.loginfo("error during instrument dumping")
                    globals.recovery_placing(module= "wetbay", coor= coor_to_go, item= "wetbay_kidney_dish")
                    return 'retry'          
            else:
                rospy.loginfo("error during picking")
                return 'retry'
        else:
            rospy.loginfo("error during wetbay_homing and changing parallel_gripper")
            return 'retry'

class TransferTraySM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail', 'retry'])
    
    def execute(self, userdata):
        # input("press enter to continue")
        globals.plc_task("drawer_ext_drybay", globals.PIGEONHOLE_PROCESSING)
        tray_out_time = time.time()
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT >= 3:
            globals.TRIAL_COUNT = 0
            globals.LOGGING.logerr("Program terminated during tray transfer, please check if tray is loaded properly")
            return 'fail'
        globals.LOGGING.loginfo("Processing tray transfer")
        vision_result = thread.apply_async(vision_thread, args=("tray",))
        print("before wetbay and gripper changing")
        if go_wetbay_with_parallel_gripper():
            coor_to_go = vision_result.get()
            print(coor_to_go)
            if coor_to_go == None:
                globals.LOGGING.logerr("all poselist returned are nan or vision cant locate tray")
                return 'retry'
            if globals.PIGEONHOLE_PROCESSING == None:
                globals.PIGEONHOLE_PROCESSING = int(input("Which pigoenhole is being processed?"))
            ### offset based on pigeonhole ###
            if (globals.PIGEONHOLE_PROCESSING == 4) or (globals.PIGEONHOLE_PROCESSING == 5) or (globals.PIGEONHOLE_PROCESSING == 6):
                coor_to_go.pose.position.z = 0.605
            elif (globals.PIGEONHOLE_PROCESSING == 1) or (globals.PIGEONHOLE_PROCESSING == 2) or (globals.PIGEONHOLE_PROCESSING == 3):
                coor_to_go.pose.position.z = 0.915
                print("adjustment made")
            ### offset based on pigeonhole ends ###
            print(coor_to_go)
            while(time.time() - tray_out_time <= 5):
                sleep(1)
            tray_place_coor = get_destination_coor("container")
            if tray_place_coor == None:
                globals.LOGGING.logerr("Unable to locate container, please make sure container is loaded at Drybay and try again")
                return 'fail'
            print("tray_place_coor", tray_place_coor)
            count_before_picking = pick_success_check("tray")
            if count_before_picking == 0:
                # something wrong, cant be zero, else wont reach here, manually chged to 1
                count_before_picking = 1
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.PICK_SPECIAL
            motion_goal.target_pose = coor_to_go
            motion_goal.special_item = "wetbay_picking"
            motion_goal.extra_argument = "wetbay_tray"
            motion_goal.pigeonhole_processing = globals.PIGEONHOLE_PROCESSING
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                #picking completed, checking vision and froce torque
                count_after_picking = pick_success_check("tray")
                rospy.loginfo("tray count before picking: " + str(count_before_picking) + "; tray count after picking: " + str(count_after_picking))
                if (count_after_picking == count_before_picking):
                    globals.LOGGING.logerr("picking failed")
                    opening = get_opening()
                    if opening != -1 and opening > 0:
                        globals.LOGGING.logerr("picking failed, procced to push back item from where it is picked")
                        globals.recovery_placing(module= "wetbay", coor= coor_to_go, item= "wetbay_tray")
                    return 'retry'
                else:
                    globals.LOGGING.loginfo("vision cant locate tray at wetbay, picking successful")
                # return 'success'
                motion_goal = RobotControlGoal()
                motion_goal.option = motion_goal.GO_TO_DRYBAY
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    motion_goal = RobotControlGoal()
                    motion_goal.option = motion_goal.PLACE_SPECIAL
                    motion_goal.special_item = "drybay_tray"
                    motion_goal.target_pose = tray_place_coor
                    motion_goal.starting_location = "drybay"
                    motion_goal.pigeonhole_processing = globals.PIGEONHOLE_PROCESSING
                    globals.MOTION_CLIENT.send_goal(motion_goal)
                    globals.MOTION_CLIENT.wait_for_result()
                    if globals.MOTION_CLIENT.get_result().success:
                        globals.LOGGING.loginfo("tray transfer is successful")
                        if globals.place_success_check("tray"):
                            robot_status = globals.get_robot_status()
                            if robot_status == "estop_without_btn":
                                globals.LOGGING.logerr("Estop encountered, please assist to move robot to a safer position")
                                return 'fail'
                            elif robot_status == "protective_stop":
                                globals.LOGGING.loginfo("Robot stucked in drawer, releasing protective stop and drybay homing")
                                motion_goal = RobotControlGoal()
                                motion_goal.option = motion_goal.DASHBOARD_SERVER_ACTION
                                motion_goal.extra_argument = "release_protective_or_estop"
                                globals.MOTION_CLIENT.send_goal(motion_goal)
                                globals.MOTION_CLIENT.wait_for_result()
                                if globals.MOTION_CLIENT.get_result().success:
                                    #sucessfully recover from protective / estop
                                    motion_goal = RobotControlGoal()
                                    motion_goal.option = motion_goal.GO_TO_DRYBAY
                                    globals.MOTION_CLIENT.send_goal(motion_goal)
                                    globals.MOTION_CLIENT.wait_for_result()
                                    if globals.MOTION_CLIENT.get_result().success:
                                        pass
                                    else:
                                        globals.LOGGING.logerr("Something is wrong while release protective / estop and cant navigate to Drbay, please check")
                                        return 'fail'
                                else:
                                    #failed to recover from protective or estop
                                    globals.LOGGING.logerr("Robot stucked in drybay and is unable to recover itself, please assist to move the robot out of the drawer")
                                    return 'fail'
                            elif robot_status == "running":
                                pass
                            globals.TRIAL_COUNT = 0
                            globals.PROCESS_COMPLETED = 5
                            globals.plc_task("drawer_ret_wetbay", globals.PIGEONHOLE_PROCESSING)
                            return 'success'
                        else:
                            globals.LOGGING.logerr("cant locate tray at destination, item might be dropped during transfer, please load the item at the input and try again")
                            return 'fail'
                    else:
                        rospy.loginfo("error during placing")
                        return 'retry'
                else:
                    rospy.loginfo("error during pt2_homing")
                    return 'retry'
            else:
                rospy.loginfo("error during picking")
                return 'retry'
        else:
            rospy.loginfo("error during wetbay_homing and changing parallel_gripper")
            return 'retry'


class WetbayDebugFunc(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])
    
    def execute(self, userdata):
        pt1_center_coor = get_destination_coor("pt1_center")
        while not pt1_center_coor:
            os.system("rosservice call /ur_hardware_interface/set_io 1 " + str(rotating_table_pin) + " 1")
            sleep(1)
            os.system("rosservice call /ur_hardware_interface/set_io 1 " + str(rotating_table_pin) + " 0")
            sleep(1)
            pt1_center_coor = get_destination_coor("pt1_center")
        print(pt1_center_coor)
        return 'success'
        if pt1_center_coor == None:
            globals.LOGGING.logerr("Unable to locate pt1 center, retrying")
            print("cant find")
            return 'success'
        motion_goal.target_pose = pt1_center_coor
        motion_goal.special_item = "kidney_dish"
        motion_goal.extra_argument = "1"
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            return 'success'
        else:
            print("too far lmao")
            return 'success'
