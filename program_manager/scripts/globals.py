#!/usr/bin/env python
import time
from geometry_msgs.msg import PoseStamped
from plc_server.srv import *
from ur10_pap.msg import *
from roswebui.msg import *
import rospy
import requests
from hardcoded_variables import *

def global_var_init():
    global MOTION_CLIENT, AI_CLIENT, PCL_CLIENT, PLC_CLIENT, IB1_CLIENT,WEBUI_CLIENT, \
            UR_DASHBOARD_SERVER_IP, UR_DASHBOARD_SERVER_PORT, \
            PIGEONHOLE_PROCESSING, PIGEONHOLE_TO_PROCESS, PIGEONHOLE_PROCESSING_INDEX,BOX_ID,\
            FRONTEND_MESSAGE_URL, PROGRAM_MANAGER_TERMINATION_URL,INSTRUMENT_SET_URL, BOX_ID_URL, ERROR_RECTIFICATION_URL, \
            INDICATOR_DISPENSER_URL,NON_RING_SPARE_BAY_URL,WRAPPING_BAY_URL, \
            UR5_START_PROCESS_URL, UR5_KILL_PROCESS_URL, UR5_QUERY_PROCESS_URL,\
            LOGGING, TRIAL_COUNT, PROCESS_COMPLETED

    MOTION_CLIENT = None
    AI_CLIENT = None
    PCL_CLIENT = None
    PLC_CLIENT = None
    IB1_CLIENT = None
    WEBUI_CLIENT = None

    UR_DASHBOARD_SERVER_IP = '192.168.10.101'
    UR_DASHBOARD_SERVER_PORT = 29999

    PIGEONHOLE_TO_PROCESS = []
    PIGEONHOLE_PROCESSING = 0
    PIGEONHOLE_PROCESSING_INDEX = 0
    BOX_ID = 8

    NODEJS_URL = "http://localhost:5000/api"
    FRONTEND_MESSAGE_URL = NODEJS_URL + "/system_control/main_program_message"
    ERROR_RECTIFICATION_URL = NODEJS_URL + "/error_rectification"
    PROGRAM_MANAGER_TERMINATION_URL = NODEJS_URL + "/system_control/kill/program_manager"
    INSTRUMENT_SET_URL = NODEJS_URL + "/instrument_set"
    BOX_ID_URL = ""

    INDICATOR_DISPENSER_URL = "http://192.168.10.105:5000/indicator_dispenser"
    NON_RING_SPARE_BAY_URL = "http://localhost:5001/non_ring_spare_bay"
    WRAPPING_BAY_URL = "http://localhost:5001/wrapping_bay"

    UR5_URL = "http://192.168.10.196:5002/"
    UR5_START_PROCESS_URL = UR5_URL + "/start/program_manager"
    UR5_KILL_PROCESS_URL = UR5_URL + "/kill_process"
    UR5_QUERY_PROCESS_URL = UR5_URL + "/query_process"
    
    LOGGING = frontend_logging()
    TRIAL_COUNT = 0
    PROCESS_COMPLETED = 0
    # 1 = completed database creation, 
    # 2 = completed both bracket trasnfer, 
    # 3 = completed gallipot transfer, 
    # 4 = completed kidney dish dumping and transfer, 
    # 5 = completed tray transfer, 
    # 6 = completed pt1 picking, 
    # 7 = completed nonring spare bay replenishment
    # 8 = completed tray detection
    # 9 = completed indicator placement
    # 10 = completed forcep bracket placement
    # 11 = completed mixed bracket placement
    # 12 = completed gallipot placement
    # 13 = completed kidney dish placement

    # 14 = completed wrapped package placement
    # 15 = completed printed list placement
    # 16 = completed sticker tag placement


def wetbay_var_init():
    global AI_SERVER_RETURN, PCL_SERVER_RETURN, BRACKET_TRANSFERRED, HALF_RESO
    AI_SERVER_RETURN = []
    PCL_SERVER_RETURN = None
    BRACKET_TRANSFERRED = 0
    HALF_RESO = False
    
def pt1_var_init():
    global HAS_INITIALISE_IB, IB_STATUS, FORCE_ISOLATE, DRAG_COUNTER, INSTRUMENT_DETECTED, FAULTY_OCM_INSTRUMENT, OCM_STATUS, PREVIOUS_TRIED_INSTRUMENT, INSRUMENT_REMAINING, EMPTY_COUNTER, F, PT1_CENTER_COOR,OCM_CENTER_COOR, KIDNEY_DISH_EMPTY, HAS_GET_REPLENISH_ITEMLISTS, ITEM_TO_REPLENISH, PT1_PROCESS_COMPLETED, RTL_WRAPPED, TCJ_WRAPPED, LOS_WRAPPED
    HAS_INITIALISE_IB = False
    IB_STATUS = {'ready': True, 'time_last_load': time.time(), 'inst_last_load': 'NAN'}
    FORCE_ISOLATE = False
    DRAG_COUNTER = 0
    INSTRUMENT_DETECTED = {'name':'', 'orientation':'', 'coor_to_go': PoseStamped(), 'drag_direction':'', 'u': 0, 'v':0}
    FAULTY_OCM_INSTRUMENT = {'name':'', 'orientation':'', 'coor_to_go': PoseStamped(), 'drag_direction':'', 'u': 0, 'v':0, 'try_count': 0}
    OCM_STATUS = {'mode': 1, 'try_count': 0}
    PREVIOUS_TRIED_INSTRUMENT = {'name': '', 'u': 0, 'v':0, 'try_count':0, 'eliminate': False}
    EMPTY_COUNTER = 0
    F = None
    PT1_CENTER_COOR = None
    OCM_CENTER_COOR = None
    KIDNEY_DISH_EMPTY = False    #should be false, else wont go drybay to collect more inst, left True for debugging
    HAS_GET_REPLENISH_ITEMLISTS = False
    ITEM_TO_REPLENISH = None
    RTL_WRAPPED = False
    TCJ_WRAPPED = False
    LOS_WRAPPED = False

def drybay_var_init():
    global TRAY_REFERENCE_POINT
    TRAY_REFERENCE_POINT = {'u':0, 'v':0, 'theta':0, 'coor':PoseStamped()}
    # TRAY_REFERENCE_POINT['coor'].pose.position.x = 0.04655389891
    # TRAY_REFERENCE_POINT['coor'].pose.position.y = -0.309634076615
    # TRAY_REFERENCE_POINT['coor'].pose.position.z = 1.08240344639

def place_success_check(module):
    #check item at destination camera, to implement this in future
    print("check item at ib, pt1 or pt2 or drybay")
    return True

def launch_pt2_camera():
    webui_goal = WebUIGoal()
    webui_goal.task = webui_goal.GET_ACTIVE_MODULE
    WEBUI_CLIENT.send_goal(webui_goal)
    WEBUI_CLIENT.wait_for_result()
    result = WEBUI_CLIENT.get_result()
    if "/pt2/rc_visard_driver" in result.active_modules:
        return True
    else:
        print("pt2 not launched, signalling ur5 side to kill cam and launching at ur10 now")
        # to include ur5 side pt2 camera killing
        webui_goal = WebUIGoal()
        webui_goal.task = webui_goal.LAUNCH
        webui_goal.module = "pt2_camera"
        WEBUI_CLIENT.send_goal(webui_goal)
        WEBUI_CLIENT.wait_for_result()
        result = WEBUI_CLIENT.get_result()
        if result.success: 
            return True
        else:
            print("error during pt2 launching")
            return False

def list_to_posestamped(input_list):
    pose = PoseStamped()
    pose.pose.position.x = input_list[0]
    pose.pose.position.y = input_list[1]
    pose.pose.position.z = input_list[2]
    pose.pose.orientation.x = input_list[3]
    pose.pose.orientation.y = input_list[4]
    pose.pose.orientation.z = input_list[5]
    pose.pose.orientation.w = input_list[6]
    return pose

def plc_task(task, which_pigeonhole = 0):
    plc_request = PLCServiceRequest()
    if task == "system_start":
        plc_request.task = plc_request.WRITE
        plc_request.defined_pin = "SYSTEM_START"
        plc_request.value = True
        resp = PLC_CLIENT(plc_request)
        return resp.success
        
    elif task[:10] == "drawer_ext":  #drawer extend
        plc_request.task = plc_request.WRITE
        plc_request.defined_pin = task[-6:].upper() + "_DRAWER_" + str(which_pigeonhole)
        plc_request.value = True
        resp = PLC_CLIENT(plc_request)
        return resp.success

    elif task[:10] == "drawer_ret":  #drawer retract
        plc_request.task = plc_request.WRITE
        plc_request.defined_pin = task[-6:].upper() + "_DRAWER_" + str(which_pigeonhole)
        print(task[-6:].upper() + "_DRAWER_" + str(which_pigeonhole))
        plc_request.value = False
        resp = PLC_CLIENT(plc_request)
        time.sleep(0.1)
        if task[-6:] != "drybay":
            plc_request.defined_pin = task[-6:].upper() + "_RECOVERY_" + str(which_pigeonhole)    #turn off recovery pin as well just in case the process went into recovery loop earlier
            plc_request.value = False
            resp = PLC_CLIENT(plc_request)
        return resp.success
    
    elif task[:10] == "drawer_err":    #drawer error
        plc_request.task = plc_request.WRITE
        plc_request.defined_pin = task[-6:].upper() + "_ERROR_" + str(which_pigeonhole)
        plc_request.value = True
        resp = PLC_CLIENT(plc_request)
        return resp.success

    elif task[:10] == "drawer_rec":  #drawer recovery and turn off red LED
        if task[-6:] == "wetbay":
            plc_request.task = plc_request.WRITE
            plc_request.defined_pin = task[-6:].upper() + "_RECOVERY_" + str(which_pigeonhole)
            plc_request.value = True
            resp = PLC_CLIENT(plc_request)
        elif task[-6:] == "drybay":
            plc_request.task = plc_request.WRITE
            plc_request.defined_pin = task[-6:].upper() + "_DRAWER_" + str(which_pigeonhole)
            plc_request.value = True
            resp = PLC_CLIENT(plc_request)
        time.sleep(0.1)
        plc_request.defined_pin = task[-6:].upper() + "_ERROR_" + str(which_pigeonhole)
        plc_request.value = False
        resp = PLC_CLIENT(plc_request)
        return resp.success
            

    elif task == "all_drawer_err":
        plc_request.defined_pin = "SYSTEM_ERROR"
        plc_request.value = True
        resp = PLC_CLIENT(plc_request)
        return resp.success

    elif task == "all_drawer_rec":
        plc_request.defined_pin = "SYSTEM_ERROR"
        plc_request.value = False
        resp = PLC_CLIENT(plc_request)
        return resp.success 
    
    elif task[:4] == "nrsb":
        plc_request.defined_pin = "NON_RING_SPARE_BAY"
        if task[-3:] == "ext":
            plc_request.value = True
        elif task[-3:] == "ret":
            plc_request.value = False
        resp = PLC_CLIENT(plc_request)
        return resp.success 

    elif task == "debug_system_reset":
        plc_request.task = plc_request.RESET
        PLC_CLIENT(plc_request)
        return True

def recovery_placing(module, coor, item= ""):
    rospy.loginfo("recovery placing in play")
    motion_goal = RobotControlGoal()
    if module != "":
        motion_goal.option = eval("motion_goal.GO_TO_" + str(module.upper()))
    MOTION_CLIENT.send_goal(motion_goal)
    MOTION_CLIENT.wait_for_result()
    if MOTION_CLIENT.get_result().success:
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.PLACE
        if item == "wetbay_bracket" and coor.pose.position.x < 0.39:
            coor.pose.position.x += 0.05
        coor.pose.position.z += 0.1
        motion_goal.target_pose = coor
        motion_goal.extra_zdown = 0.1
        motion_goal.pigeonhole_processing = PIGEONHOLE_PROCESSING
        if module != "":
            motion_goal.starting_location = module
        MOTION_CLIENT.send_goal(motion_goal)
        MOTION_CLIENT.wait_for_result()
        if MOTION_CLIENT.get_result().success:
            rospy.loginfo("recovery placing successful")
            return True
        else: 
            rospy.logerr("error during recovery placing place routine")
            return False
    else:
        rospy.logerr("error during recovery placing module homing routine")
        return False

class frontend_logging():
    def loginfo(self, message):
        rospy.loginfo(message)
        message_data = {'main_program_message': message, 'main_program_error_message': False}
        try:
            requests.post(FRONTEND_MESSAGE_URL, json=message_data)
        except:
            #frontend not activated, ignoring
            pass

    def logwarn(self, message):
        rospy.logwarn(message)
        message_data = {'main_program_message': message, 'main_program_error_message': False}
        try:
            requests.post(FRONTEND_MESSAGE_URL, json=message_data)
        except:
            #frontend not activated, ignoring
            pass

    def logerr(self, message):
        rospy.logerr(message)
        message_data = {'main_program_message': message, 'main_program_error_message': True}
        try:
            requests.post(FRONTEND_MESSAGE_URL, json=message_data)
        except:
            #frontend not activated, ignoring
            pass

############################################################################################################### Common Robot Process to shares between modules
def get_indicator():
    motion_goal = RobotControlGoal()
    motion_goal.option = motion_goal.CHANGE_GRIPPER
    motion_goal.gripper = motion_goal.SUCTION
    MOTION_CLIENT.send_goal(motion_goal)
    MOTION_CLIENT.wait_for_result()
    if MOTION_CLIENT.get_result().success:
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.GO_TO_DRYBAY
        MOTION_CLIENT.send_goal(motion_goal)
        MOTION_CLIENT.wait_for_result()
        if MOTION_CLIENT.get_result().success:
            motion_goal.option = motion_goal.PICK_SPECIAL
            motion_goal.special_item = "drybay_picking"
            motion_goal.starting_location = "drybay"
            motion_goal.extra_argument = "drybay_indicator"
            motion_goal.target_pose = list_to_posestamped(indicator_dispenser_coor)
            MOTION_CLIENT.send_goal(motion_goal)
            MOTION_CLIENT.wait_for_result()
            if MOTION_CLIENT.get_result().success:
                return True
            else:
                LOGGING.logerr("Error during picking, retrying")
                return False
        else:
            LOGGING.logerr("Error during drybay homing, retrying")
            return False
    else:
        LOGGING.logerr("Error during gripper changing, retrying")
        return False
        
def get_robot_status():
    motion_goal = RobotControlGoal()
    motion_goal.option = motion_goal.DASHBOARD_SERVER_QUERY
    MOTION_CLIENT.send_goal(motion_goal)
    MOTION_CLIENT.wait_for_result()
    #if normal == RUNNING
    #if estop == IDLE
    motion_result = RobotControlResult()
    motion_result = MOTION_CLIENT.get_result()
    if motion_result.success:
        if motion_result.dashboard_safety_status == "ROBOT_EMERGENCY_STOP":
            return "estop"
        elif motion_result.dashboard_safety_status == "PROTECTIVE_STOP":
            return "protective_stop"
        elif motion_result.dashboard_safety_status == "NORMAL" and motion_result.dashboard_robot_status == "IDLE" and motion_result.dashboard_program_status == "PAUSE":
            return "estop_without_btn"
        elif motion_result.dashboard_safety_status == "NORMAL" and motion_result.dashboard_robot_status == "RUNNING" and motion_result.dashboard_program_status == "PLAYING":
            return "running"
        else:
            rospy.logerr("DEBUG ME")
            print(motion_result.dashboard_robot_status, motion_result.dashboard_program_status, motion_result.dashboard_safety_status)
            return "debug me"
    else:
        return False