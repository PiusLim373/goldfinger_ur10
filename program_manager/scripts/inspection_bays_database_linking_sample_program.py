#!/usr/bin/env python
import json
import requests
import time
import datetime
import os

BOX_ID = 0
NON_RING_URL = ""
RING_URL = ""
ROOT_DIR = "/home/ur10controller/Documents"

def make_and_chg_dir(dir):
    print(dir)
    try: 
        os.makedirs(dir) 
    except OSError:
        print("directory existed")
    finally:
        os.chdir(dir)
    print("here ", os.getcwd()[-len(dir):])
    if os.getcwd()[-len(dir):] == dir:
        return True
    else:
        return False

def cd_root():
    os.chdir(ROOT_DIR)
    if os.getcwd() == ROOT_DIR:
        print("successful change to root directory")
        return True
    else:
        return False

def non_ring_function():
    global BOX_ID, NON_RING_URL
    # switch to root directory
    cd_root()

    # UR10 controller or UR5 controller will send a box_id for master level reference, update box id and url accordingly
    BOX_ID = 2
    NON_RING_URL = "http://localhost:5000/api/instrument_set/" + str(BOX_ID) + "/non_ring_instrument"
    # create folder with box id and cd into it
    box_dir = str(BOX_ID).zfill(3)
    if make_and_chg_dir(box_dir):
        print("directories creation is successful, cwd: " + os.getcwd())

    #======================================================================================================================
    # after instrument is loaded into inspection bay, top camera will return item seen, create a new task in databse and folder 
    # create new task, send a put request, server will return an 'item_id', use this item_id for future updates
    instrument_type = "BAP"
    item_id = requests.put(NON_RING_URL, json={'instrument_type': instrument_type}).json()['item_id']
    # create folder with instrument_type/item id and cd into it
    instrument_dir = instrument_type + "/" + item_id[4:]
    if make_and_chg_dir(instrument_dir):
        print("directories creation is successful, cwd: " + os.getcwd())

    # after ai check the item, render the image with cv2.imwrite to directory with this naming convention: 
    # (box_id)_(datetime)_(instrument_type)_(item_id)_(camera_position).png
    # eg: 002_1641182568133_BAP_1_front.png
    # then send this name string to database for storing 
    camera_position ="front"
    ai_output_name = str(BOX_ID).zfill(3) + "_" + str(int(time.time())) + "_" + instrument_type + "_" + item_id[4:] + "_" + camera_position +".png"
    if requests.put(NON_RING_URL, json={'instrument_type': instrument_type, 'item_id':item_id, 'ai_output':ai_output_name}).json()['success']:
        print("successfully updated")
    else:
        print("error during update")
    
    raw_input()
    # if all inspections pass, change the inspection_result of the item to pass
    if requests.put(NON_RING_URL, json={'instrument_type': instrument_type, 'item_id':item_id, 'inspection_result':'pass'}).json()['success']:
        print("successfully updated")
    else:
        print("error during update")

    raw_input()
    # else if any inspections return defective, change the inspection_result of the item to fail
    if requests.put(NON_RING_URL, json={'instrument_type': instrument_type, 'item_id':item_id, 'inspection_result':'fail'}).json()['success']:
        print("successfully updated")
    else:
        print("error during update")

    raw_input()
    # else if for some reason instrument went missing or dropped in process, change the inspection_result of the item to missing
    if requests.put(NON_RING_URL, json={'instrument_type': instrument_type, 'item_id':item_id, 'inspection_result':'missing'}).json()['success']:
        print("successfully updated")
    else:
        print("error during update")
    
    #======================================================================================================================
    # change directory to root and ready for next iteration
    cd_root()

def ring_function():
    global BOX_ID, RING_URL
    # switch to root directory
    cd_root()

    # UR10 controller or UR5 controller will send a box_id for master level reference, update box id and url accordingly
    BOX_ID = 2
    RING_URL = "http://localhost:5000/api/instrument_set/" + str(BOX_ID) + "/ring_instrument"
    # create folder with box id and cd into it
    box_dir = str(BOX_ID).zfill(3)
    if make_and_chg_dir(box_dir):
        print("directories creation is successful, cwd: " + os.getcwd())

    #======================================================================================================================
    # after instrument is loaded into inspection bay, top camera will return item seen, create a new task in databse and folder 
    # create new task, send a put request, server will return an 'item_id', use this item_id for future updates
    instrument_type = "FTL"
    item_id = requests.put(RING_URL, json={'instrument_type': instrument_type}).json()['item_id']

    # create folder with instrument_type/item id and cd into it
    instrument_dir = instrument_type + "/" + item_id[4:]
    if make_and_chg_dir(instrument_dir):
        print("directories creation is successful, cwd: " + os.getcwd())

    # after ai check the item, render the image with cv2.imwrite to directory with this naming convention: 
    # (box_id)_(datetime)_(instrument_type)_(item_id)_(camera_position).png
    # eg: 002_1641182568_BAP_1_front.png
    # then send this name string to database for storing 
    camera_position ="front"
    ai_output_name = str(BOX_ID).zfill(3) + "_" + str(int(time.time())) + "_" + instrument_type + "_" + item_id[4:] + "_" + camera_position +".png"
    if requests.put(RING_URL, json={'instrument_type': instrument_type, 'item_id':item_id, 'ai_output':ai_output_name}).json()['success']:
        print("successfully updated")
    else:
        print("error during update")
    
    raw_input()
    # if all inspections pass, change the inspection_result of the item to pass
    if requests.put(RING_URL, json={'instrument_type': instrument_type, 'item_id':item_id, 'inspection_result':'pass'}).json()['success']:
        print("successfully updated")
    else:
        print("error during update")

    raw_input()
    # else if any inspections return defective, change the inspection_result of the item to fail
    if requests.put(RING_URL, json={'instrument_type': instrument_type, 'item_id':item_id, 'inspection_result':'fail'}).json()['success']:
        print("successfully updated")
    else:
        print("error during update")

    raw_input()
    # else if for some reason instrument went missing or dropped in process, change the inspection_result of the item to missing
    if requests.put(RING_URL, json={'instrument_type': instrument_type, 'item_id':item_id, 'inspection_result':'missing'}).json()['success']:
        print("successfully updated")
    else:
        print("error during update")

    #======================================================================================================================
    # change directory to root and ready for next iteration
    cd_root()


if __name__ == "__main__":
    # non_ring_function()
    ring_function()