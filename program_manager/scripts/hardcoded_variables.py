#!/usr/bin/env python

#for wetbay
# sb_placing_coor = [[-1.21, -0.03, 0.86, 1, 0, 0, 0], [-0.96, -0.04, 0.86, 1, 0, 0, 0],[-1.1, -0.34, 0.86, 1, 0, 0, 0]]
kidney_dish_placing_coor = [-0.125, -0.95, 1.295, 0.707, 0.707, 0, 0] #was [-0.02, -0.95, 1.295, 0.707, 0.707, 0, 0]
gallipot_placing_coor = [0.105, -0.92, 1.27, 0, -1, 0, 0] #was[0.21, -0.93, 1.27, 0, -1, 0, 0]

#for pt1
ib_placeholder = [-0.256, 0.525, 0.95+0.15, 1, 0, 0, 0] #was[-0.305, 0.52, 0.99+0.05, 1, 0, 0, 0]
ib_tcj_input_placeholder = [-0.21, 0.527, 0.948+0.15, -0.707, 0.707, 0, 0] #was -0.255, 0.52, 0.99 + 0.05, -0.707, 0.707, 0, 0


ib_indicator_release_placeholder = [-0.21, 0.527, 0.948, 1, 0, 0, 0]  #was[-0.261, 0.52, 0.99, 1, 0, 0, 0]
half_kidney_dish_picking_coor = [-0.105, -0.95, 1.265, 0.707, 0.707, 0, 0] #was [0, -0.95, 1.265, 0.707, 0.707, 0, 0]
instrument_width = {'TCJ': {'F': 3.2, 'S': 6.4}, 'RTL':{'F1': 6.4, 'F2': 3.2, 'S':6.4}, 'FOR':{'F1': 9.6, 'F2': 12.8, 'S1':6.4, 'S2': 3.2}, 'BAP': {'F1': 6.4, 'F2': 9.6}, 'SPR': {'F1': 0.00, 'F2': 3.2}}
sparebay_ruler_placeholder = [0.75, 0.58, 1.1, 1, 0, 0, 0]
sparebay_tool_release_placeholder = [0.7, 0.58, 1.1, 1, 0, 0, 0]
ocm_pin_a = 7
ocm_pin_b = 2
rotating_table_pin = 3


#for drybay
indicator_dispenser_coor = [-0.465, -0.87, 1.245, 0.707, -0.707, 0, 0] #was[-0.36, -0.87, 1.245, 0.707, -0.707, 0, 0]
kidney_dish_picking_coor = [-0.285, -0.94, 1.23, 0, -1, 0, 0] #was[-0.19, -0.95, 1.23, 0, -1, 0, 0]
gallipot_picking_coor = [-0.015, -0.948, 1.23, 0, -1, 0, 0] #was [0.085, -0.953, 1.23, 0, -1, 0, 0]
wrapping_bay_tray_placeholder = [-0.25, 0.4, 0.75, 1, 0, 0, 0]
printed_list_placeholder = [0.9, -0.2, 1.3, 1, 0, 0, 0] #was[0.8, -0.55, 1.25, 1, 0, 0, 0] 
sticker_tag_placeholder = [0.9, 0.15, 1.3, 1, 0, 0, 0] #was[0.8, -0.25, 1.25, 1, 0, 0, 0] 

