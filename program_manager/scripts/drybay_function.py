#!/usr/bin/env python
import requests
import rospy
import globals
from hardcoded_variables import *
from geometry_msgs.msg import PoseStamped
import smach
import os
from time import sleep
import serial

import rospy

from roswebui.msg import *
from ur10_pap.msg import *
from pcl_server.msg import *
from ai_server.msg import *
from plc_server.srv import *


######################################################################### common function ###########################################333
def pick_success_check(item):
    #check ai result
    # return True
    if item == "bracket":
        ai_goal = AIServerGoal()
        ai_goal.task = ai_goal.PICK_SUCCESS_CHECK
        ai_goal.instrument_needed = item
        ai_goal.which_module = ai_goal.PT2
        globals.AI_CLIENT.send_goal(ai_goal)
        globals.AI_CLIENT.wait_for_result()
        ai_result = AIServerResult()
        ai_result = globals.AI_CLIENT.get_result()
        return ai_result.item_remaining
    elif item == "WBT":
        return True
        ai_goal = AIServerGoal()
        ai_goal.task = ai_goal.DETECT_WBT
        ai_goal.which_module = ai_goal.PT1
        globals.AI_CLIENT.send_goal(ai_goal)
        globals.AI_CLIENT.wait_for_result()
        ai_result = AIServerResult()
        ai_result = globals.AI_CLIENT.get_result()
        if ai_result.topick:
            # ar marker located, picking successful
            return True
        else:
            #cant see the marker, picking failed
            return False

def place_success_check(item):
    if item == "GAL" or item == "KID":
        ai_goal = AIServerGoal()
        ai_goal.instrument_needed = item
        ai_goal.which_module = ai_goal.DRYBAY
        globals.AI_CLIENT.send_goal(ai_goal)
        globals.AI_CLIENT.wait_for_result()
        ai_result = AIServerResult()
        ai_result = globals.AI_CLIENT.get_result()
        if ai_result.topick and ai_result.ai_output.v >= 730:
            rospy.logerr(item + " still at flip bay")
            return False 
        else:
            globals.LOGGING.loginfo("Can't locate "+ item + " at Flip Bay, transfer successful") #high chance that the ai cant recognise kid and gal at drybay pigeonhole, change the detection method to detect if it is still at flip bay
            return True
    else:
        ai_goal = AIServerGoal()
        ai_goal.instrument_needed = item
        ai_goal.which_module = ai_goal.DRYBAY
        globals.AI_CLIENT.send_goal(ai_goal)
        globals.AI_CLIENT.wait_for_result()
        ai_result = AIServerResult()
        ai_result = globals.AI_CLIENT.get_result()
        if ai_result.topick:
            return True
        else:
            rospy.logerr("ai cant see " + item + " at destination")
            return False


def query_ai(item):
    '''Query ai_server to get the u, v and theta of the item'''
    ai_return = {'item': '', 'u':0, 'v':0, 'theta':0, 'success': False}
    ai_goal = AIServerGoal()
    ai_goal.instrument_needed = item
    if item == "WBT":
        ai_goal.which_module = ai_goal.PT1
        ai_goal.task = ai_goal.DETECT_WBT
    elif item == "pt2_FOB" or item == "pt2_MIB" or item == "pt2_TEB" or item == "pt2_BRAC_0":
        ai_goal.which_module = ai_goal.PT2
        ai_goal.instrument_needed = item[4:]
    else:
        ai_goal.which_module = ai_goal.DRYBAY
    globals.AI_CLIENT.send_goal(ai_goal)
    globals.AI_CLIENT.wait_for_result()
    ai_result = AIServerResult()
    ai_result = globals.AI_CLIENT.get_result()
    if ai_result.topick:
        ai_return['name'] = ai_result.ai_output.item
        ai_return['u'] = ai_result.ai_output.u
        ai_return['v'] = ai_result.ai_output.v
        ai_return['theta'] = ai_result.ai_output.theta
        ai_return['success'] = True
    return ai_return

def query_pcl(u, v, theta, corrected_angle, which_camera = "drybay"):
    '''Query plc_server to get the coordibate based on the u, v and theta input'''
    angle_list = [0, 45, 90, 135, 180]
    closest_angle_list = []
    for x  in angle_list:
        closest_angle_list.append(abs(theta - x))
    closest_angle = angle_list[closest_angle_list.index(min(closest_angle_list))]
    angle_pixel_crosstable = {0: (0, -1), 45:(1, -1), 90:(1, 0), 135:(1, 1), 180: (0, 1), 225:(-1, 1), 270:(-1, 0), 315:(-1, -1)}
    
    angle_list = [0, 45, 90, 135, 180, 225, 270, 315]
    if closest_angle < 180:
        priority_angle_list = [closest_angle, closest_angle+180]
    else:
        priority_angle_list = [closest_angle, closest_angle-180]
    for x in priority_angle_list:
        angle_list.remove(x)
    total = [-1] + priority_angle_list + angle_list
    
    has_depth = False
    return_pose = None

    pcl_goal = PCLServerGoal()
    pcl_goal.task = pcl_goal.UPDATE_POINTCLOUD
    pcl_goal.module = which_camera
    globals.PCL_CLIENT.send_goal(pcl_goal)
    globals.PCL_CLIENT.wait_for_result()
    pcl_result = PCLServerResult()
    pcl_result = globals.PCL_CLIENT.get_result()
    if pcl_result.success:
        for i in range(1, 4):
            print("i:", i)
            for x in total:
                if x == -1:
                    check_u = u
                    check_v = v
                else:
                    check_u = u + angle_pixel_crosstable[x][0]*i
                    check_v = v + angle_pixel_crosstable[x][1]*i
                rospy.loginfo("checking point cloud at u, v: [" + str(check_u) + ", " +str(check_v) + "]")
                print("debug", corrected_angle)
                pcl_input = [check_u, check_v, corrected_angle]
                pcl_goal = PCLServerGoal()
                pcl_goal.task = pcl_goal.GET_COOR
                pcl_goal.pcl_input = pcl_input
                pcl_goal.module = which_camera
                globals.PCL_CLIENT.send_goal(pcl_goal)
                globals.PCL_CLIENT.wait_for_result()
                pcl_result = PCLServerResult()
                pcl_result = globals.PCL_CLIENT.get_result()
                has_depth = pcl_result.success
                if has_depth:
                    rospy.loginfo("depth get successfully")
                    return_pose = pcl_result.coor_wrt_world
                    break
        return return_pose
    else:
        rospy.logerr("something wrong when capturing depth")
        return return_pose

def query_indicator_dispenser():
    return True
    if requests.get(globals.INDICATOR_DISPENSER_URL).json()['status'] == "empty":
        globals.LOGGING.logerr("Indicator Dispenser signalled insufficient indicator, please refill and continue")
        return False
    else:
        return True


######################################################################### common function end ###########################################333

######################################################################### SMACH CLASS ###########################################333
class PlaceTrayPaperSM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])
    
    def execute(self, userdata):
        """Request Human Intervention to place a tray paper at drybay, to changed to placing sequence if the module is ever made lmao"""
        globals.LOGGING.loginfo("Retracting drawer for Tray Paper placement")
        return 'success'
        
class GetTrayDetailsSM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail','retry'])
    
    def execute(self, userdata):
        '''Get the coordinate and detials of the tray for later placing usage'''
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT > 3:
            globals.TRIAL_COUNT = 0
            return 'fail'
        globals.LOGGING.loginfo("Getting coordinate and details of tray...")
        ai_return = query_ai("TRY")
        if ai_return['success']:
            pose = query_pcl(ai_return['u'], ai_return['v'], ai_return['theta'], 180)
            if not pose:
                globals.LOGGING.logerr("Failed to get depth data, tray might not loaded properly, please load the tray properly with linen in place and try again")
                return 'retry'
            else:
                globals.TRAY_REFERENCE_POINT['u'] = ai_return['u']
                globals.TRAY_REFERENCE_POINT['v'] = ai_return['v']
                globals.TRAY_REFERENCE_POINT['theta'] = ai_return['theta']
                globals.TRAY_REFERENCE_POINT['coor'] = pose
                globals.LOGGING.loginfo("Successfully got tray's details")
                globals.PROCESS_COMPLETED = 8
                globals.TRIAL_COUNT = 0
                print(globals.TRAY_REFERENCE_POINT)
                return 'success'
        else:
            globals.LOGGING.logerr("Unable to locate tray coordinate at Drybay, please load the tray properly with linen in place and try again")
            return 'fail'

class TransferIndicatorSM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail', 'retry'])
    
    def execute(self, userdata):
        '''Transferring Indicator to Drybay'''
        #query indicator dispensor if have indicator
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT > 3:
            globals.TRIAL_COUNT = 0
            return 'fail'
        if not query_indicator_dispenser():
            return 'fail'
        globals.LOGGING.loginfo("Transferring an indicator to DryBay...")
        if not globals.get_indicator():
            return 'retry'
        drybay_indicator = [globals.TRAY_REFERENCE_POINT['coor'].pose.position.x, globals.TRAY_REFERENCE_POINT['coor'].pose.position.y - 0.25, 9.99, 0, 1, 0, 0]
        if globals.PIGEONHOLE_PROCESSING <= 3:
            drybay_indicator[2] = 1.1
        elif globals.PIGEONHOLE_PROCESSING >3:
            drybay_indicator[2] = 0.8
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.PLACE
        motion_goal.target_pose = globals.list_to_posestamped(drybay_indicator)
        motion_goal.starting_location = "drybay"
        motion_goal.pigeonhole_processing = globals.PIGEONHOLE_PROCESSING
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            globals.LOGGING.loginfo("Indicator transfer is successful")
            globals.PROCESS_COMPLETED = 9
            globals.TRIAL_COUNT = 0
            return 'success'
        else:
            rospy.logerr("Error during placing, retrying")
            return 'retry'

class QueryUR5ProcessSM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])
    
    def execute(self, userdata):
        '''query ur5 if ring instrument process has been completed'''
        raw_input('enter to simulate ur5 finish processing')
        globals.LOGGING.logwarn("Press enter to simulate ur5 finish processing")
        # while requests.get(globals.UR5_QUERY_PROCESS_URL).json()['program_status']:
        #     # pooling to check if ur5 has completed
        #     sleep(3)
        if not globals.launch_pt2_camera():
            return 'retry'
        return 'success'
        # webui_goal = WebUIGoal()
        # webui_goal.task = webui_goal.LAUNCH
        # webui_goal.module = "pt2_camera"
        # globals.WEBUI_CLIENT.send_goal(webui_goal)
        # globals.WEBUI_CLIENT.wait_for_result()
        # if globals.WEBUI_CLIENT.get_result().success:
        #     #successfully launched pt2 camera driver, ready for bracket transfer
        #     return 'success'

def pick_forcep_brac():
    forcep_brac_ai_return = query_ai("pt2_FOB")
    if not forcep_brac_ai_return['success']:
        globals.LOGGING.logwarn("Can't locate Forcep Bracket at Processing Table 2")
        return False
    forcep_brac_pose = query_pcl(forcep_brac_ai_return['u'], forcep_brac_ai_return['v'], forcep_brac_ai_return['theta'] + 90, 180 + 90, which_camera="pt2")
    if forcep_brac_pose == None:
        globals.LOGGING.logwarn("Frocep Bracket coor return none")
        return False
    forcep_brac_pose.pose.position.z = 0.865
    forcep_brac_pose.pose.position.y += 0.01 #was-= 0.003
    print(forcep_brac_pose)
    motion_goal = RobotControlGoal()
    motion_goal.option = motion_goal.CHANGE_GRIPPER
    motion_goal.gripper = motion_goal.PARALLEL
    globals.MOTION_CLIENT.send_goal(motion_goal)
    globals.MOTION_CLIENT.wait_for_result()
    if globals.MOTION_CLIENT.get_result().success:
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.GO_TO_PT2
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            motion_goal.option = motion_goal.PICK_SPECIAL
            motion_goal.special_item = "drybay_picking"
            motion_goal.starting_location = "pt2"
            motion_goal.extra_argument = "drybay_bracket"
            motion_goal.target_pose = forcep_brac_pose
            motion_goal.extra_zdown = 0.025
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                motion_goal = RobotControlGoal()
                motion_goal.option = motion_goal.GO_TO_DRYBAY
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    return True
                else: 
                    rospy.logerr("error during drybay homing")
                    globals.recovery_placing("pt2", forcep_brac_pose, "drybay_bracket")
                    return False
            else: 
                rospy.logerr("error during bracket picking")
                globals.recovery_placing("pt2", forcep_brac_pose, "drybay_bracket")
                return False
        else: 
            rospy.logerr("error during pt2 homing")
            return False
    else: 
        rospy.logerr("error during gripper changing")
        return False

def pick_mix_brac():
    mix_brac_ai_return = query_ai("pt2_MIB")
    if not mix_brac_ai_return['success']:
        globals.LOGGING.logwarn("Can't locate Mix Bracket at Processing Table 2")
        return False
    mix_brac_pose = query_pcl(mix_brac_ai_return['u'], mix_brac_ai_return['v'], mix_brac_ai_return['theta'] + 90, 180 + 90, which_camera="pt2")
    if mix_brac_pose == None:
        globals.LOGGING.logwarn("Mix Bracket coor return none")
        return False
    mix_brac_pose.pose.position.z = 0.865
    mix_brac_pose.pose.position.y += 0.01 #was-= 0.003
    print(mix_brac_pose)
    motion_goal = RobotControlGoal()
    motion_goal.option = motion_goal.CHANGE_GRIPPER
    motion_goal.gripper = motion_goal.PARALLEL
    globals.MOTION_CLIENT.send_goal(motion_goal)
    globals.MOTION_CLIENT.wait_for_result()
    if globals.MOTION_CLIENT.get_result().success:
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.GO_TO_PT2
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            motion_goal.option = motion_goal.PICK_SPECIAL
            motion_goal.special_item = "drybay_picking"
            motion_goal.starting_location = "pt2"
            motion_goal.extra_argument = "drybay_bracket"
            motion_goal.target_pose = mix_brac_pose
            motion_goal.extra_zdown = 0.025
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                motion_goal = RobotControlGoal()
                motion_goal.option = motion_goal.GO_TO_DRYBAY
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    return True
                else: 
                    rospy.logerr("error during drybay homing")
                    globals.recovery_placing("pt2", mix_brac_pose, "drybay_bracket")
                    return False
            else: 
                rospy.logerr("error during bracket picking")
                globals.recovery_placing("pt2", mix_brac_pose, "drybay_bracket")
                return False
        else: 
            rospy.logerr("error during pt2 homing")
            return False
    else: 
        rospy.logerr("error during gripper changing")
        return False

def pick_temp_brac():
    temp_brac_ai_return = query_ai("pt2_TEB")
    if not temp_brac_ai_return['success']:
        globals.LOGGING.logwarn("Can't locate Temp Bracket at Processing Table 2")
        return False
    temp_brac_pose = query_pcl(temp_brac_ai_return['u'], temp_brac_ai_return['v'], temp_brac_ai_return['theta'] + 90, 180 + 90, which_camera="pt2")
    if temp_brac_pose == None:
        globals.LOGGING.logwarn("Frocep Bracket coor return none")
        return False
    temp_brac_pose.pose.position.z = 0.865
    temp_brac_pose.pose.position.y -= 0.003

    brac_0_ai_return = query_ai("pt2_BRAC_0")
    if not brac_0_ai_return['success']:
        globals.LOGGING.logwarn("Can't locate Temp Bracket at Processing Table 2")
        return False
    brac_0_pose = query_pcl(brac_0_ai_return['u'], brac_0_ai_return['v'], 180 + 90, 180 + 90, which_camera="pt2")
    if brac_0_pose == None:
        globals.LOGGING.logwarn("Temp Bracket coor return none")
        return False
    brac_0_pose.pose.position.z = 0.855
    brac_0_pose.pose.position.y -= 0.02

    print(temp_brac_pose, brac_0_pose)
    motion_goal = RobotControlGoal()
    motion_goal.option = motion_goal.CHANGE_GRIPPER
    motion_goal.gripper = motion_goal.PARALLEL
    globals.MOTION_CLIENT.send_goal(motion_goal)
    globals.MOTION_CLIENT.wait_for_result()
    if globals.MOTION_CLIENT.get_result().success:
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.GO_TO_PT2
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            motion_goal.option = motion_goal.PICK_SPECIAL
            motion_goal.special_item = "drybay_picking"
            motion_goal.extra_argument = "drybay_bracket"
            motion_goal.target_pose = temp_brac_pose
            motion_goal.extra_zdown = 0.025
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                motion_goal = RobotControlGoal()
                motion_goal.option = motion_goal.PLACE
                motion_goal.target_pose = brac_0_pose
                motion_goal.starting_location = "pt2"
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    rospy.loginfo("temp brac transfer is successful")
                    return True
                else: 
                    rospy.logerr("error during temp brac placing")
                    globals.recovery_placing("pt2", temp_brac_pose, "drybay_bracket")
                    return False
            else: 
                rospy.logerr("error during temp bracket picking")
                globals.recovery_placing("pt2", temp_brac_pose, "drybay_bracket")
                return False
        else: 
            rospy.logerr("error during pt2 homing")
            return False
    else: 
        rospy.logerr("error during gripper changing")
        return False

############## bracket placeholder, to update this with actual AI ends ######################################333
class TransferForcepBrac(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail', 'retry'])
    
    def execute(self, userdata):
        """Transferring forcep bracket to drybay"""
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT > 3:
            globals.TRIAL_COUNT = 0
            return 'fail'
        globals.LOGGING.loginfo("Transferring Forcep Bracket to Drybay...")
        drybay_sb1 = [globals.TRAY_REFERENCE_POINT['coor'].pose.position.x + 0.051, globals.TRAY_REFERENCE_POINT['coor'].pose.position.y - 0.31, 9.99, 0.985, 0.174, 0, 0]
        if globals.PIGEONHOLE_PROCESSING <= 3:
            drybay_sb1[2] = 1.185     #1.175
        elif globals.PIGEONHOLE_PROCESSING >=4:
            drybay_sb1[2] = 0.855     # 0.845
        print("drybay_sb1", drybay_sb1)
        if not pick_forcep_brac():     
            return 'retry'
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.PLACE_SPECIAL
        motion_goal.special_item = "drybay_forcep_bracket"
        motion_goal.extra_argument = "1"
        motion_goal.pigeonhole_processing = globals.PIGEONHOLE_PROCESSING
        motion_goal.target_pose = globals.list_to_posestamped(drybay_sb1)
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            ai_return = query_ai("FOB")
            if not ai_return['success']:
                globals.LOGGING.logerr("AI can't locate FOB, please assist to place it down nicely")
                return 'fail'
            pose = query_pcl(ai_return['u'], ai_return['v'], ai_return['theta'], ai_return['theta'])
            if not pose:
                globals.LOGGING.logerr("PCL can't locate FOB, please assist to place it down nicely")
                return 'fail'
            print(pose)
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.CHANGE_GRIPPER
            motion_goal.gripper = motion_goal.DRAG
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                motion_goal = RobotControlGoal()
                motion_goal.option = motion_goal.PLACE_SPECIAL
                motion_goal.special_item = "drybay_forcep_bracket"
                motion_goal.extra_argument = "2"
                motion_goal.pigeonhole_processing = globals.PIGEONHOLE_PROCESSING
                pose.pose.position.x += 0.04
                motion_goal.target_pose = pose
                if globals.PIGEONHOLE_PROCESSING == 1:
                    globals.TRAY_REFERENCE_POINT['coor'].pose.position.z = 1.167 
                elif globals.PIGEONHOLE_PROCESSING == 2:
                    globals.TRAY_REFERENCE_POINT['coor'].pose.position.z = 1.168 #1.165
                elif globals.PIGEONHOLE_PROCESSING == 3:
                    globals.TRAY_REFERENCE_POINT['coor'].pose.position.z = 1.167 
                elif globals.PIGEONHOLE_PROCESSING == 4:
                    globals.TRAY_REFERENCE_POINT['coor'].pose.position.z = 0.839 #0.849
                elif globals.PIGEONHOLE_PROCESSING == 5:
                    globals.TRAY_REFERENCE_POINT['coor'].pose.position.z = 0.835 #845
                elif globals.PIGEONHOLE_PROCESSING == 6:
                    globals.TRAY_REFERENCE_POINT['coor'].pose.position.z = 0.83 #pointcloud issue, 0.84
                motion_goal.reference_pose = globals.TRAY_REFERENCE_POINT['coor']
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    if place_success_check("FOB"):
                        globals.LOGGING.loginfo("Forcep Bracket transfer is successful")
                        globals.PROCESS_COMPLETED = 10
                        globals.TRIAL_COUNT = 0
                        return 'success'
                    else:
                        globals.LOGGING.logerr("AI can't locate FOB after side pushing, please assist to place it down nicely")
                        return 'fail'
                else:
                    globals.LOGGING.logerr("Error during bracket pushing, bracket placing process can't continue, please assist to place it down nicely")
                    return 'fail'
            else:
                globals.LOGGING.logerr("Error during gripper chaging, bracket placing process can't continue, please assist to place it down nicely")
                return 'fail'
        else:
            globals.LOGGING.logerr("Error during bracket placement, bracket placing process can't continue, please assist to place it down nicely")
            return 'fail'
                

class TransferMixBrac(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail', 'retry'])
    
    def execute(self, userdata):
        '''Transferring Mix Bracket to Drybay'''
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT > 3:
            globals.TRIAL_COUNT = 0
            return 'fail'
        globals.LOGGING.loginfo("Transferring Mixed Bracket to Drybay...")
        # drybay_sb2 = [globals.TRAY_REFERENCE_POINT['coor'].pose.position.x + 0.051, globals.TRAY_REFERENCE_POINT['coor'].pose.position.y - 0.091, 9.99, 0.174, -0.985, 0, 0]
        drybay_sb2 = [globals.TRAY_REFERENCE_POINT['coor'].pose.position.x, globals.TRAY_REFERENCE_POINT['coor'].pose.position.y - 0.09, 9.99, -0.044, -0.999, 0, 0] #y was -0.11, qutenion was 0,1,0,0
        if globals.PIGEONHOLE_PROCESSING == 1 or globals.PIGEONHOLE_PROCESSING == 3:
            drybay_sb2[2] = 1.173      
        elif globals.PIGEONHOLE_PROCESSING == 2:
            drybay_sb2[2] = 1.177  
        elif globals.PIGEONHOLE_PROCESSING >= 4: #pigeonhole 5 is calibrated to be 0.84, but testing 0.845 will works such that >4 can be generalised 
            drybay_sb2[2] = 0.845
        print("drybay_sb2", drybay_sb2)
        if not pick_mix_brac():             #to replace with ai to search for mix bracket
            return 'retry'
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.PLACE_SPECIAL
        motion_goal.special_item = "drybay_mix_bracket"
        motion_goal.pigeonhole_processing = globals.PIGEONHOLE_PROCESSING
        motion_goal.target_pose = globals.list_to_posestamped(drybay_sb2)
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            if place_success_check("MIB"):
                globals.LOGGING.loginfo("Mixed Bracket transfer is successful")
                globals.PROCESS_COMPLETED = 11
                globals.TRIAL_COUNT = 0
                return 'success'
            else:
                globals.LOGGING.logerr("AI can't locate MIB after side pushing, please assist to place it down nicely")
                return 'fail'
        else:
            globals.LOGGING.logerr("Error during bracket pushing, bracket placing process can't continue, please assist to place it down nicely")
            return 'fail'
   
class TransferGallipotDBSM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'fail', 'retry'])
    
    def execute(self, userdata):
        globals.PROCESS_COMPLETED = 11 #remove this
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT > 3:
            globals.TRIAL_COUNT = 0
            return 'fail'
        globals.LOGGING.loginfo("Activating Flip Bay to flip Kidney Dish and Gallipot")
        os.system("rosservice call /ur_hardware_interface/set_io 1 0 1")
        sleep(0.5)
        os.system("rosservice call /ur_hardware_interface/set_io 1 0 0")
        sleep(5)
        globals.LOGGING.loginfo("Transferring Gallipot to Drybay...")
        # commented due to gallipot ai not performing
        # ai_return = query_ai('GAL')
        # try:
        #     if ai_return['v'] < 730: 
        #         globals.LOGGING.logerr("Unable to locate Gallipot at Flip Bay, item probably dropped during WetBay transfer, please assist to load in Gallipot at DryBay")
        #         return 'fail'
        # except:
        #     globals.LOGGING.logerr("Unable to locate Gallipot at Flip Bay, item probably dropped during WetBay transfer, please assist to load in Gallipot at DryBay")
        #     return 'fail'
        drybay_gallipot = [globals.TRAY_REFERENCE_POINT['coor'].pose.position.x + 0.08, globals.TRAY_REFERENCE_POINT['coor'].pose.position.y - 0.05, 9.99, 0, 1, 0, 0]
        if globals.PIGEONHOLE_PROCESSING <= 3:
            drybay_gallipot[2] = 1.1
        elif globals.PIGEONHOLE_PROCESSING >=4:
            drybay_gallipot[2] = 0.77
        print("drybay_gallipot", drybay_gallipot)
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.GO_TO_DRYBAY
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.CHANGE_GRIPPER
            motion_goal.gripper = motion_goal.SUCTION
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                motion_goal.option = motion_goal.PICK_SPECIAL
                motion_goal.special_item = "drybay_picking"
                motion_goal.starting_location = "drybay"
                motion_goal.extra_argument = "drybay_gallipot"
                motion_goal.target_pose = globals.list_to_posestamped(gallipot_picking_coor)
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    motion_goal.option = motion_goal.GO_TO_DRYBAY
                    globals.MOTION_CLIENT.send_goal(motion_goal)
                    globals.MOTION_CLIENT.wait_for_result()
                    if globals.MOTION_CLIENT.get_result().success:
                        motion_goal.option = motion_goal.PLACE
                        motion_goal.target_pose = globals.list_to_posestamped(drybay_gallipot)
                        motion_goal.starting_location = "drybay"
                        motion_goal.pigeonhole_processing = globals.PIGEONHOLE_PROCESSING
                        globals.MOTION_CLIENT.send_goal(motion_goal)
                        globals.MOTION_CLIENT.wait_for_result()
                        if globals.MOTION_CLIENT.get_result().success:
                            if place_success_check("GAL"):
                                globals.LOGGING.loginfo("Gallipot transfer is successful")
                                globals.PROCESS_COMPLETED = 12
                                globals.TRIAL_COUNT = 0
                                return 'success'
                            else:
                                globals.LOGGING.logerr("AI can't locate Gallipot at DryBay, please assist to transfer from Flip Bay to DryBay")
                                return 'fail'
                        else:
                            globals.LOGGING.logerr("Error during placing, retrying")
                            globals.recovery_placing("drybay", globals.list_to_posestamped(gallipot_picking_coor), "drybay_gallipot")
                            return 'retry'
                    else:
                        globals.LOGGING.logerr("Error during drybay homing, retrying")
                        globals.recovery_placing("drybay", globals.list_to_posestamped(gallipot_picking_coor), "drybay_gallipot")
                        return 'retry'
                else:
                    globals.LOGGING.logerr("Error during picking, retrying")
                    globals.recovery_placing("drybay", globals.list_to_posestamped(gallipot_picking_coor), "drybay_gallipot")
                    return 'retry'
            else:
                globals.LOGGING.logerr("Error during gripper changing, retrying")
                return 'retry'
        else:
            globals.LOGGING.logerr("Error during drybay homing, retrying")
            return 'retry'

class TransferKidneyDishDBSM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail', 'retry'])
    
    def execute(self, userdata):
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT > 3:
            globals.TRIAL_COUNT = 0
            return 'fail'
        globals.LOGGING.loginfo("Transferring Kidney Dish to Drybay...")
        ai_return = query_ai('KID')
        try:
            if ai_return['v'] < 730: 
                globals.LOGGING.logerr("Unable to locate Kidney Dish at Flip Bay, item probably dropped during WetBay transfer, please assist to load in Kidney Dish at DryBay")
                return 'fail'
        except:
            globals.LOGGING.logerr("Unable to locate Kidney Dish at Flip Bay, item probably dropped during WetBay transfer, please assist to load in Kidney Dish at DryBay")
            return 'fail'
        drybay_kidney_dish = [globals.TRAY_REFERENCE_POINT['coor'].pose.position.x, globals.TRAY_REFERENCE_POINT['coor'].pose.position.y - 0.25, 9.99, 0.707, -0.707, 0, 0]
        if globals.PIGEONHOLE_PROCESSING <= 3:
            drybay_kidney_dish[2] = 1.13
            #default 1.185, but hole3 has height issue
        elif globals.PIGEONHOLE_PROCESSING >=4:
            #to verify
            drybay_kidney_dish[2] = 0.8
        print("drybay_kidney_dish", drybay_kidney_dish)
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.GO_TO_DRYBAY
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.CHANGE_GRIPPER
            motion_goal.gripper = motion_goal.SUCTION
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                motion_goal.option = motion_goal.PICK_SPECIAL
                motion_goal.special_item = "drybay_picking"
                motion_goal.starting_location = "drybay"
                motion_goal.extra_argument = "drybay_kidney_dish"
                motion_goal.target_pose = globals.list_to_posestamped(kidney_dish_picking_coor)
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    #picking completed, checking vision and froce torque
                    motion_goal.option = motion_goal.GO_TO_DRYBAY
                    globals.MOTION_CLIENT.send_goal(motion_goal)
                    globals.MOTION_CLIENT.wait_for_result()
                    if globals.MOTION_CLIENT.get_result().success:
                        motion_goal.option = motion_goal.PLACE
                        motion_goal.target_pose = globals.list_to_posestamped(drybay_kidney_dish)
                        motion_goal.starting_location = "drybay"
                        motion_goal.pigeonhole_processing = globals.PIGEONHOLE_PROCESSING
                        globals.MOTION_CLIENT.send_goal(motion_goal)
                        globals.MOTION_CLIENT.wait_for_result()
                        if globals.MOTION_CLIENT.get_result().success:
                            rospy.loginfo("kidney_dish transfer is successful")
                            if place_success_check("KID"):
                                globals.LOGGING.loginfo("Kidney Dish transfer is successful")
                                globals.PROCESS_COMPLETED = 13
                                globals.TRIAL_COUNT = 0
                                return 'success'
                            else:
                                globals.LOGGING.logerr("AI can't locate Kidney Dish at DryBay, please assist to transfer from Flip Bay to DryBay")
                                return 'fail'
                        else:
                            globals.LOGGING.logerr("Error during placing, retrying")
                            globals.recovery_placing("drybay", globals.list_to_posestamped(kidney_dish_picking_coor), "drybay_kidney_dish")
                            return 'retry'
                    else:
                        globals.LOGGING.logerr("Error during drybay homing, retrying")
                        globals.recovery_placing("drybay", globals.list_to_posestamped(kidney_dish_picking_coor), "drybay_kidney_dish")
                        return 'retry'
                else:
                    globals.LOGGING.logerr("Error during picking, retrying")
                    globals.recovery_placing("drybay", globals.list_to_posestamped(kidney_dish_picking_coor), "drybay_kidney_dish")
                    return 'retry'
            else:
                globals.LOGGING.logerr("Error during gripper changing, retrying")
                return 'retry'
        else:
            globals.LOGGING.logerr("Error during drybay homing, retrying")
            return 'retry'

class TransferWrappingBayTraySM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail','retry'])
    
    def execute(self, userdata):
        """Transfer Wrapping Bay Tray (consists of retractors and 2 more wrapped packages) from wrapping bay output to drybay"""
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT > 3:
            globals.TRIAL_COUNT = 0
            return 'fail'
        globals.LOGGING.loginfo("Transferring Wrapping Bay Tray to Drybay...")
        drybay_wbt = [globals.TRAY_REFERENCE_POINT['coor'].pose.position.x + 0.05, globals.TRAY_REFERENCE_POINT['coor'].pose.position.y - 0.2, 9.99, 0.707, 0.707, 0, 0] #y was -0.11, qutenion was 0,1,0,0
        if globals.PIGEONHOLE_PROCESSING <= 3:
            drybay_wbt[2] = 1.2     
        elif globals.PIGEONHOLE_PROCESSING >3:
            drybay_wbt[2] = 0.9
        print("drybay_wbt", drybay_wbt)
        globals.LOGGING.loginfo("Transferring Retractors to Drybay...")
        ai_return = query_ai('WBT') #WBT = Wrapping Bay Tray
        if ai_return['success']:
            globals.LOGGING.logerr("Unable to locate Wrapping Bay Tray at Wrapping Bay Output, item missing, please assist to transfer Retractors and 2 more Wrapped Packages to DryBay")
            return 'fail'
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.GO_TO_PT1
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.CHANGE_GRIPPER
            motion_goal.gripper = motion_goal.PARALLEL
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                motion_goal = RobotControlGoal()
                motion_goal.option = motion_goal.PICK_SPECIAL
                motion_goal.special_item = "drybay_picking"
                motion_goal.extra_argument = "drybay_wbt"
                motion_goal.target_pose = globals.list_to_posestamped(wrapping_bay_tray_placeholder)
                motion_goal.starting_location = "pt1"
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    if pick_success_check('WBT'):
                        globals.LOGGING.loginfo("AI can't detect wrapped package at Wrapping Bay Output, transfer successful")
                        motion_goal = RobotControlGoal()
                        motion_goal.option = motion_goal.GO_TO_DRYBAY
                        globals.MOTION_CLIENT.send_goal(motion_goal)
                        globals.MOTION_CLIENT.wait_for_result()
                        if globals.MOTION_CLIENT.get_result().success:
                            motion_goal.option = motion_goal.PLACE_SPECIAL
                            # to code this placing motion such that it dumps the package instread of letting it go
                            motion_goal.target_pose = globals.list_to_posestamped(drybay_wbt)
                            motion_goal.special_item = "drybay_wbt"
                            motion_goal.starting_location = "drybay"
                            globals.MOTION_CLIENT.send_goal(motion_goal)
                            globals.MOTION_CLIENT.wait_for_result()
                            if globals.MOTION_CLIENT.get_result().success:
                                motion_goal = RobotControlGoal()
                                motion_goal.option = motion_goal.GO_TO_PT1
                                globals.MOTION_CLIENT.send_goal(motion_goal)
                                globals.MOTION_CLIENT.wait_for_result()
                                if globals.MOTION_CLIENT.get_result().success:
                                    motion_goal = RobotControlGoal()
                                    motion_goal.option = motion_goal.PLACE
                                    motion_goal.target_pose = globals.list_to_posestamped(wrapping_bay_tray_placeholder)
                                    motion_goal.starting_location = "pt1"
                                    motion_goal.pigeonhole_processing = globals.PIGEONHOLE_PROCESSING
                                    globals.MOTION_CLIENT.send_goal(motion_goal)
                                    globals.MOTION_CLIENT.wait_for_result()
                                    if globals.MOTION_CLIENT.get_result().success:
                                        globals.PROCESS_COMPLETED = 14
                                        globals.TRIAL_COUNT = 0
                                        return 'success'
                                    else:
                                        globals.recovery_placing("drybay", drybay_wbt, "wbt")
                                        globals.LOGGING.logerr("Error during transferring Wrapping Bay Tray back to original position, please assist to transfer it back")
                                        return 'fail'
                                else:
                                    globals.recovery_placing("drybay", drybay_wbt, "wbt")
                                    globals.LOGGING.logerr("Error during transferring Wrapping Bay Tray back to original position, please assist to transfer it back")
                                    return 'fail'
                            else:
                                globals.recovery_placing("drybay", drybay_wbt, "wbt")
                                globals.LOGGING.logerr("Error during transferring Retractors and Wrapped Packages to DryBay, please assist to transfer it and Wrapping Bay Tray to its original position")
                                return 'fail'
                        else:
                            globals.recovery_placing("pt1", globals.list_to_posestamped(wrapping_bay_tray_placeholder), "wbt")
                            globals.LOGGING.logerr("Error during transferring Retractors and Wrapped Packages to DryBay, retrying")
                            return 'retry'
                    else:
                        globals.recovery_placing("pt1", globals.list_to_posestamped(wrapping_bay_tray_placeholder), "wbt")
                        globals.LOGGING.logerr("AI still seeing Wrapping Bay Tray at Wrapping Bay, retrying")
                        return 'retry'
                else:
                    globals.recovery_placing("pt1", globals.list_to_posestamped(wrapping_bay_tray_placeholder), "wbt")
                    globals.LOGGING.logerr("Error during Wrapping Bay Tray picking, retrying")
                    return 'retry'
            else:
                globals.LOGGING.logerr("Error during gripper changing, retrying")
                return 'retry'
        else:
            globals.LOGGING.logerr("Error during pt1 homing, retrying")
            return 'retry'
  
class TransferPrintedListSM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail', 'retry'])
    
    def execute(self, userdata):
        '''Trasnfer wrapped printed list from wetbay printer bay to drybay'''
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT > 3:
            globals.TRIAL_COUNT = 0
            return 'fail'
        drybay_printed_list = [globals.TRAY_REFERENCE_POINT['coor'].pose.position.x, globals.TRAY_REFERENCE_POINT['coor'].pose.position.y - 0.25, 9.99, 0.707, -0.707, 0, 0] #to fill in the quatenion
        if globals.PIGEONHOLE_PROCESSING <= 3:
            drybay_printed_list[2] = 1.13
        elif globals.PIGEONHOLE_PROCESSING >=4:
            drybay_printed_list[2] = 0.8
        print("drybay_printed_list", drybay_printed_list)
        #activate printer to print the list and wrapp, estimated to take 20secs
        globals.LOGGING.loginfo("Printing logsheet for instrument set at Pigeonhole #" + str(globals.PIGEONHOLE_PROCESSING))
        sleep(1)
        # sleep(20)
        globals.LOGGING.loginfo("Printing comepleted, transferring to DryBay")
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.GO_TO_WETBAY
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.CHANGE_GRIPPER
            motion_goal.gripper = motion_goal.PARALLEL
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                motion_goal = RobotControlGoal()
                motion_goal.option = motion_goal.PICK
                motion_goal.target_pose = globals.list_to_posestamped(printed_list_placeholder)
                motion_goal.starting_location = "wetbay"
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    motion_goal.option = motion_goal.GO_TO_DRYBAY
                    globals.MOTION_CLIENT.send_goal(motion_goal)
                    globals.MOTION_CLIENT.wait_for_result()
                    if globals.MOTION_CLIENT.get_result().success:
                        motion_goal = RobotControlGoal()
                        motion_goal.option = motion_goal.PLACE
                        motion_goal.target_pose = globals.list_to_posestamped(drybay_printed_list)
                        motion_goal.starting_location = "drybay"
                        motion_goal.pigeonhole_processing = globals.PIGEONHOLE_PROCESSING
                        globals.MOTION_CLIENT.send_goal(motion_goal)
                        globals.MOTION_CLIENT.wait_for_result()
                        if globals.MOTION_CLIENT.get_result().success:
                            globals.LOGGING.loginfo("Successfully transfer Printed List to Drybay")
                            globals.PROCESS_COMPLETED = 15
                            globals.TRIAL_COUNT = 0
                            return 'success'
                        else:
                            globals.recovery_placing("wetbay", printed_list_placeholder, "printed_list") #check if recovery placing can be done, else transfer to pt1 instead
                            globals.LOGGING.logerr("Error during transferring Printed List to DryBay, retrying")
                            return 'retry'
                    else:
                        globals.recovery_placing("wetbay", printed_list_placeholder, "printed_list") #check if recovery placing can be done, else transfer to pt1 instead
                        globals.LOGGING.logerr("Error during Drybay homing, retrying")
                        return 'retry'
                else:
                    globals.LOGGING.logerr("Error during picking Printed List from Wetbay, retrying")
                    return 'retry'
            else:
                globals.LOGGING.logerr("Error during gripper changing, retrying")
                return 'retry'
        else:
            globals.LOGGING.logerr("Error during wetbay homing, retrying")
            return 'retry'

class TransferStickerSM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail', 'retry'])
    
    def execute(self, userdata):
        '''Trasnfer wrapped Sticker Tag from wetbay printer bay to drybay'''
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT > 3:
            globals.TRIAL_COUNT = 0
            return 'fail'
        drybay_sticker_tag = [globals.TRAY_REFERENCE_POINT['coor'].pose.position.x, globals.TRAY_REFERENCE_POINT['coor'].pose.position.y - 0.25, 9.99, 0.707, -0.707, 0, 0] #to fill in the quatenion
        if globals.PIGEONHOLE_PROCESSING <= 3:
            drybay_sticker_tag[2] = 1.13
        elif globals.PIGEONHOLE_PROCESSING >=4:
            drybay_sticker_tag[2] = 0.8
        print("drybay_sticker_tag", drybay_sticker_tag)
        #activate printer to print the list and wrapp, estimated to take 20secs
        globals.LOGGING.loginfo("Printing logsheet for instrument set at Pigeonhole #" + str(globals.PIGEONHOLE_PROCESSING))
        sleep(2)
        globals.LOGGING.loginfo("Printing comepleted, transferring to DryBay")
        motion_goal = RobotControlGoal()
        motion_goal.option = motion_goal.GO_TO_WETBAY
        globals.MOTION_CLIENT.send_goal(motion_goal)
        globals.MOTION_CLIENT.wait_for_result()
        if globals.MOTION_CLIENT.get_result().success:
            motion_goal = RobotControlGoal()
            motion_goal.option = motion_goal.CHANGE_GRIPPER
            motion_goal.gripper = motion_goal.PARALLEL
            globals.MOTION_CLIENT.send_goal(motion_goal)
            globals.MOTION_CLIENT.wait_for_result()
            if globals.MOTION_CLIENT.get_result().success:
                motion_goal = RobotControlGoal()
                motion_goal.option = motion_goal.PICK
                motion_goal.target_pose = globals.list_to_posestamped(sticker_tag_placeholder)
                motion_goal.starting_location = "wetbay"
                globals.MOTION_CLIENT.send_goal(motion_goal)
                globals.MOTION_CLIENT.wait_for_result()
                if globals.MOTION_CLIENT.get_result().success:
                    motion_goal.option = motion_goal.GO_TO_DRYBAY
                    globals.MOTION_CLIENT.send_goal(motion_goal)
                    globals.MOTION_CLIENT.wait_for_result()
                    if globals.MOTION_CLIENT.get_result().success:
                        motion_goal = RobotControlGoal()
                        motion_goal.option = motion_goal.PLACE
                        motion_goal.target_pose = globals.list_to_posestamped(drybay_sticker_tag)
                        motion_goal.starting_location = "drybay"
                        motion_goal.pigeonhole_processing = globals.PIGEONHOLE_PROCESSING
                        globals.MOTION_CLIENT.send_goal(motion_goal)
                        globals.MOTION_CLIENT.wait_for_result()
                        if globals.MOTION_CLIENT.get_result().success:
                            globals.LOGGING.loginfo("Successfully transfer Sticker Tag to Drybay")
                            globals.PROCESS_COMPLETED = 16
                            globals.TRIAL_COUNT = 0
                            return 'success'
                        else:
                            globals.recovery_placing("wetbay", sticker_tag_placeholder, "printed_list") #check if recovery placing can be done, else transfer to pt1 instead
                            globals.LOGGING.logerr("Error during transferring Sticker Tag to DryBay, retrying")
                            return 'retry'
                    else:
                        globals.recovery_placing("wetbay", sticker_tag_placeholder, "printed_list") #check if recovery placing can be done, else transfer to pt1 instead
                        globals.LOGGING.logerr("Error during Drybay homing, retrying")
                        return 'retry'
                else:
                    globals.LOGGING.logerr("Error during picking Sticker Tag from Wetbay, retrying")
                    return 'retry'
            else:
                globals.LOGGING.logerr("Error during gripper changing, retrying")
                return 'retry'
        else:
            globals.LOGGING.logerr("Error during wetbay homing, retrying")
            return 'retry'

class ReleaseCoverAndRetract(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])
    
    def execute(self, userdata):
        '''After the assembling process has completed, release the cover of Drybay and retract the drawer'''
        globals.LOGGING.loginfo("Instrument set at Pigeonhole #" + str(globals.PIGEONHOLE_PROCESSING) + " has been fully processed, retracking and dropping cover")
        # arduino = serial.Serial('/dev/drybay_cover', 1000000, timeout=1)
        globals.plc_task("drawer_ret_drybay", globals.PIGEONHOLE_PROCESSING)
        # sleep(6)
        # arduino.write(str.encode(str(globals.PIGEONHOLE_PROCESSING)))
        if requests.put(globals.BOX_ID_URL, json={'status':'completed'}).status_code != 200:
            globals.LOGGING.logwarn("Error updating box status in database")
        return 'success'


class ResetTempBracPositionSM(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success','fail', 'retry'])
    
    def execute(self, userdata):
        '''Reset the temp bracket position to BRAC_1'''
        globals.TRIAL_COUNT += 1
        if globals.TRIAL_COUNT > 3:
            globals.TRIAL_COUNT = 0
            return 'fail'
        if not pick_temp_brac():             
            return 'retry'
        globals.LOGGING.loginfo("Successfully transfer Temp Bracket to default position")
        globals.PROCESS_COMPLETED = 17
        globals.TRIAL_COUNT = 0
        return 'success'
        

######################################################################### SMACH CLASS end ###########################################333
class DrybayDebugFunc(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])
    
    def execute(self, userdata):
        pick_temp_brac()
        return 'success'