# Project: Goldfinger UR10 Related Respitory
Consists of 2 packages: 

*	ur10_pap: The moveit package with connection to physical UR10 and motion_server
*	program_manager: The main program that controls the whole Goldfinger process, eg: getting data from ai, sending movement commands to UR10, activating modules etc

## Dependencies
* [Moveit](https://moveit.ros.org/install/)
* [Universal Robot Moveit config](https://github.com/ros-industrial/universal_robot)
* [Universal Robot ROS Driver](https://github.com/UniversalRobots/Universal_Robots_ROS_Driver) - for physical robot control 

## Launching
1. Launch the UR10 at IP address = 192.168.10.100, this will launch all necessary moveit framework, rviz visualizer, gripper_server and motion_server  
```roslaunch ur10_pap ur10_pap_bringup.launch```  
Alternatively, if simultion can be launched with  
```roslaunch ur10_pap demo.launch```  
2. Launch the program_manager and with number of pigeonhole to be processed parsed as arguments, for example to process pigeonhole #1, #3 and #6  
```rosrun program_manager combined_controller.py 0 2 5```

---
## motion_server
The motion_server is a wrapper around the moveit motion planner. Through different option send to it as action call, different moveit function will be used to move the robot to desired position. Gripper control is built into this motion_server such the loop is properly closed

### About action_lib usage
RobotControlGoal has 10 arguments:

| Variable Type | Variable Name | Description |
| --- | --- | --- |
| int16 | RobotControlGoal.option | Run some predefined function from the motion_server |
| string | RobotControlGoal.starting_location | (Optional) If this is set, the robot will moved back to predefined joint |
| string | RobotControlGoal.special_item | (Optional) The name of the item to be consider during pick and place task, used together with PICK_SPECIAL (11) or PLACE_SPECIAL (21) |
| string | RobotControlGoal.extra_zdown | (Optional) Extra downward motion during picking / placing, used together with PICK (1), PICK_SPECIAL(11), PLACE (2) or PLACE_SPECIAL (21) |
| int8 | RobotControlGoal.pigeonhole_processing | (Optional) If this is set, the robot will moved to a fixed height of 1.25m instead of 10cm above to ignore collision during wetbay / drybay process|
| string | RobotControlGoal.extra_argument | (Optional) Some extra argument to be pass in |
| geometry_msgs/PoseStamped | target_pose | The target pose of the coordinate to go |
| geometry_msgs/PoseStamped | reference_pose | (Optional) The reference pose of something, used together with PLACE_SPECIAL (21) |
| int8 | gripper | (Optional) The target gripper to usem use with CHANGE_GRIPPER |

#### Status Query Option
Returns the UR10's current PoseStamped and joints' angle

* int16 RobotControlGoal.STATUS (3)  

#### Predefined Joint Motion Option
* int16 RobotControlGoal.HOME (0) 
* int16 RobotControlGoal.GO_TO_WETBAY (61)
* int16 RobotControlGoal.GO_TO_DRYBAY (62)
* int16 RobotControlGoal.GO_TO_PT1 (63)
* int16 RobotControlGoal.GO_TO_PT1_INSTRUMENT_PAP (631)
* int16 RobotControlGoal.GO_TO_PT2 (64)
#### Cartesian Motion Option
Move the UR10 to targeted position with cartesian movement.

* int16 RobotControlGoal.GO_TO_LOCATION (6)

#### Picking Option
Standard picking task, the UR10 will move to a coordinate 10cm above, then navigate 10cm downwards for item picking, then proceed with closing the gripper, lift up by 10cm.  
If pigeonhole_processing is provided, the robot will lift itself up to 1.25m instead of 10cm to prevent collision when picking from pigeonhole  
If starting_location is provided, the robot will route back to one of the predefined joint position joint earlier
If extra_zdown is provided, the robot will navigate 10cm + extra_zdown downwards and upward during picking

* int16 RobotControlGoal.PICK (1)  

For some items that required non standard picking routine, special_item must be provided

* int16 RobotControlGoal.PICK_SPECIAL (11)

#### Placing Option
Standard placing task, the UR10 will move to a coordinate 10cm above, then navigate 10cm downwards for item picking, then proceed with open the gripper, lift up by 10cm.  
If pigeonhole_processing is provided, the robot will lift itself up to 1.25m instead of 10cm to prevent collision when picking from pigeonhole  
If starting_location is provided, the robot will route back to one of the predefined joint position joint earlier
If extra_zdown is provided, the robot will navigate 10cm + extra_zdown downwards and upward during picking

* int16 RobotControlGoal.PLACE (2)  

For some items that required non standard picking routine, special_item must be provided, reference_target parameter is required for some item

* int16 RobotControlGoal.PLACE_SPECIAL (21)

#### Gripper Option
To change the gripper

* int16 RobotControlGoal.CHANGE_GRIPPER (5)  

gripper argument must be parse in, choose one from the below:

* int8 RobotControlGoal.PARALLEL_GRIPPER (50) 
* int8 RobotControlGoal.SUCTION_GRIPPER (51) 
* int8 RobotControlGoal.DRAG_GRIPPER (52) 

#### Read Gripper Opening Option
Returns the curent gripper opening in mm

* int16 RobotControlGoal.READ_GRIPPER_OPENING (7)  

#### Read Force-troque Option
Returns the curent force-torque

* int16 RobotControlGoal.READ_FORCE_TORQUE (7)  

#### Dashboard Server Query Option
Query UR10's dashboard server and return its robotstate and programstate

* int16 RobotControlGoal.DASHBOARD_SERVER_QUERY (100)  

#### Dashboard Server Command Option
Send command to UR10's dashboard server, command should be provided in extra_argument 

* int16 RobotControlGoal.DASHBOARD_SERVER_ACTION (101)  


---
## program_manager
The program_manager is the main program of the system, it accepts up to 6 command line arguments (represents pigeonhole_to_process).  
The [smach](http://wiki.ros.org/smach) architecture is adopted such that the program will run the next function based on previous output.  
It will also communicates with the front-end UI and reports its status from time to time.  
Rough flow:

1. Initialization - Establish connection to all servers, failed to do so will result in termination and error prompted to frontend
2. Read the command line argument to get the pigeonhole to be processed
3. Wetbay unloading 
4. Processing Table 1 to Non-ring Inspection Bay Picking
5. Drybay assembling